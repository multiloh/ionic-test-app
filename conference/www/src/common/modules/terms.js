'use strict';

(function (triptop) {
     if (!triptop || !triptop.options || !triptop.widgets) {
        throw new Error('Application not configured');
    }

    var options = triptop.options;
    var widgets = triptop.widgets;

    triptop.terms = {};

    widgets.directive('terms', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('css', 'common', 'terms' + options.css_suffix);
            }
        };
    }]);

    widgets.directive('termsrules', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div id="tt-booking-rules"></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('css', 'common', 'terms' + options.css_suffix);
                resourceLoader.get('css', 'libs', 'elusive-iconfont/css/elusive-webfont');

                resourceLoader.get('templates', 'common', 'flights/rules', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('transferstermsrules', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div id="tt-booking-transfers-rules"></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('css', 'common', 'terms' + options.css_suffix);
                resourceLoader.get('css', 'libs', 'elusive-iconfont/css/elusive-webfont');

                resourceLoader.get('templates', 'common', 'transfers/rules', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('termstariff', ['$compile', '$http', 'resourceLoader', function ($compile, $http, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div id="tt-booking-tariff"></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('css', 'common', 'terms' + options.css_suffix);
                resourceLoader.get('css', 'libs', 'elusive-iconfont/css/elusive-webfont');

                if(triptop.booking && triptop.booking.flights && triptop.booking.flights.rules) {
                    triptop.booking.flights.rules(function (url) {
                        $scope.termsRulesUrl = url + '&hide_focus=true';
                        resourceLoader.get('templates', 'common', 'flights/tariff', null, function (template) {
                            $element.html(template);
                            $compile($element.contents())($scope);
                        });
                    });
                }
            }
        };
    }]);

    widgets.directive('termsconvention', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div id="tt-booking-convention"></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('css', 'common', 'terms' + options.css_suffix);
                resourceLoader.get('css', 'libs', 'elusive-iconfont/css/elusive-webfont');

                resourceLoader.get('templates', 'common', 'flights/convention', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);
}(window.triptop));