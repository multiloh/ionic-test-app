'use strict';

(function (triptop) {
    if (!triptop || !triptop.options || !triptop.widgets) {
        throw new Error('Application not configured');
    }

    var options = triptop.options;
    var widgets = triptop.widgets;

    triptop.terminal = {
        scope: null,
    };

    widgets.directive('terminal', ['$compile', '$http', 'resourceLoader', function ($compile, $http, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div id="triptop-terminal" class="triptop-container"></div>',
            controller: function ($scope, $element) {
                triptop.terminal.scope = $scope;

                
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'terminal', 'content', 'terminal.jquery.min', function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);

                    if(options.terminal && options.terminal.ready) {
                        options.terminal.ready();
                    }
                });
            }
        };
    }]);

    widgets.config(function (resourceLoaderProvider) {
        resourceLoaderProvider.addDependency('terminal.jquery.min', function (loader, success) {
            loader.css('libs', 'bootstrap/css/less/bootstrap' + options.css_suffix);
            loader.css('terminal', 'terminal' + options.css_suffix);

            loader.js('libs', 'jquery/jquery.min', function () {
                loader.js('libs', 'jquery/jquery.md5', success);
            });
        });
    });
}(window.triptop));