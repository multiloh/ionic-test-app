'use strict';

(function (triptop) {
    if (!triptop || !triptop.options || !triptop.widgets || !triptop.api) {
        throw new Error('Application not configured');
    }

    var options = triptop.options;
    var widgets = triptop.widgets;
    var api = triptop.api;
    var transfers = api.transfers;
    var cart = api.cart;

    triptop.transfers = {
        scope: null
    };

    var isCenter = function(str) {
        if (str.toUpperCase().indexOf("מרכז") > -1) {
            return true;
        }
        if (str.toUpperCase().indexOf("CENTER") > -1) {
            return true;
        }
        if (str.toUpperCase().indexOf("ЦЕНТР") > -1) {
            return true;
        }
        return false;
    };

    var isAirport = function(str) {
        if (str.toUpperCase().indexOf("התעופה") > -1) {
            return true;
        }
        if (str.toUpperCase().indexOf("AIRPORT") > -1) {
            return true;
        }
        if (str.toUpperCase().indexOf("АЭРОПОРТ") > -1) {
            return true;
        }
        return false;
    };

    var isStation = function(str) {
        if (str.toUpperCase().indexOf("רכבת") > -1) {
            return true;
        }
        if (str.toUpperCase().indexOf("STATION") > -1) {
            return true;
        }
        if (str.toUpperCase().indexOf("ВОКЗАЛ") > -1) {
            return true;
        }
        return false;
    };

    var errorCallback = function (err) {
        api.alert(api.getErrorLocalizedMessage(404));
    };

    var upDate = function (date, days) {
        return new Date(date.getTime() + days*24*60*60*1000);
    };

    widgets.directive('transfers', ['$compile', '$http', 'resourceLoader', function ($compile, $http, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div id="triptop-transfers" class="triptop-container"></div>',
            controller: function ($scope, $element) {
                triptop.transfers.scope = $scope;

                $scope.transfersSearchStage = 0;
                $scope.imgUrlBase = options.paths.templates.transfers + "/../../img/";

                $scope.query = {
                    country: null,
                    city: null,
                    from: null,
                    to: null,
                    type: 0,
                    arrivalTime: upDate(new Date(), 3),
                    departureTime: null,
                    paxes: {
                        adults: 1,
                        children: 0,
                        infants: 0
                    }
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'transfers', 'content', 'transfers.jquery.min', function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);

                    if(options.transfers && options.transfers.ready) {
                        options.transfers.ready();
                    }
                });
            }
        };
    }]);

    widgets.config(function (resourceLoaderProvider) {
        resourceLoaderProvider.addDependency('transfers.jquery.min', function (loader, success) {
            loader.css('libs', 'bootstrap/css/less/bootstrap' + options.css_suffix);
            loader.css('libs', 'bootstrap-combobox/css/bootstrap-combobox' + options.css_suffix);
            loader.css('libs', 'datepicker/css/bootstrap-datepicker');
            loader.css('libs', 'timepicker/css/jquery.timepicker');
            loader.css('transfers', 'transfers' + options.css_suffix);

            loader.get('templates', 'common', 'modal', null, function (template) {
                angular.element('body').append(template);
            });

            loader.js('libs', 'jquery/jquery.min', function () {
                var cnt = 4;
                var runner = function () {
                    cnt--;
                    if (cnt === 0 && success) { success(); }
                };
                loader.js('libs', 'bootstrap/js/bootstrap.min', runner);
                loader.js('libs', 'datepicker/bootstrap-datepicker', runner);
                loader.js('libs', 'timepicker/jquery.timepicker', runner);
                loader.js('libs', 'bootstrap-combobox/js/bootstrap-combobox', runner);
            });
        });
    });

    widgets.directive('transfersearch', ['$compile', '$http', 'resourceLoader', function ($compile, $http, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                transfers.CountriesRequest($http).send(function (data) {
                    $scope.countries = data;
                }, function (e) {});

                $scope.onSelectCountry = function () {
                    if(!$scope.query.country || !$scope.query.country.id) {
                        if ($scope.query.city) {
                            $element.find('select[combobox=cities]').data('combobox').toggle();
                        }
                        $scope.query.city = null;
                        $scope.query.cityTo = null;
                        if ($scope.query.from) {
                            $element.find('select[combobox^=locations]').data('combobox').toggle();
                        }
                        $scope.query.from = null;
                        $scope.query.to = null;
                        $scope.cities = null;
                        $scope.locations = null;
                        $scope.locationsTo = null;
                        return;
                    }

                    transfers.CitiesRequest($http, $scope.query.country.id).send(function (data) {
                        $scope.cities = data;
                        $scope.query.cityTo = $scope.query.city = ($scope.cities.length > 0) ? $scope.cities[0] : null;

                        transfers.LocationsRequest($http, $scope.query.city.id).send(function (data) {
                            $scope.locations = $scope.locationsTo = data;
                            $scope.query.from = null;
                            $scope.query.to = null;

                            for (var i = 0; i < $scope.locations.length; i++) {
                                var location = $scope.locations[i];
                                location.parent = $scope.query.city;
                                var name = location.name;
                                if (isAirport(name)) {
                                    $scope.query.from = location;
                                }
                                if (isCenter(name)) {
                                    $scope.query.to = location;
                                }
                            }

                            if(!$scope.query.from) {
                                $scope.query.from = ($scope.locations.length > 0) ? $scope.locations[0] : null;
                            }
                            if(!$scope.query.to) {
                                $scope.query.to = ($scope.locations.length > 0) ? $scope.locations[0] : null;
                            }
                        }, function (e) {});
                    }, function (e) {});
                };

                $scope.onSelectCity = function (direction) {
                    if(!$scope.query.city || !$scope.query.city.id) {
                        $scope.query.city = null;
                        $scope.query.cityTo = null;
                        if ($scope.query.from || $scope.query.to) {
                            $element.find('select[combobox^=locations]').data('combobox').toggle();
                        }
                        $scope.query.from = null;
                        $scope.query.to = null;
                        return;
                    }

                    var both = false;
                    if(!direction) {
                        direction = 'from';
                        both = $scope.query.from.parent.id == $scope.query.to.parent.id; 
                    }

                    var suffix = direction == 'to' ? 'To' : '';
                    var city = 'city' + suffix;

                    var requestCallback = function (data, direction, locations) {
                        $scope[locations] = data;
                        $scope.query[direction] = null;

                        for (var i = 0; i < $scope[locations].length; i++) {
                            var location = $scope[locations][i];
                            location.parent = $scope.query[city];
                            var name = location.name;
                            if (direction == 'from' && isAirport(name)) {
                                $scope.query[direction] = location;
                            }
                            if (direction == 'to' && isCenter(name)) {
                                $scope.query[direction] = location;
                            }
                        }

                        if(!$scope.query[direction]) {
                            $scope.query[direction] = ($scope[locations].length > 0) ? $scope[locations][0] : null;
                        }
                    };

                    transfers.LocationsRequest($http, $scope.query[city].id).send(function (data) {
                        requestCallback(data, direction, 'locations' + suffix);
                        if(both) {
                            $scope.cities = angular.copy($scope.cities, []);
                            var city = null;
                            for (var i = 0; i < $scope.cities.length; i++) {
                                city = $scope.cities[i];
                                if(city.id === $scope.query.city.id) {
                                    break;
                                }
                            }
                            $scope.query.city = $scope.query.cityTo = city;
                            requestCallback(data, 'to', 'locationsTo');
                        }
                    }, function (e) {});
                };

                $scope.onChangeArrivalTime = function (changedDate) {
                    if(changedDate) {
                        var arrivalTime = $scope.query.arrivalTime;
                        var departureTime = $scope.query.departureTime;

                        var elem = $element.find('input[name=departureTime]');
                        elem.bootstrapDatepicker('setStartDate', arrivalTime);
                        if (!departureTime || arrivalTime.getTime() > departureTime.getTime()) {
                            departureTime = upDate(arrivalTime, 7);
                            elem.bootstrapDatepicker('_setDate', departureTime);
                            elem.bootstrapDatepicker('update');
                        }
                    }
                    $scope.setTimePicker($scope.query.arrivalTime, $scope.query._arrivalTime);
                };

                $scope.onChangeDepartureTime = function () {
                    $scope.setTimePicker($scope.query.departureTime, $scope.query._departureTime);
                };

                $scope.onChangeType = function () {
                    if($scope.query.type === '1') {
                        $scope.onChangeArrivalTime(true);
                    }
                };

                $scope.runSearch = function () {
                    if (!$scope.query.from || !$scope.query.to) {
                        api.alert(api.getErrorLocalizedMessage(7));
                        return;
                    }

                    if(triptop.booking && triptop.booking.transfers) {
                        triptop.booking.transfers.cancel();
                        triptop.booking.hide();
                    }

                    var queryType = $scope.query.type;
                    if (typeof queryType == 'string') {
                        queryType = parseInt(queryType, 10);
                        $scope.query.type = queryType;
                    }

                    delete $scope.results;
                    delete $scope.backwardResults;

                    var searchResultCallback = function (data, forwardSearchId, backwardSearchId) {
                        var id = data.id;
                        if (forwardSearchId === id) {
                            $scope.results = data;
                        }
                        else if (backwardSearchId === id) {
                            $scope.backwardResults = data;
                        }
                        else {
                            return;
                        }
                        var resultsPresent = false, p, product;

                        var initProduct = function (product) {
                            var merged = product.mergedProduct;
                            var p = merged ? merged : product;

                            p.amount = 1;
                            p.prices = {
                                phone: 10,
                                online: p.onlinePrice.baseAmount,
                                fees: {
                                    creditcard: p.onlinePrice.onlineMarkup,
                                },
                                currency: {
                                    code: p.onlinePrice.currencyCode,
                                    symbol: api.getCurrencySymbol(p.onlinePrice.currencyCode)
                                }
                            };

                            product.prices.total = product.prices._total = (merged ? product.prices.total : 0) + p.prices.online;

                            product.vehicle.isSole = false;
                            if(product.vehicle.code.indexOf('ss') === 0 || product.vehicle.code == 'cch' || product.vehicle.code == 'tba') {
                                product.vehicle.isSole = true;
                            }
                        };

                        if (queryType === 1 && $scope.results && $scope.backwardResults) {
                            for (p = 0; p < $scope.results.visibleProducts.length; p++) {
                                product = $scope.results.visibleProducts[p];
                                initProduct(product);

                                for (var q = 0; q < $scope.backwardResults.visibleProducts.length; q++) {
                                    var backProduct = $scope.backwardResults.visibleProducts[q];
                                    if (product.vehicle.code == backProduct.vehicle.code) {
                                        product.mergedProduct = backProduct;
                                        initProduct(product);
                                    }
                                }
                            }
                            resultsPresent = true;
                        }
                        else if (queryType === 0) {
                            resultsPresent = true;

                            for (p = 0; p < $scope.results.visibleProducts.length; p++) {
                                product = $scope.results.visibleProducts[p];
                                initProduct(product);
                            }
                        }

                        if (resultsPresent) {
                            if ($scope.results.visibleProducts.length === 0) {
                                $scope.transfersSearchStage = 2;
                            }
                            else {
                                $scope.results.visibleProducts.sort(function (a,b) {
                                    return a.prices.online - b.prices.online;
                                });
                                $scope.transfersSearchStage = 3;
                            }
                        }
                    };

                    var paxes = $scope.query.paxes;
                    paxes = parseInt(paxes.adults, 10) + parseInt(paxes.children, 10) + parseInt(paxes.infants, 10);
                    $scope.query.paxes.count = paxes;

                    transfers.StartSearchRequest($http, $scope.query.from.id, $scope.query.to.id, api.toISOString($scope.query.arrivalTime, false), paxes, 1).send(function (data) {
                        var forwardSearchId = data.id;

                        var polling = transfers.PollSearchRequest($http, forwardSearchId);
                        var pollingCallback = function (data) {
                            var complete = data.complete;
                            if (!complete) {
                                setTimeout(function () {
                                    polling.send(pollingCallback, errorCallback);
                                }, 500);
                            }
                            else {
                                transfers.ResultRequest($http, forwardSearchId).send(function (data) {
                                    searchResultCallback(data, forwardSearchId, null);
                                }, errorCallback);
                            }
                        };
                        polling.send(pollingCallback, errorCallback);
                    }, errorCallback);

                    if (queryType === 1) {
                        transfers.StartSearchRequest($http, $scope.query.to.id, $scope.query.from.id, api.toISOString($scope.query.departureTime, false), paxes, 1).send(function (data) {
                            var backwardSearchId = data.id;

                            var polling = transfers.PollSearchRequest($http, backwardSearchId);
                            var pollingCallback = function (data) {
                                var complete = data.complete;
                                if (!complete) {
                                    setTimeout(function () {
                                        polling.send(pollingCallback, errorCallback);
                                    }, 500);
                                } else {
                                    transfers.ResultRequest($http, backwardSearchId).send(function (data) {
                                        searchResultCallback(data, null, backwardSearchId);
                                    }, errorCallback);
                                }
                            };
                            polling.send(pollingCallback, errorCallback);
                        }, errorCallback);
                    }

                    $scope.transfersSearchStage = 1;
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'transfers', 'search', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('transferwaiting', ['$compile', '$http', 'resourceLoader', function ($compile, $http, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'transfers', 'waiting', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('transfernoresults', ['$compile', '$http', 'resourceLoader', function ($compile, $http, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'transfers', 'noresults', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('transferresults', ['$compile', '$http', 'resourceLoader', function ($compile, $http, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                $scope.getImage = function (code) {
                    var pic;
                    if(code.indexOf('ss') === 0) {
                        pic = "shuttle.png";
                    }
                    else {
                        switch (code) {
                            case undefined:
                                return $scope.imgUrlBase;
                            case 'std':
                                pic = "private.png";
                                break;
                            case 'tba':
                                pic = "bus.png";
                                break;
                            case "lux":
                                pic = "luxury.png";
                                break;
                            case "mnv":
                                pic = "minivan.png";
                                break;
                            case 'mnc':
                                pic = "minicoach.png";
                                break;
                            case "mnb":
                                pic = "minibus.png";
                                break;
                            case "cch":
                                pic = "coach.png";
                                break;
                            default:
                                pic = "unknown.png";
                                break;
                        }
                    }
                    return $scope.imgUrlBase + pic;
                };

                $scope.onAvailabilityInfo = function () {
                    api.alert(api.getErrorLocalizedMessage(6));
                };

                $scope.parseDuration = function (str, stringify) {
                    var ret = [0, 0], match = /PT((\d*)H)?((\d*)M)?/.exec(str);
                    if (match[2] && match[2] != '0') {
                        ret[0] = parseInt(match[2], 10);
                    }
                    if (match[4] && match[4] != '0') {
                        ret[1] = parseInt(match[4], 10);
                    }
                    if(stringify) {
                        ret[0] = (ret[0] < 10 ? '0' : '') + ret[0]; 
                        ret[1] = (ret[1] < 10 ? '0' : '') + ret[1]; 
                    }
                    return ret;
                };

                var getLocationSubType = function (id) {
                    for (var i = 0; i < $scope.locations.length; i++) {
                        var location = $scope.locations[i];
                        if (location.id == id) {
                            if (isCenter(location.name)) {
                                return "CENTER";
                            }
                            if (isAirport(location.name)) {
                                return "AIRPORT";
                            }
                            if (isStation(location.name)) {
                                return "STATION";
                            }
                        }
                    }
                    return "OTHER";
                };

                $scope.changeAmount = function (product) {
                    product.prices.total = product.prices._total * product.amount; 
                };

                $scope.startBooking = function (product) {
                    var scope = triptop.transfers.scope;
                    scope.transfersSearchStage = 4;
                    widgets.safeApply(scope);

                    var currency = product.onlinePrice.currencyCode;
                    currency = { code: currency, symbol: api.getCurrencySymbol(currency) };

                    var createBooking = function () {
                        var booking = { products: [], prices: { fare: 0, tax: 0, options: 0, discount: 0, creditcard: 0, total: 0, currency: currency }, help: {} };

                        var paxNum = $scope.query.paxes.count;
                        var paxNumbers = [];
                        for (var i = product.vehicle.minPax; i <= product.vehicle.maxPax; i++) {
                            paxNumbers.push(i);
                        }

                        var luggageNumbers = [];
                        for (var j = 0; j <= product.vehicle.maxLuggage; j++) {
                            luggageNumbers.push(j);
                        }

                        var products = (product.mergedProduct) ? [product, product.mergedProduct] : [product];
                        for (var l = 0; l < products.length; l++) {
                            var p = products[l], direction, dur, startTime, endTime, from, to;

                            if(l === 0) {
                                direction = 'direct';
                                startTime = $scope.query.arrivalTime;
                                from = p.pickUp;
                                to = p.dropOff;
                            }
                            else {
                                direction = 'return';
                                startTime = $scope.query.departureTime;
                                from = p.pickUp;
                                to = p.dropOff;
                            }

                            dur = $scope.parseDuration(p.transferDuration);
                            endTime = new Date(startTime.getTime());
                            endTime.setHours(startTime.getHours() + dur[0]);
                            endTime.setMinutes(startTime.getMinutes() + dur[1]);

                            booking.products.push({
                                type: 'TRANSFER',
                                id: p.id,
                                originalProduct: p,
                                direction: direction,
                                paxNumber: paxNum,
                                paxNumbers: paxNumbers,
                                availability: p.availability,
                                luggageNumber: paxNum,
                                luggageNumbers: luggageNumbers,
                                city: from.parent.name,
                                car: p.vehicle.code,
                                amount: p.amount,
                                locations: [
                                    {
                                        name: from.name,
                                        type: "PICKUP",
                                        id: from.id,
                                        subType: getLocationSubType(from.id),
                                        time: startTime
                                    },
                                    {
                                        name: to.name,
                                        type: "DROPOFF",
                                        id: to.id,
                                        subType: getLocationSubType(to.id),
                                        time: endTime
                                    }
                                ],
                                prices: {
                                    fare: p.prices.online * p.amount,
                                    tax: 0,
                                    options: 0,
                                    discount: 0,
                                    creditcard: p.prices.fees.creditcard * p.amount,
                                    total: p.prices.online * p.amount,
                                    currency: p.prices.currency
                                },
                                paxes: {}
                            });
                        }
                        return booking;
                    };

                    triptop.booking.init(createBooking(), undefined, undefined, function () {
                        scope.transfersSearchStage = 3;
                        widgets.safeApply(scope);
                    });
                    triptop.booking.show({'width': angular.element('#triptop-transfers').width(), 'margin':'0 auto'});
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'transfers', 'results', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.filter('transfersprice', function () {
        return function (prices, type) {
            var value = prices[type];
            if(value) {
                return Math.abs(value).toFixed(2) + ' ' + prices.currency.symbol;
            }
            return '';
        };
    });
}(window.triptop));