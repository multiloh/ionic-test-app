'use strict';

(function (triptop) {
    if (!triptop) {
        throw new Error('Application not configured');
    }

    var options = triptop.options;
    var widgets = triptop.widgets;

    triptop.filters.hotelsfilter = {
        scope: null,
		open: function (names) {
			triptop.filters.hotelsfilter.hotelNames = names;
            var scope = triptop.filters.hotelsfilter.scope;
			scope.isExternalFilter = true;
			scope.openFilter();
			scope.$digest();
		},
		close: function () {
            var scope = triptop.filters.hotelsfilter.scope;
			scope.isExternalFilter = true;
			scope.closeFilter();
			scope.$digest();
		},
		hide: function () {
            var scope = triptop.filters.hotelsfilter.scope;
			scope.isExternalFilter = true;
			scope.hideFilter();
			scope.$digest();
		}
    };

	widgets.directive('hotelsfilter', ['$compile', function ($compile) {
		return {
			restrict: 'AE',
			controller: function ($scope, $element) {
				triptop.filters.hotelsfilter.scope = $scope; 

				$scope.isExternalFilter = false;
				$scope.isOpenedFilter = undefined;
				
				$scope.filter = {
					name: '',
					budget: 0,
					raiting: 0
				};

				$scope.openFilter = function () {
					this.isOpenedFilter = true;
				};

				$scope.closeFilter = function () {
					this.isOpenedFilter = false;
				};

				$scope.hideFilter = function () {
					this.isOpenedFilter = undefined;
					$scope.filter = {
						name: '',
						budget: 0,
						raiting: 0
					};
				};
			},
			link: function ($scope, $element, $attrs) {
				var modify = function (isOpenedFilter, value) {
					if($scope.isExternalFilter === false && isOpenedFilter === value) { return; }
					$scope.isExternalFilter = false;

					if(isOpenedFilter === true) {
						$element.html('<hotelsfilteropened></hotelsfilteropened>');
						$compile($element.contents())($scope);
                    }
					else if(isOpenedFilter === false) {
						$element.html('<hotelsfilterclosed></hotelsfilterclosed>');
						$compile($element.contents())($scope);
					}
					else {
						$element.html('');
					}
				};

				$scope.$watch('isOpenedFilter', function (isOpenedFilter, value) {
					modify(isOpenedFilter, value);
				});
				modify($scope.isOpenedFilter);

                if(options.filters && options.filters.ready) {
                    options.filters.ready();
                }
			}
		};
	}]);

	widgets.directive('hotelsfilteropened', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
		return {
			restrict: 'AE',
			controller: function ($scope, $element) {
				var ranges = [], budgets = window.triptopApi.ctrlFlags.hotelFilterBudget;
				for (var i = 0; i < budgets.length; i++) {
					ranges.push(budgets[i].title);
				}

				$scope.budgets = ranges;

				$scope.getNames = function () {
                    var options = [];
                    var names = triptop.filters.hotelsfilter.hotelNames;
                    for (var i = 0, s = names.length; i < s; i++) {
                        var opt = names[i];
                        options.push({ value: opt, text: opt });
                    }
                    return options;
				};

				$scope.setNames = function (value) {
					$scope.filter.name = value;
					$scope.applyFilter();
				};
/*
                $scope.$watch('filter.name', function(newValue, oldValue) {
					$scope.applyFilter();
                });

				$scope.changeName = function ($event) {
					if(!$event.keyCode || $event.keyCode === 13) {
						$scope.filter.name = $event.target.value;
						$scope.applyFilter();
					}
				};
*/
				$scope.selectBudget = function ($event, index) {
					var target = angular.element($event.target);
					if($event.target.nodeName === 'INPUT' || $event.target.nodeName === 'SPAN') {
						target = target.parent();
					}
					target.parent().children().removeClass('active');
					target.addClass('active');
					target.children()[0].checked = true;
					$scope.filter.budget = index;
					$scope.applyFilter();
				};

				$scope.selectRaiting = function ($event, index) {
					var target = angular.element($event.target);
					if($event.target.nodeName === 'INPUT') {
						target = target.parent();
					}
					target.parent().children().removeClass('active');
					target.addClass('active');
					target.children()[0].checked = true;
					$scope.filter.raiting = index;
					$scope.applyFilter();
				};

				$scope.applyFilter = function () {
					var triptopApi = window.triptopApi;
					if(triptopApi && triptopApi.HotelSearchFilterJs) {
						var req = new triptopApi.HotelSearchFilterJs($scope.filter.budget, $scope.filter.raiting, $scope.filter.name);
						triptopApi[req.apiName](req);
					}
				};
			},
			link: function ($scope, $element, $attrs) {
				resourceLoader.get('templates', 'filters', 'hotelsfilter/opened', null, function (template) {
					$element.html(template);
					$compile($element.contents())($scope);
				});
			}
		};
	}]);

	widgets.directive('hotelsfilterclosed', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
		return {
			restrict: 'AE',
			link: function ($scope, $element, $attrs) {
				resourceLoader.get('templates', 'filters', 'hotelsfilter/closed', null, function (template) {
					$element.html(template);
					$compile($element.contents())($scope);
				});
			}
		};
	}]);
}(window.triptop));