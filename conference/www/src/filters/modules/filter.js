'use strict';

(function (triptop) {
    if (!triptop || !triptop.options || !triptop.widgets) {
        throw new Error('Application not configured');
    }

    var options = triptop.options;
    var widgets = triptop.widgets;

    triptop.filters = {
        scope: null
    };

    widgets.directive('filter', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            scope: true,
            replace: true,
            template: '<div id="triptopFilter" class="triptop-container"></div>',
            link: function ($scope, $element, $attrs) {
                var styling = (options.filters && options.filters.styling) ? options.filters.styling : '';
                resourceLoader.get('css', 'filters', 'filters' + styling);

                triptop.filters.scope = $scope;

                var type = $attrs.type;
                resourceLoader.get('module', 'filters', type, 'filters.common', function () {
                    $element.html('<' + type + '></' + type + '>');
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.config(function (resourceLoaderProvider) {
        resourceLoaderProvider.addDependency('filters.common', function (loader, success) {
            loader.css('libs', 'bootstrap/css/less/bootstrap' + options.css_suffix);
            loader.css('libs', 'selectize/css/selectize.bootstrap3');
            loader.js('libs', 'jquery/jquery.min', function () {
                loader.js('libs', 'selectize/js/standalone/selectize', function () {
                    loader.js('libs', 'selectize/js/plugins/continue_editing', success);
                });
            });
        });
    });
}(window.triptop));