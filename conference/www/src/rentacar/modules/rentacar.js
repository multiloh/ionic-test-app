'use strict';

(function (triptop, $) {
    if (!triptop || !triptop.options || !triptop.widgets || !triptop.api) {
        throw new Error('Application not configured');
    }

    var options = triptop.options;
    var widgets = triptop.widgets;
    var api = triptop.api;
    // var insurance = api.rentacar;

    triptop.rentacar = {
        scope: null
    };

    widgets.directive('rentacar', ['$compile', '$http', 'resourceLoader', function ($compile, $http, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div id="triptop-rentacar" class="triptop-container"></div>',
            controller: function ($scope, $element) {
                triptop.rentacar.scope = $scope;

                $scope.rentacarSearchStage = 3;

                $scope.resultCars = [{
                    name: 'Suzuki Alto',
                    class: 'Mini', // Mini, economy, etc.
                    img: 'alto_nologo.jpg',
                    passengerQuantity: 2,
                    baggageQuantity: 1,
                    doorsQuantity: 2,
                    transmission: 'man',
                    airConditioning: true,
                    info: {
                        terminalPickup: true,
                        meetAndGreet: false,
                        fuelFull: true,
                        freeCancellation: true,
                        freeAmendment: true
                    },
                    price: 31.50
                },
                    {
                        name: 'Kia Picanto',
                        class: 'Economy', // Mini, economy, etc.
                        img: 'fortwo_nologo.jpg',
                        passengerQuantity: 5,
                        baggageQuantity: 3,
                        doorsQuantity: 3,
                        transmission: 'auto',
                        airConditioning: true,
                        info: {
                            terminalPickup: false,
                            meetAndGreet: true,
                            fuelFull: true,
                            freeCancellation: true,
                            freeAmendment: true
                        },
                        price: 29.73
                    }];

                $scope.countries = [
                    'Russia',
                    'USA',
                    'Canada',
                    'UK',
                    'Albania',
                    'Austria',
                    'Germany'
                ];

                $scope.query = {
                    from: null,
                    diffLocation: false,
                    returnLocation: null,
                    startDate: null,
                    startTime: null,
                    endDate: null,
                    endTime: null,
                    country: null
                };

                $scope.runSearch = function () {
                    if ($scope.rentacarSearchStage < 3) {
                        $scope.rentacarSearchStage++;
                    }
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'rentacar', 'content', 'rentacar.jquery.min', function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);

                    if(options.rentacar && options.rentacar.ready) {
                        options.rentacar.ready();
                    }
                });
            }
        };
    }]);

    widgets.config(function (resourceLoaderProvider) {
        resourceLoaderProvider.addDependency('rentacar.jquery.min', function (loader, success) {
            loader.css('libs', 'bootstrap/css/less/bootstrap' + options.css_suffix);
            loader.css('libs', 'bootstrap-combobox/css/bootstrap-combobox' + options.css_suffix);
            loader.css('libs', 'datepicker/css/bootstrap-datepicker');
            loader.css('libs', 'timepicker/css/jquery.timepicker');
            loader.css('rentacar', 'rentacar' + options.css_suffix);

            loader.get('templates', 'common', 'modal', null, function (template) {
                angular.element('body').append(template);
            });

            loader.js('libs', 'jquery/jquery.min', function () {
                var cnt = 3;
                var complete = function () {
                    cnt--;
                    if (cnt === 0 && success) { success(); }
                };
                loader.js('libs', 'datepicker/bootstrap-datepicker', complete);
                loader.js('libs', 'timepicker/jquery.timepicker', complete);
                loader.js('libs', 'bootstrap-combobox/js/bootstrap-combobox', complete);
            });
        });
    });
    /***/
    widgets.directive('rentacarsearch', ['$compile', '$http', 'resourceLoader', function ($compile, $http, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {},
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'rentacar', 'search', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('rentacarwaiting', ['$compile', '$http', 'resourceLoader', function ($compile, $http, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'rentacar', 'waiting', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('rentacarnoresults', ['$compile', '$http', 'resourceLoader', function ($compile, $http, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                $scope.countTotal = function () {};
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'rentacar', 'noresults', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('rentacarresults', ['$compile', '$http', 'resourceLoader', function ($compile, $http, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {},
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'rentacar', 'results', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);
}(window.triptop, window.jQuery));