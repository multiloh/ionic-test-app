'use strict';

(function (triptop, $) {
    if (!triptop || !triptop.options || !triptop.widgets || !triptop.api) {
        throw new Error('Application not configured');
    }

    var options = triptop.options;
    var widgets = triptop.widgets;
    var api = triptop.api;
    // var insurance = api.insurance;

    triptop.insurance = {
        scope: null
    };

    widgets.directive('insurance', ['$compile', '$http', '$timeout', 'resourceLoader', function ($compile, $http, $timeout, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div id="triptop-insurance" class="triptop-container"></div>',
            controller: function ($scope, $element) {
                triptop.insurance.scope = $scope;

                $scope.insuranceSearchStage = 0;
                // $scope.imgUrlBase = options.paths.templates.transfers + "/../../img/";

                function _calculateAge(birthday) { // birthday is a date
                    var ageDifMs = Date.now() - birthday.getTime();
                    var ageDate = new Date(ageDifMs); // miliseconds from epoch
                    return Math.abs(ageDate.getUTCFullYear() - 1970);
                }

                $scope.coverTypes = [
                    'ChronicallyIll',
                    'UnderMedicalCare'
                ];

                $scope.countries = [
                    'Russia',
                    'USA',
                    'Canada',
                    'UK',
                    'Albania',
                    'Austria',
                    'Germany'
                ];

                $scope.cities = [
                    'Moscow',
                    'Saint Petersburg',
                    'Helsinki',
                    'New York',
                    'Samara'
                ];

                $scope.years = [
                    '1993',
                    '1994',
                    '1995',
                    '1996'
                ];

                $scope.query = {
                    underwriter: {
                        id: null,                       // Ignore - null
                        type: 'Adult',                  // Infant, Child, Student, Adult, Senior, Youth
                        title: 'Mister',                // Mister, Miss
                        firstName: null,
                        lastName: null,
                        middleName: null,               // Ignore - null
                        gender: 'Male',                 // Male, Female
                        city: null,
                        country: null,                  // Ignore - null
                        birthday: null,
                        phone: null,
                        mobilePhone: null,              // EXTRA FIELD
                        email: null,
                        address: null,
                        zipCode: null,
                        passportNumber: null,
                        passportIssueDate: null,        // Ignore - null
                        passportExpirationDate: null    // Ignore - null
                    },
                    startDate: null,
                    endDate: null,
                    options: [null],                    // OlderThan75,ChronicallyIll,UnderMedicalCare
                    paxes: [{
                        firstName: null,
                        lastName: null,
                        birthday: null,
                        divingFlag: false,              // EXTRA FIELD
                        divingStartDate: null,
                        divingEndDate: null,
                        competitionFlag: false,         // EXTRA FIELD
                        competitionStartDate: null,
                        competitionEndDate: null,
                        skiFlag: false,                 // EXTRA FIELD
                        skiStartDate: null,
                        skiEndDate: null,
                        challengingSports: false,
                        laptopFlag: false,              // EXTRA FIELD
                        laptop: {
                            model: 'Test',
                            year: null
                        }
                    }]
                };

                $scope.PriceRequest = function ($http) {
//                    var tmp = JSON.parse(JSON.stringify($scope.query));
                    var tmp = jQuery.extend(true, {}, $scope.query);

                    console.log(_calculateAge(tmp.paxes[0].birthday));

                    delete tmp.underwriter.mobilePhone;
                    for (var i = 0; i < tmp.paxes.length; i++) {
                        delete tmp.paxes[i].competitionFlag;
                        delete tmp.paxes[i].divingFlag;
                        delete tmp.paxes[i].skiFlag;
                        delete tmp.paxes[i].laptopFlag;
                    }

                    var params = {
                        preview: JSON.stringify(tmp, $scope.insuranceDateReplacer),
                        action: 'start'
                    };

                    var request = api.Request($http, params);
                    request.urlStart = "/insurance/preview";
                    return request;
                };

                $scope.addPerson = function () {
                    var person = {
                        lastName: null,
                        birthday: null,
                        divingFlag: false,
                        divingStartDate: null,
                        divingEndDate: null,
                        competitionFlag: false,
                        competitionStartDate: null,
                        competitionEndDate: null,
                        skiFlag: false,
                        skiStartDate: null,
                        skiEndDate: null,
                        challengingSportsFlag: false,
                        challengingSports: null,
                        laptopFlag: false,
                        laptop: {
                            model: null,
                            year: null
                        }
                    };

                    if ($scope.query.paxes.length < 5) {
                        $scope.query.paxes.push(person);
                    }
                };

                $scope.removePerson = function (row) {
                    if ($scope.query.paxes.length > 1) {
                        $scope.query.paxes.splice($scope.query.paxes.indexOf(row), 1);
                    }
                };

                $scope.runSearch = function () {
                    $scope.insuranceSearchStage = 1;
                };

                $scope.removeEnter = function ($event) {
                    $timeout(function () {
                        var target = angular.element($event.target);
                        if (target) {
                            var val = target.val(), length = val.length;
                            if (length > 0) {
                                target.val(val.substring(0, length - 1));
                            }
                        }
                    }, 100);
                };

                $scope.latinNameEnter = function ($event) {
                    var ch = $event.charCode;
                    if (ch !== 0 && ch !== 8 && ch !== 32 && (ch < 65 || ch > 122)) {
                        $scope.removeEnter($event);
                    }
                };

                $scope.changePaxSex = function (value) {
                    $scope.query.underwriter.gender = value;

                    if (value = 'Male') {
                        $scope.query.underwriter.title = 'Mister';
                    } else {
                        $scope.query.underwriter.title = 'Miss';
                    }
                };

                $scope.insuranceDateReplacer = function (key, value) {
                    // angular has some special fields starting with $, that should not go to server
                    if (typeof key === 'string' && key.charAt(0) === '$') {
                        return undefined;
                    }
                    if (this[key] instanceof Date) {
                        var nn = function (n) { return n < 10 ? '0' + n : n; };

                        return this[key].getFullYear() + '-' + nn(this[key].getMonth() + 1) + '-' + nn(this[key].getDate());
                    }
                    return value;
                };

                $scope.totalPrice = 0;
                $scope.showTotal = 2 + $scope.query.paxes.length;

                $scope.$watch('query', function (newVal, oldVal) {
                    $scope.showTotal = 2 + $scope.query.paxes.length;

                    if ($scope.query.options[0]) {
                        $scope.showTotal--;
                    }

                    if ($scope.query.startDate && $scope.query.endDate) {
                        $scope.showTotal--;
                    }

                    for (var i = 0; i < $scope.query.paxes.length; i++) {
                        if ($scope.query.paxes[i].firstName && $scope.query.paxes[i].lastName && $scope.query.paxes[i].birthday) {
                            $scope.showTotal--;

                            if ($scope.query.paxes[i].competitionFlag) {
                                $scope.showTotal++;

                                if ($scope.query.paxes[i].competitionStartDate &&
                                    $scope.query.paxes[i].competitionEndDate) {
                                    $scope.showTotal--;
                                }
                            }

                            if ($scope.query.paxes[i].divingFlag) {
                                $scope.showTotal++;

                                if ($scope.query.paxes[i].divingStartDate &&
                                    $scope.query.paxes[i].divingEndDate) {
                                    $scope.showTotal--;
                                }
                            }

                            if ($scope.query.paxes[i].skiFlag) {
                                $scope.showTotal++;

                                if ($scope.query.paxes[i].skiStartDate &&
                                    $scope.query.paxes[i].skiEndDate) {
                                    $scope.showTotal--;
                                }
                            }

                            if ($scope.query.paxes[i].challengingSports) {
                                console.log('paxes: ' + i + ' extreme');
                            }

                            if ($scope.query.paxes[i].laptopFlag) {
                                $scope.showTotal++;

                                if ($scope.query.paxes[i].laptop.year &&
                                    $scope.query.paxes[i].laptop.model) {
                                    $scope.showTotal--;
                                }
                            }
                        }
                    }

                    if ($scope.showTotal === 0) {
                        $scope.PriceRequest($http).send(function(resp) {
                            console.log(resp);
                        });
                    }

                }, true);
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'insurance', 'content', 'insurance.jquery.min', function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);

                    if(options.insurance && options.insurance.ready) {
                        options.insurance.ready();
                    }
                });
            }
        };
    }]);

    widgets.config(function (resourceLoaderProvider) {
        resourceLoaderProvider.addDependency('insurance.jquery.min', function (loader, success) {
            loader.css('libs', 'bootstrap/css/less/bootstrap' + options.css_suffix);
            loader.css('libs', 'bootstrap-combobox/css/bootstrap-combobox' + options.css_suffix);
            loader.css('libs', 'datepicker/css/bootstrap-datepicker');
            loader.css('libs', 'timepicker/css/jquery.timepicker');
            loader.css('insurance', 'insurance' + options.css_suffix);

            loader.get('templates', 'common', 'modal', null, function (template) {
                angular.element('body').append(template);
            });

            loader.js('libs', 'jquery/jquery.min', function () {
                var cnt = 3;
                var complete = function () {
                    cnt--;
                    if (cnt === 0 && success) { success(); }
                };
                loader.js('libs', 'datepicker/bootstrap-datepicker', complete);
                loader.js('libs', 'timepicker/jquery.timepicker', complete);
                loader.js('libs', 'bootstrap-combobox/js/bootstrap-combobox', complete);
            });
        });
    });

    widgets.directive('insurancesearch', ['$compile', '$http', 'resourceLoader', function ($compile, $http, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {

            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'insurance', 'search', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('insurancewaiting', ['$compile', '$http', 'resourceLoader', function ($compile, $http, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'insurance', 'waiting', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('checkPhone', ['$http', function($http) {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function($scope, $element, $attrs, $ctrl) {
                $scope.$watch($attrs.ngModel, function() {
                    if ($scope.query.underwriter.phone || $scope.query.underwriter.mobilePhone) {
                        $ctrl.$setValidity("phone", true);

                        if (!$scope.query.underwriter.phone) {
                            $scope.insuranceForm.phone.$setPristine();
                        }

                        if (!$scope.query.underwriter.mobilePhone) {
                            $scope.insuranceForm.mobile.$setPristine();
                        }
                    } else {
                        $scope.insuranceForm.phone.$setValidity("phone", false);
                        $scope.insuranceForm.mobile.$setValidity("phone", false);
                    }
                });
            }
        };
    }]);
}(window.triptop, window.jQuery));