'use strict';

/**
 * @file Implementation for regular flights service.
 * @author amglin@trip-top.com
 * @module triptop/flights
 */

(function (triptop) {
    if (!triptop || !triptop.options || !triptop.widgets) {
        throw new Error('Application not configured');
    }

    // Application settings, main module and utility namespace
    var options = triptop.options;
    var widgets = triptop.widgets;
    var api = triptop.api;

    /**
     * Namespace for regular flights service.
     * @namespace triptop.flights
     */
    triptop.flights = {
        scope: {}
    };

    /**
     * Tag directive <flights> to implement container for regular flights flow. Using template 'flights/templates/content.html'.
     * @member {angular.Directive} flights
     * @memberof triptop.widgets
     */
    widgets.directive('flights', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div id="triptop-flights" class="triptop-container"></div>',
            controller: function ($scope, $element) {
                triptop.flights.scope = $scope;

                $scope.flightOn = true;
                $scope.flightSearchOn = true;
                $scope.flightFlexibleOn = true;
                $scope.flightFilterOn = true;

                $scope.flightState = 'READY';

                $scope.imgDir = options.paths.basedir + 'flights/img/';

                $scope.airportsCached = {};
                $scope.airportsCodes = {};

                $scope.resultSet = undefined;
                $scope.filterSet = undefined;
                $scope.querySet = undefined;

                $scope.loadAirports = function (query, callback) {
                    var cached = $scope.airportsCached[query];
                    if (cached) {
                        callback(cached);
                    }
                    else {
                        services.$airports(query, function (data) {
                            for (var i = data.length - 1; i >= 0; i--) {
                                var item = data[i];
                                item.search = item.code + ' ' + item.location.name + ' ' + item.name;
                                $scope.airportsCodes[item.code] = item;
                            }
                            $scope.airportsCached[query] = data;
                            callback(data);
                        });
                    }
                };

                $scope.findAirports = function (value) {
                    return $scope.airportsCodes[value];
                };

                $scope.firstSegment = function (segments) {
                    return segments[0];
                };

                $scope.lastSegment = function (segments) {
                    return segments[segments.length - 1];
                };

                $scope.changeFlightState = function (state) {
                    if ($scope.flightState === state) { return; }
                    $scope.flightState = state;
                    widgets.safeApply($scope);
                };

                $scope.changeProcessState = function (state) {
                    if ($scope.processState === state) { return; }
                    $scope.processState = state;
                    widgets.safeApply($scope);
                };

                $scope.showRules = function ($event, product) {
                    var title = $event.target.innerText;
                    var rules = product.rules;
                    if(!rules) {
                        $scope.changeProcessState('WAITING');

                        services.$details(product, function (data) {
                            rules = data.fareRules;
                            rules = rules.replace(/\n/g, '<br>');
                            rules = rules.replace(/\t/g, '&nbsp;');
                            product.rules = rules;
                            $scope.changeProcessState('READY');
                            $scope.showRules($event, product);
                        }, function (code) {
                            $scope.changeProcessState('READY');
                            if(code === '201') {
                                triptop.flights.scope.corruptSearch();
                            }
                        });
                    }
                    else {
                        api.message(title, rules);
                    }
                };

                $scope.query = {
                    tripType: 2,
                    segments: [{}],
                    paxes: {
                        adults: 1,
                        children: 0,
                        infants: 0
                    },
                    stops: 2,
                    flightClass: 1,
                    flexible: false
                };

                if(options.flights && options.flights.start) {
                    var from = options.flights.start.from;
                    if(from) {
                        $scope.loadAirports(from, function(data) {
                            if(data && data.length > 0) {
                                $scope.query.segments[0].from = from;
                            }
                        });
                    }

                    var to = options.flights.start.to;
                    if(to) {
                        $scope.loadAirports(to, function(data) {
                            if(data && data.length > 0) {
                                $scope.query.segments[0].to = to;
                            }
                        });
                    }

                    var time = options.flights.start.time;
                    if(time) {
                        $scope.query.segments[0].time = new Date(time);
                    }

                    var round = options.flights.start.round;
                    if(round) {
                        $scope.query.segments[0].round = new Date(round);
                    }
                }
                else {
                    if(!triptop.dictionaries.ips) { triptop.dictionaries.ips = {}; }
                    if(!triptop.dictionaries.ips.airport) {
                        services.$ips(function (data) {
                            triptop.dictionaries.ips.airport = data;
                            $scope.query.segments[0].from = data.code;
                            widgets.safeApply($scope);
                        }, function () {
                            triptop.dictionaries.ips.airport = undefined;
                        });
                    }
                }

                if(!options.siteType) {
                    services.$config(function (data) {
                        $scope.siteType = options.siteType = data.affiliateSiteType;
                    }, function () {
                        $scope.siteType = options.siteType = 'PUBLIC_SITE';
                    });
                }
                else {
                    $scope.siteType = options.siteType;
                }
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'flights', 'content', 'flights.libs', function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);

                    if(options.flights && options.flights.ready) {
                        options.flights.ready();
                    }
                });
            }
        };
    }]);

    // Creates a new dependency 'flights.libs' for flights flow initialization
    widgets.config(function (resourceLoaderProvider) {
        resourceLoaderProvider.addDependency('flights.libs', function (loader, success) {
            loader.css('libs', 'bootstrap/css/less/bootstrap' + options.css_suffix);
            loader.css('libs', 'selectize/css/selectize.bootstrap3');
            loader.css('libs', 'datepicker/css/bootstrap-datepicker');
            loader.css('libs', 'bootstrap-slider/css/slider');
            loader.css('libs', 'elusive-iconfont/css/elusive-webfont');
            loader.css('flights', 'flights' + options.css_suffix);

            loader.get('templates', 'common', 'modal', null, function (template) {
                angular.element('body').append(template);
            });

            loader.js('libs', 'jquery/jquery.min', function () {
                var cnt = 4;
                var runner = function () {
                    cnt--;
                    if (cnt === 0 && success) { success(); }
                };
                loader.js('libs', 'bootstrap/js/bootstrap.min', runner);
                loader.js('libs', 'bootstrap-slider/js/bootstrap-slider', runner);
                loader.js('libs', 'datepicker/bootstrap-datepicker', runner);
                loader.js('libs', 'selectize/js/standalone/selectize', function () {
                    loader.js('libs', 'selectize/js/plugins/continue_editing', runner);
                });
            });
        });
    });

    /**
     * Tag directive <flightsearch> to implement search form for regular flights. Using template 'flights/templates/search/search.html'.
     * @member {angular.Directive} flightsearch
     * @memberof triptop.widgets
     */
     widgets.directive('flightsearch', ['$compile', '$timeout', 'resourceLoader', function ($compile, $timeout, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                $scope.onChangeTime = function () {
                    var segment = $scope.query.segments[0];
                    var time = segment.time;
                    var round = segment.round;

                    var elem = $element.find('input[name=round]');
                    elem.bootstrapDatepicker('setStartDate', time);
                    if (!round || time.getTime() > round.getTime()) {
                        segment.round = time;
                        elem.bootstrapDatepicker('_setDate', time);
                        elem.bootstrapDatepicker('update');
                    }
                };

                $scope.configAirports = function () {
                    return {
                        valueField: 'code',
                        searchField: 'search',
                        searchConjunction: 'or',
                        options: [],
                        create: false,
                        render: {
                            item: function(item, escape) {
                                return '<div class="tt-item">' +
                                        '<span class="tt-name">' + escape(item.location.name) + '</span>' +
                                        '<span class="tt-code">' + escape(item.code) + '</span>' +
                                    '</div>';
                            },
                            option: function(item, escape) {
                                return '<div>' +
                                        '<div class="tt-item">' +
                                            '<span class="tt-name">' + escape(item.location.name) + '</span>' +
                                            '<span class="tt-code">' + escape(item.code) + '</span>' +
                                        '</div>' +
                                        '<div class="tt-description">' + escape(item.name) + '</div>' +
                                    '</div>';
                            }
                        },
                        load: function (query, callback) {
                            if (query.length <= 2) {
                                return callback();
                            }
                            $scope.loadAirports(query, callback);
                        }
                    };
                };

                $scope.addSegment = function () {
                    var segments = $scope.query.segments, length = segments.length;
                    if(length === 9) {
                        return;
                    }
                    var last = $scope.query.segments[length - 1];
                    $scope.query.segments.push({from: last.to, time: last.time});
                };

                $scope.removeSegment = function (segment) {
                    var segments = $scope.query.segments;
                    for(var i = segments.length; i--;) {
                        if(segments[i] === segment) {
                            segments.splice(i, 1);
                        }
                    }
                };

                $scope.runSearch = function () {
                    var params = {
                        action: 'start'
                    };

                    var query = $scope.query, segment;
                    if(query.tripType < 3) {
                        segment = query.segments[0];

                        if(!segment.from || !segment.to || !segment.time) {
                            api.alert($element.find('.tt-dest-invalid').text());
                            return;
                        }

                        params.fromAirport = segment.from;
                        params.toAirport = segment.to;
                        params.departureDate = api.toISOString(segment.time, false, true);

                        if(query.tripType === 2) {
                            if(!segment.round) {
                                api.alert($element.find('.tt-date-invalid').text());
                                return;
                            }
                            params.returnDate = api.toISOString(segment.round, false, true);
                        }
                    }
                    else {
                        var segments = [];
                        for (var i = 0, length = query.segments.length; i < length; i++) {
                            segment = query.segments[i];
                            segments.push({
                                fromAirport: segment.from,
                                toAirport: segment.to,
                                departureDate: api.toISOString(segment.time, false, true)
                            });
                        }
                        params.segments = api.toJson({segments: segments});
                    }

                    var amount = $scope.$eval(query.paxes.adults + '+' + query.paxes.children + '+' + query.paxes.infants);
                    if(amount === 0) {
                        api.alert($element.find('.tt-zero-amount').text());
                        return;
                    }
                    else if(amount > 9) {
                        api.alert($element.find('.tt-more-amount').text());
                        return;
                    }

                    params.adults = query.paxes.adults;
                    params.children = query.paxes.children;
                    params.infants = query.paxes.infants;
                    params.flexDays = query.flexible ? 1 : 0;
                    params.maxStops = query.stops > 1 ? 10 : query.stops;
                    params.flightClass = ['', 'Economy', 'Premium', 'Standard', 'First', 'Business'][query.flightClass];
                    params.displayCurrency = 'USD';

                    var scope = triptop.flights.scope;

                    scope.resultSet = undefined;
                    scope.filterSet = undefined;

                    if(scope.querySet && scope.querySet.promise) {
                        $timeout.cancel(scope.querySet.promise);
                    }
                    scope.querySet = angular.copy(query);

                    services.$search(params, function (data) {
                        scope.filterSet = data.digest;

                        if (!data.visibleProducts || data.visibleProducts.length === 0) {
                            api.alert($element.find('.tt-notfound').text());
                            scope.changeFlightState('READY');
                        }
                        else {
                            if(options.siteType === 'TERMINAL') {
                                data.pagination = { amount: Math.ceil(data.total / 10), current: 0, limit: 10 };
                            }
                            else {
                                data.pagination = { amount: 1, current: 0, limit: data.total };
                            }
                            scope.resultSet = data;
                            scope.changeFlightState('RESULTS');

                            triptop.flights.scope.querySet.promise = $timeout(function () {
                                triptop.flights.scope.querySet.corrupt = function () {
                                    triptop.flights.scope.corruptSearch();
                                };
                            }, 15*60*1000);
                        }
                    }, function () {
                        api.alert($element.find('.tt-notfound').text());
                        scope.changeFlightState('READY');
                    });

                    scope.changeFlightState('SEARCHING');
                };

                triptop.flights.scope.corruptSearch = function () {
                    api.alert($element.find('.tt-corrupt').text(), function () {
                        $scope.runSearch();
                        widgets.safeApply($scope);
                    });
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'flights', 'search/search', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    /**
     * Filter 'flightsQuerySegments' to create segments array.
     * @function flightsQuerySegments
     * @memberof triptop.widgets
     * @param {object} query Search query.
     * @return {angular.Filter} Returns filter 'flightsQuerySegments'.
     */
    widgets.filter('flightsQuerySegments', function() {
        return function(query) {
            if(typeof query.tripType === 'string') {
                query.tripType = parseInt(query.tripType, 10);
            }
            if(query.tripType === 3) {
                return query.segments;
            }
            return [query.segments[0]];
        };
    });

    /**
     * Tag directive <flightsearchwait> to implement waiting search results form for regular flights. Using template 'flights/templates/search/wait.html'.
     * @member {angular.Directive} flightsearchwait
     * @memberof triptop.widgets
     */
    widgets.directive('flightsearchwait', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                var segments = $scope.query.segments, segment = segments[0];
                var type = $scope.query.tripType, airports, times;
                if(type === 1) {
                    airports = [$scope.findAirports(segment.from), $scope.findAirports(segment.to)];
                    times = [segment.time];
                }
                else if(type === 2) {
                    airports = [$scope.findAirports(segment.from), $scope.findAirports(segment.to)];
                    times = [segment.time, segment.round];
                }
                else if(type === 3) {
                    airports = [];
                    times = [];
                    for (var i = 0, length = segments.length; i < length; i++) {
                        segment = segments[i];
                        airports.push($scope.findAirports(segment.from));
                        times.push(segment.time);
                    }
                }

                $scope.digest = {
                    airports: airports,
                    times: times,
                    tripType: type
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'flights', 'search/wait', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    /**
     * Tag directive <flightresult> to implement search results container for regular flights. Using template 'flights/templates/results/result.html'.
     * @member {angular.Directive} flightresult
     * @memberof triptop.widgets
     */
    widgets.directive('flightresult', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                $scope.displayedSimilarProduct = undefined;

                $scope.changeSimilarState = function (product, state) {
                    if(product) {
                        product.similarState = state;
                    }
                    $scope.displayedSimilarProduct = product;
                    widgets.safeApply($scope);
                };

                $scope.showDetails = function (product) {
                    product.showDetails = true;
                };

                $scope.hideDetails = function (product) {
                    product.showDetails = false;
                };

                $scope.loadPage = function (index) {
                    var pagination = $scope.resultSet.pagination;
                    if(!pagination) {
                        return;
                    }

                    if(index === undefined) {
                        index = pagination.amount - 1;
                    }

                    if(pagination.current === index) {
                        return;
                    }

                    $scope.changeProcessState('WAITING');

                    services.$page($scope.resultSet, index, function (data) {
                        pagination = $scope.resultSet.pagination;
                        data.pagination = {
                            amount: pagination.amount,
                            current: index,
                            limit: pagination.limit
                        };
                        triptop.flights.scope.resultSet = data;
                        $scope.changeProcessState('READY');
                    }, function () {
                        $scope.changeProcessState('READY');
                    });
                };

                $scope.showSimilars = function (product, pagination) {
                    if($scope.displayedSimilarProduct && $scope.displayedSimilarProduct !== product) {
                        $scope.hideSimilars($scope.displayedSimilarProduct, pagination);
                    }

                    $scope.changeSimilarState(product, 'WAITING');

                    services.$similar($scope.resultSet, product, function (data) {
                        product.similars = data.visibleProducts;
                        product.scrollY = $element.find('[data-id=' + product.id + ']').offset().top - 170;

                        $scope.changeSimilarState(product, 'READY');

                        angular.element('html, body').animate({ scrollTop: product.scrollY }, 500);
                        $element.find('.tt-similar').addClass('tt-slide');
                    }, function () {
                        $scope.changeSimilarState(product, 'READY');
                    });
                };

                $scope.reloadSimilars = function (product) {
                    $scope.displayedSimilarProduct = product;
                    $scope.showSimilars(product);
                };

                $scope.loadSimilarPage = function (index) {
                    if(!$scope.displayedSimilarProduct) {
                        return;
                    }

                    var product = $scope.displayedSimilarProduct;
                    var pagination = product.pagination;
                    if(!pagination) {
                        return;
                    }

                    if(index === undefined) {
                        index = pagination.amount - 1;
                    }

                    if(pagination.current === index) {
                        return;
                    }
                    
                    pagination.current = index;
                    $scope.showSimilars(product, true);
                };

                $scope.hideSimilars = function (product, pagination) {
                    $element.find('.tt-similar').removeClass('tt-slide');
                    if(product.scrollY) {
                        angular.element('html, body').animate({ scrollTop: product.scrollY }, 500);
                    }

                    if(!pagination) {
                        delete product.pagination;
                    }
                    $scope.displayedSimilarProduct = undefined;
                };

                $scope.switchGdsGroup = function (product, group) {
                    if(product.gds === group.gds) {
                        return;
                    }

                    product.id = group.id;
                    product.gds = group.gds;
                    product.onlinePrice = group.price;
                };

                $scope.runBooking = function (product) {
                    var querySet = $scope.querySet;
                    if(querySet && querySet.corrupt) {
                        querySet.corrupt();
                    }
                    else {
                        triptop.flights.scope.bookingProduct = product;
                        $scope.changeProcessState('WAITING');
                        $scope.changeFlightState('BOOKING');
                    }
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'flights', 'results/result', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    /**
     * Filter 'flightsVisibleProducts' to create array of visible products.
     * @function flightsVisibleProducts
     * @memberof triptop.widgets
     * @param {object} row Regular flight object.
     * @return {angular.Filter} Returns filter 'flightsVisibleProducts'.
     */
    widgets.filter('flightsVisibleProducts', function() {
        return function (product, displayedSimilarProduct) {
            if(displayedSimilarProduct && displayedSimilarProduct.validatingCarrier.code === product.validatingCarrier.code) {
                product.displayedSimilarProduct = true;
                return product.similars;
            }
            else {
                product.displayedSimilarProduct = false;
                return [product];
            }
        };
    });

    /**
     * Tag directive <flightresultrow> to implement search results item for regular flights. Using template 'flights/templates/results/row.html'.
     * @member {angular.Directive} flightresultrow
     * @memberof triptop.widgets
     */
    widgets.directive('flightresultrow', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'flights', 'results/row', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    /**
     * Filter 'flightsRow' to create array of any segments.
     * @function flightsRow
     * @memberof triptop.widgets
     * @param {object} row Regular flight object.
     * @return {angular.Filter} Returns filter 'flightsRow'.
     */
    widgets.filter('flightsRow', function() {
        return function(row) {
            return row.in && row.in.length > 0 ? [row.out, row.in] : [row.out];
        };
    });

    /**
     * Filter 'flightsSeatsAmount' to calculate seats amount.
     * @function flightsSeatsAmount
     * @memberof triptop.widgets
     * @param {object} row Regular flight object.
     * @return {angular.Filter} Returns filter 'flightsSeatsAmount'.
     */
    widgets.filter('flightsSeatsAmount', function() {
        return function(row) {
            var i, seats, amount = 0;
            for (i = 0; i < row.out.length; i++) {
                seats = row.out[i].seats;
                if(amount === 0 || amount > seats) {
                    amount = seats;
                }
            }
            if(row.in) {
                for (i = 0; i < row.in.length; i++) {
                    seats = row.in[i].seats;
                    if(amount === 0 || amount > seats) {
                        amount = seats;
                    }
                }
            }
            return amount;
        };
    });

    /**
     * Filter 'flightsChangesDetail' to display of transit destination.
     * @function flightsChangesDetail
     * @memberof triptop.widgets
     * @param {object} segments Flight segment object.
     * @return {angular.Filter} Returns filter 'flightsChangesDetail'.
     */
    widgets.filter('flightsChangesDetail', function() {
        return function(segments) {
            var value = '';
            if(segments.length < 2) {
                return value;
            }
            for (var i = 1, length = segments.length; i < length; i++) {
                value += ', ' + segments[i].departure.airport.location.name;
            }
            return value.substr(2);
        };
    });

    /**
     * Filter 'flightsDuration' to parse duration.
     * @function flightsDuration
     * @memberof triptop.widgets
     * @param {string} value Duration in ISO value.
     * @return {angular.Filter} Returns filter 'flightsDuration'.
     */
    widgets.filter('flightsDuration', function() {
        return function(value) {
            var str = '';
            if(!value) {
                return str;
            }

            var match = /PT((\d*)H)?((\d*)M)?/.exec(value);
            if (match[2]) {
                value = parseInt(match[2], 10);
                str = (value < 10 ? '0' : '') + value;
            }
            else {
                str = '00';
            }
            
            if (match[4]) {
                value = parseInt(match[4], 10);
                str += (':' + (value < 10 ? '0' : '') + value);
            }
            else {
                str += ':00';
            }
            return str;
        };
    });

    /**
     * Tag directive <flightresultrowdetails> to implement item details for regular flights. Using template 'flights/templates/results/details.html'.
     * @member {angular.Directive} flightresultrowdetails
     * @memberof triptop.widgets
     */
    widgets.directive('flightresultrowdetails', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'flights', 'results/details', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    /**
     * Tag directive <flightflextable> to implement flexible table for regular flights. Using template 'flights/templates/results/flextable.html'.
     * @member {angular.Directive} flightflextable
     * @memberof triptop.widgets
     */
    widgets.directive('flightflextable', ['$compile', '$timeout', 'resourceLoader', function ($compile, $timeout, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                $scope.selectedDate = undefined;

                $scope.flexDatePrices = $scope.resultSet.digest.flexDatePrices;
                if(!$scope.flexDatePrices) {
                    $scope.flexDatePrices = [];
                }

                $scope.selectFlexDate = function (index) {
                    if($scope.selectedDate === index) {
                        return;
                    }

                    var date = $scope.flexDatePrices[index];
                    if(!date || date.price === 0) {
                        return;
                    }

                    $scope.selectedDate = index;
                    $scope.changeProcessState('WAITING');
                    $scope.changeSimilarState();

                    services.$supplement($scope.resultSet, date, function (data) {
                        if (!data.visibleProducts || data.visibleProducts.length === 0) {
                            api.alert($element.find('.tt-notfound').text());
                            $scope.changeProcessState('READY');
                            return;
                        }

                        if(options.siteType === 'TERMINAL') {
                            data.pagination = { amount: Math.ceil(data.total / 10), current: 0, limit: 10 };
                        }
                        else {
                            data.pagination = { amount: 1, current: 0, limit: data.total };
                        }
                        triptop.flights.scope.resultSet = data;

                        data.digest.flexDate = date;
                        triptop.flights.scope.filterSet = data.digest;

                        $scope.changeFlightState('RESULTS');
                        $scope.changeProcessState('READY');

                        var querySet = triptop.flights.scope.querySet;
                        if(querySet && querySet.promise) {
                            $timeout.cancel(querySet.promise);
                        }

                        triptop.flights.scope.querySet.promise = $timeout(function () {
                            triptop.flights.scope.querySet.corrupt = function () {
                                triptop.flights.scope.corruptSearch();
                            };
                        }, 15*60*1000);
                    }, function () {
                        api.alert($element.find('.tt-notfound').text());
                        $scope.changeProcessState('READY');
                    });
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'flights', 'results/flextable', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    // Parse flights time from minutes
    var timeFormater = function (value) {
        var v, str = '';

        v = Math.floor(value / 60);
        str = v < 10 ? ('0' + v) : v;

        v = Math.round(value % 60);
        str += ':' + (v < 10 ? ('0' + v) : v);

        return str;
    };

    // Parse flights duration from minutes
    var durationFormater = function (value) {
        if(value === 1439) {
            return undefined;
        }

        var v, str = 'PT';

        v = Math.floor(value / 60);
        str += v < 10 ? ('0' + v) : v;

        v = Math.round(value % 60);
        str += 'H' + (v < 10 ? ('0' + v) : v) + 'M';

        return str;
    };

    /**
     * Tag directive <flightfilter> to implement filter panel for regular flights. Using template 'flights/templates/results/filter.html'.
     * @member {angular.Directive} flightfilter
     * @memberof triptop.widgets
     */
    widgets.directive('flightfilter', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                var filterApply = function () {
                    $scope.changeProcessState('WAITING');
                    services.$page($scope.resultSet, 0, function (data) {
                        if(options.siteType === 'TERMINAL') {
                            data.pagination = { amount: Math.ceil(data.total / 10), current: 0, limit: 10 };
                        }
                        else {
                            data.pagination = { amount: 1, current: 0, limit: data.total };
                        }
                        triptop.flights.scope.resultSet = data;

                        var similar = $scope.displayedSimilarProduct;
                        if(similar) {
                            var products = data.visibleProducts, product;
                            if(products) {
                                for (var i = 0, s = products.length; i < s; i++) {
                                    var p = products[i];
                                    if(p.validatingCarrier.code === similar.validatingCarrier.code) {
                                        product = p;
                                        break;
                                    }
                                }
                            }

                            if(product) {
                                delete product.pagination;
                                $scope.reloadSimilars(product);
                                $scope.changeProcessState('READY');
                            }
                            else {
                                $scope.changeProcessState('READY');
                            }
                        }
                        else {
                            $scope.changeProcessState('READY');
                        }
                    },
                    function () {
                        $scope.changeProcessState('READY');
                    });
                };

                $scope._setSelectedAll = function () {
                    var filterSet = $scope.filterSet;
                    for (var i = 0; i < filterSet.airlines.length; i++) {
                        filterSet.airlines[i].selected = filterSet.airlines.selected;
                    }
                };

                $scope.setSelectedAll = function () {
                    $scope._setSelectedAll();
                    if($scope.filterSet.airlines.selected) {
                        filterApply();
                    }
                };

                $scope.setSelected = function (airline) {
                    var airlines = $scope.filterSet.airlines;
                    if(airline.selected) {
                        var i, size = airlines.length;
                        for (i = 0; i < size; i++) {
                            if(!airlines[i].selected) {
                                break;
                            }
                        }
                        airlines.selected = i === size;
                    }
                    else {
                        airlines.selected = false;
                    }
                    filterApply();
                };

                $scope.setStops = function (value) {
                    filterApply();
                };

                $scope.setSort = function (value) {
                    filterApply();
                };

                $scope.configPrice = function () {
                    var config = {
                        min: Math.floor($scope.filterSet.minPrice),
                        max: Math.ceil($scope.filterSet.maxPrice)
                    };
                    config.value = [config.min, config.max];
                    $scope.filterSet.price = config.value;
                    return config;
                };

                $scope.changePrice = function (event, type) {
                    if(type == 'stop') {
                        filterApply();
                    }
                    else {
                        $scope.filterSet.price = event.value;
                        widgets.safeApply($scope);
                    }
                };                

                var timeConfig = {
                    min: 0,
                    max: 24 * 60 - 1,
                    formater: timeFormater
                };
                timeConfig.value = [timeConfig.min, timeConfig.max];

                $scope.configDeparture = function () {
                    var config = angular.copy(timeConfig);
                    $scope.filterSet.departure = config.value;
                    return config;
                };

                $scope.changeDeparture = function (event, type) {
                    if(type == 'stop') {
                        filterApply();
                    }
                    else {
                        $scope.filterSet.departure = event.value;
                        widgets.safeApply($scope);
                    }
                };                

                $scope.configArrival = function () {
                    var config = angular.copy(timeConfig);
                    $scope.filterSet.arrival = config.value;
                    return config;
                };

                $scope.changeArrival = function (event, type) {
                    if(type == 'stop') {
                        filterApply();
                    }
                    else {
                        $scope.filterSet.arrival = event.value;
                        widgets.safeApply($scope);
                    }
                };                

                $scope.configDuration = function () {
                    var config = angular.copy(timeConfig);
                    $scope.filterSet.duration = config.value;
                    return config;
                };

                $scope.changeDuration = function (event, type) {
                    if(type == 'stop') {
                        filterApply();
                    }
                    else {
                        $scope.filterSet.duration = event.value;
                        widgets.safeApply($scope);
                    }
                };              
            },
            link: function ($scope, $element, $attrs) {
                $scope.$watch('filterSet', function (filter) {
                    if(filter.airlines.selected === undefined) {
                        filter.stops = '2';
                        filter.sort = 'price';
                        filter.airlines.selected = true;
                        $scope._setSelectedAll();
                    }

                    resourceLoader.get('templates', 'flights', 'results/filter', null, function (template) {
                        $element.html(template);
                        $compile($element.contents())($scope);
                    });
                });

                var top, fixedOn;
                var handler = function () {
                    if(top === undefined) {
                        top = $element.offset().top;
                    }
                    if(window.scrollY > top) {
                        if(!fixedOn) {
                            $element.addClass('tt-fixed-on').parent().addClass('tt-fixed-filter');
                            fixedOn = true;
                        }
                    }
                    else if(fixedOn) {
                       $element.removeClass('tt-fixed-on').parent().removeClass('tt-fixed-filter');
                       fixedOn = false;
                    }
                };
                angular.element(window).bind('scroll', handler);

                $scope.$on('$destroy', function () {
                    angular.element(window).unbind('scroll', handler);
                });
            }
        };
    }]);

    /**
     * Filter 'flightsTimeFormater' to parse flights time from minutes.
     * @function flightsTimeFormater
     * @memberof triptop.widgets
     * @param {string} value Duration in ISO value.
     * @return {angular.Filter} Returns filter 'flightsTimeFormater'.
     */
    widgets.filter('flightsTimeFormater', function() {
        return timeFormater;
    });

    /**
     * Service to communicate with the server side.
     * @memder services
     * @memberof triptop.flights
     * @property {function} $airports Getting list of airports.
     * @property {function} $search Running the search query.
     * @property {function} $supplement Running query by date.
     * @property {function} $similar Running the search query similar data.
     * @property {function} $page Running query to retrieve data for specific page.
     * @property {function} $details Running query for flight details.
     */
    var services = {
        options: {
            path: '//82.80.216.47:8080/triptop-fo-api-war/api'
//            path: '//testsrv.trip-top.com:8888/triptop-fo-api-war/api'
        },
        params: {
            locale: options.locale ? options.locale.substr(0, 2).toUpperCase() : 'EN'
        },
        $ips: function (success, error) {
            api._Request('/ips/airport', this.params, this.options.path).send(success, error);
        },
        $config: function (success, error) {
            api._Request('/affiliates/sites/' + options.key + '/info', this.params, this.options.path).send(success, error);
        },
        $airports: function (query, success, error) {
            api._Request('/flights/dictionaries/airports', angular.extend({
                query: encodeURIComponent(query)
            }, this.params), this.options.path).send(success, error);
        },
        $search: function (params, success, error) {
            var _this = this;
            if(options.siteType === 'TERMINAL') {
                params.exclude = 'none';
            }

            api._Request('/flights/searches', angular.extend(params, this.params), this.options.path).send(function (data) {
                var id = data.id;

                api._Request('/flights/searches/' + id + '/raw', angular.extend({
                    offset: 0,
                    limit: 1
                }, _this.params), _this.options.path).polling(function (data) {
                    return data.complete;
                },
                function (data) {
                    api._Request('/flights/searches/' + id + '/processed', angular.extend({
                        fold: options.siteType === 'TERMINAL'? 'gds' : 'airline',
                        offset: 0,
                        limit: options.siteType === 'TERMINAL'? 10 : data.total
                    }, _this.params), _this.options.path).send(success, error);
                },
                error);
            });
        },
        $supplement: function (resultSet, date, success, error) {
            var _this = this;
            api._Request('/flights/searches/' + resultSet.id, angular.extend({
                action: 'supplement',
                departureDate: date.departure,
                returnDate: date.arrival
            }, _this.params), _this.options.path).polling(function (data) {
                return data.complete;
            },
            function (data) {
                api._Request('/flights/searches/' + resultSet.id + '/processed', angular.extend({
                    fold: options.siteType === 'TERMINAL'? 'gds' : 'airline',
                    departureDate: date.departure,
                    returnDate: date.arrival,
                    offset: 0,
                    limit: options.siteType === 'TERMINAL'? 10 : data.total
                }, _this.params), _this.options.path).send(success, error);
            },
            error);
        },
        $similar: function (resultSet, product, success, error) {
            var pagination = product.pagination;
            if(!pagination) {
                pagination = { current: 0, limit: 5, amount: Math.ceil(product.similarProductGroup.groupSize / 5) };
                product.pagination = pagination;
            }

            var filterSet = triptop.flights.scope.filterSet;
            var filter = {
                offset: pagination.current * pagination.limit,
                limit: pagination.limit,
                minPrice: filterSet.price[0],
                maxPrice: filterSet.price[1],
                minDeparture: timeFormater(filterSet.departure[0]),
                maxDeparture: timeFormater(filterSet.departure[1]),
                minArrival: timeFormater(filterSet.arrival[0]),
                maxArrival: timeFormater(filterSet.arrival[1]),
                maxOutDuration: durationFormater(filterSet.duration[1]),
                maxInDuration: durationFormater(filterSet.duration[1]),
                minStops: filterSet.stops === '2' ? 0 : filterSet.stops,
                maxStops: filterSet.stops,
                sort: filterSet.sort + ':asc'
            };

            var date = filterSet.flexDate;
            if(date) {
                filter.departureDate = date.departure;
                filter.returnDate = date.arrival;
            }

            filter['airlines[]'] = product.validatingCarrier.code;

            api._Request('/flights/searches/' + resultSet.id + '/processed', angular.extend(filter, this.params), this.options.path).send(success, error);
        },
        $page: function (resultSet, index, success, error) {
            var filterSet = triptop.flights.scope.filterSet;
            var filter = {
                fold: options.siteType === 'TERMINAL'? 'gds' : 'airline',
                offset: index * resultSet.pagination.limit,
                limit: resultSet.pagination.limit,
                minPrice: filterSet.price[0],
                maxPrice: filterSet.price[1],
                minDeparture: timeFormater(filterSet.departure[0]),
                maxDeparture: timeFormater(filterSet.departure[1]),
                minArrival: timeFormater(filterSet.arrival[0]),
                maxArrival: timeFormater(filterSet.arrival[1]),
                maxOutDuration: durationFormater(filterSet.duration[1]),
                maxInDuration: durationFormater(filterSet.duration[1]),
                minStops: filterSet.stops === '2' ? 0 : filterSet.stops,
                maxStops: filterSet.stops,
                sort: filterSet.sort + ':asc'
            };

            var date = filterSet.flexDate;
            if(date) {
                filter.departureDate = date.departure;
                filter.returnDate = date.arrival;
            }

            var airlines = filterSet.airlines;
            if(!airlines.selected) {
                var str = '';
                for (var i = 0; i < airlines.length; i++) {
                    var item = airlines[i];
                    if(item.selected) {
                        str += ',' + item.airline.code;
                    }
                }
                filter['airlines[]'] = str.substr(1);
            }

            api._Request('/flights/searches/' + resultSet.id + '/processed', angular.extend(filter, this.params), this.options.path).send(success, error);
        },
        $details: function (product, success, error) {
            api._Request('/flights/products/' + product.id, this.params, this.options.path).polling(function (data) {
                return data.complete === undefined || data.complete === true;
            }, success, error);
        }
    };
}(window.triptop));