'use strict';

(function (triptop) {
    if (!triptop || !triptop.options || !triptop.widgets || !triptop.api) {
        throw new Error('Application not configured');
    }

    var options = triptop.options;
    var widgets = triptop.widgets;
    var api = triptop.api;
    var cart = api.cart;
    var payment = api.paymentWizard;

    triptop.booking = {
        scope: null,
        init: function (booking, bookingCallback, checkCallback, cancelCallback, finalCallback, rulesCallback) {
            this.bookingCallback = bookingCallback;
            this.checkCallback = checkCallback;
            this.cancelCallback = cancelCallback;
            this.finalCallback = finalCallback;
            this.rulesCallback = rulesCallback;

            this._init(booking);
        },
        show: function (styles) {
            angular.element('#triptop-booking').css(styles).show();
        },
        hide: function () {
            angular.element('#triptop-booking').hide();
        },
        _init: function (booking) {
            var scope = triptop.booking.scope;
            if (!scope || !booking || !booking.products || booking.products.length === 0) {
                return;
            }

            booking.agree = false;
            booking.type = 'creditcard'; // 'reserve' 'creditcard';
            booking.cardOwner = '0';
            booking.paxes = [];
            booking.person = {sex: 'male'};
            booking.prices.agent = 0;

            scope.booking = booking;
            delete scope.inprocess;

            var products = booking.products;
            for (var i = 0; i < products.length; i++) {
                var product = products[i];
                var type = product.type.toLowerCase() + 's'; // TODO
                var service = booking[type];
                if (service && service.products) {
                    service.products.push(product);
                    continue;
                }
                if (!service) {
                    service = {};
                    booking[type] = service;
                }
                service.products = [product];

                if (type === 'flights' || type === 'charters' || type === 'packages' || type === 'lowcosters') {
                    booking.hasFlights = true;
                    if (type === 'flights') {
                        booking.hasRegularFlights = true;
                    }
                    else {
                        booking.paymentUnavailable = true;
                    }
                }
                else if (type === 'transfers') {
                    booking.hasTransfers = true;
                }
            }

            if (booking.paymentUnavailable) {
                booking.type = 'reserve';
            }

            booking.siteType = options.siteType;
            if(!booking.siteType) {
                booking.siteType = window.location.hostname === 'il.trip-top.com' ? 4 : 1;
            }

            this._service('init', function () {
                widgets.safeApply(scope, function () {
                    scope.bookingStage = 0;
                    scope.gotoStage(1);
                    angular.element('html,body').scrollTop(angular.element('#triptop-booking').offset().top);
                });
            });
        },
        _sendPooling: function (vsId, sessionId, error) {
            if (vsId && sessionId) {
                var scope = triptop.booking.scope;
                var booking = scope.booking;
                var inprocess = scope.inprocess;

                booking.vsId = parseInt(vsId, 10);

                var injector = angular.injector(["ng"]);
                var BOOKING_POLL_TIMEOUT = 1000;
                var wrapper = function($http) {
                    payment.QueryBookingRequest($http, sessionId, vsId,
                        function (resp) {
                            var docketId = resp.value.dtid;
                            var request = payment.PnrPollRequest($http, sessionId, vsId, resp.value.searchId,
                                function (resp) {
                                    var state = resp.value.id;
                                    switch (state) {
                                        case 3:
                                        case 8:
                                            if(triptop.payment && triptop.payment.scope) {
                                                if (!inprocess.docketId) {
                                                    triptop.payment.scope.loadDocket(docketId);
                                                    inprocess.docketId = parseInt("" + docketId, 10);
                                                    booking.docketId = inprocess.docketId;
                                                }
                                            }
                                            else {
                                                scope.showError('SYSTEM_ERROR');
                                                if(error) { error(); }
                                            }
                                            setTimeout(function () {
                                                request.send();
                                            }, BOOKING_POLL_TIMEOUT);
                                            break;
                                        case 2:
                                            setTimeout(function () {
                                                request.send();
                                            }, BOOKING_POLL_TIMEOUT);
                                            break;
                                        case undefined:
                                        case 4:
                                        case 5:
                                            break;
                                        default:
                                            break;
                                    }
                                },
                                function (e) {
                                    scope.showError('BOOKING_ONLINE_ERROR');
                                    if(error) { error(); }
                                }
                            );
                            request.send();
                        }
                    ).send();
                };
                injector.invoke(wrapper);
            }
        },
        _service: function (action, success, interrupt, error) {
            var booking = this.scope.booking;
            if (!booking) {
                return;
            }

            var services = ['flights', 'charters', 'packages', 'hotels', 'transfers'];
            for (var i = 0; i < services.length; i++) {
                var service = services[i];
                if (booking[service] && booking[service].products && triptop.booking[service]) {
                    var method = triptop.booking[service][action];
                    if (method && angular.isFunction(method)) {
                        method(success, interrupt, error);
                    }
                }
            }
        }
    };

    widgets.directive('booking', ['$compile', '$http', '$timeout', 'resourceLoader', function ($compile, $http, $timeout, resourceLoader) {
        return {
            restrict: 'AE',
            scope: true,
            replace: true,
            template: '<div id="triptop-booking" class="triptop-container"></div>',
            controller: function ($scope, $element) {
                triptop.booking.scope = $scope;

                $scope.bookingStage = 0;
                $scope.bookingImgUrlBase = options.paths.templates.booking + "/../../img/";
                $scope.captchaUrl = options.paths.services.main + "/captcha";

                $scope.refreshCaptcha = function() {
                    var captReq = api.GetCaptchaRequest($http, 192, 48);
                    captReq.send(function(resp){
                        $scope.booking.captchaId = resp.id;
                    });
                };

                $scope.toggleAgreement = function () {
                    $scope.booking.agree = !$scope.booking.agree;
                };

                $scope.showTermsView = function (id) {
                    angular.element('.tt-booking-view-terms').hide();
                    angular.element(id).show();
                };

                $scope.gotoStage = function (stage) {
                    if ($scope.bookingStage === stage) {
                        return;
                    }

                    $scope.bookingStage = stage;

                    if (stage < 0) {
                        triptop.booking._service('terminate');
                        $scope.bookingStage = 0;
                    }
                    else if (stage === 0) {
                        triptop.booking._service('cancel');
                    }
                    else if (stage === 1) {
                        if (triptop.profile) {
                            triptop.profile.check($scope.profileStageCallback);
                        }
                        $scope.refreshCaptcha();
                    }
                    else if (stage === 2) {
                        if (triptop.profile) {
                            triptop.profile.check($scope.profileStageCallback);
                        }
                    }
                    else if (stage === 3) {
                        var inprocess = angular.copy($scope.booking);

                        if (inprocess.prices.agent) {
                            inprocess.prices.agent = parseInt(inprocess.prices.agent, 10) * 100;
                        }
                        inprocess.prices.fare = inprocess.help.prices.fare;
                        inprocess.prices.tax = inprocess.help.prices.tax;
                        inprocess.prices.options = inprocess.help.prices.options;
                        inprocess.prices.discount = inprocess.help.prices.discount;
                        inprocess.prices.currency = inprocess.help.prices.currency;
                        inprocess.prices.total = inprocess.help.prices.fare +
                            inprocess.help.prices.tax +
                            inprocess.help.prices.options +
                            inprocess.help.prices.discount +
                            (inprocess.prices.agent ? inprocess.prices.agent : 0) +
                            (inprocess.type === 'creditcard' && inprocess.prices.creditcard ? inprocess.prices.creditcard : 0);
                        delete inprocess.help;

                        $scope.inprocess = inprocess;

                        var goBookingUrl = function (type) {
                            if (window.triptopApi && window.triptopApi.config) {
                                var url = window.triptopApi.config['booking' + type + 'Url'];
                                if(url && url.length > 0) {
                                    var title = $element.find('.tt-warning').text();
                                    var message = type === 'Success' ? $element.find('.tt-success').text() : $element.find('.tt-fail').text();
                                    api.message(title, message, function() {
                                        try { window.open(url, '_blank').focus(); } catch (e) {}
                                    });
                                }
                            }
                        };

                        triptop.booking._service('process', function () {
                            goBookingUrl('Success');
                            $scope.gotoStage(4);
                            widgets.safeApply($scope);
                        }, function (code) {
                            if (code == '209') {
                                angular.element('#captchaId').addClass('ng-dirty').addClass('ng-invalid');
                                api.alert(api.getErrorLocalizedMessage(10));
                                $scope.refreshCaptcha();
                            }
                            $scope.gotoStage(2);
                            widgets.safeApply($scope);
                        }, function () {
                            goBookingUrl('Fail');
                            $scope.gotoStage(-1);
                            widgets.safeApply($scope);
                        });
                    }
                    else if (stage === 4) {
                        if (triptop.profile && triptop.profile.scope && triptop.profile.scope.profile) {
                            var id = triptop.profile.scope.profile.id;
                            if (id !== undefined) {
                                payment.AssignDocketRequest($http, $scope.inprocess.docketId, id).send();
                            }
                            triptop.profile.fire('bookingSuccess', null, $scope.inprocess);
                        }
                        triptop.booking._service('final');
                    }
                };

                $scope.removeEnter = function ($event) {
                    $timeout(function () {
                        var target = angular.element($event.target);
                        if (target) {
                            var val = target.val(), length = val.length;
                            if (length > 0) {
                                target.val(val.substring(0, length - 1));
                            }
                        }
                    }, 100);
                };

                $scope.latinNameEnter = function ($event) {
                    var ch = $event.charCode;
                    if (ch !== 0 && ch !== 8 && ch !== 32 && (ch < 65 || ch > 122)) {
                        $scope.removeEnter($event);
                    }
                };

                $scope.dateEnter = function ($event) {
                    var ch = $event.charCode;
                    if (ch !== 0 && ch !== 8 && (ch < 46 || ch > 57)) {
                        $scope.removeEnter($event);
                    }
                };

                $scope.numberEnter = function ($event) {
                    var ch = $event.charCode;
                    if (ch !== 0 && ch !== 8 && (ch < 48 || ch > 57)) {
                        $timeout(function () {
                            var target = angular.element($event.target);
                            if (target) {
                                var val = target.val();
                                var upd = "";
                                for (var i = 0; i < val.length; i++) {
                                    var c = val.charCodeAt(i);
                                    if (c >= 48 && c <= 57) {
                                        upd += val.charAt(i);
                                    }
                                }
                                target.val(upd);
                            }
                        }, 100);
                    }
                };

                $scope.checkPaxType = function (pax) {
                    if (!pax.birthday || isNaN(pax.birthday.getYear())) {
                        pax.type = null;
                        return;
                    }

                    var diff = new Date(new Date().getTime() - pax.birthday.getTime());
                    diff = Math.abs(diff.getUTCFullYear() - 1970);
                    if (diff < 2) {
                        pax.type = 'INFANT';
                    }
                    else if (diff < 12) {
                        pax.type = 'CHILD';
                    }
                    else if (triptop.booking.scope.booking.checkPaxTypeStrong === true) {
                        if (diff < 24) {
                            pax.type = 'YOUTH';
                        }
                        else if (diff <= 65) {
                            pax.type = 'ADULT';
                        }
                        else {
                            pax.type = 'SENIOR';
                        }
                    }
                    else {
                        pax.type = 'ADULT';
                    }
                };

                $scope.changePaxSex = function (pax, value) {
                    pax.sex = value;
                };

                $scope.selectPax = function (selected, pax) {
                    pax.sex = selected.sex;
                    pax.surname = selected.surname;
                    pax.name = selected.name;
                    pax.birthday = selected.birthday;
                    pax.country = selected.country;

                    var passports = selected.passports;
                    if (passports && passports.length > 0) {
                        pax.document = selected.passports[0].number;
                        pax.documentdate = selected.passports[0].date;

                        if (passports.length > 1) {
                            $scope.booking.help.passports = passports;
                        } else {
                            $scope.booking.help.passports = null;
                        }
                    } else {
                        pax.document = selected.document;
                        pax.documentdate = selected.documentdate;
                    }
                    pax.email = selected.email;
                    pax.phone = selected.phone;
                    pax.bonusCards = selected.bonusCards;

                    $scope.checkPaxType(pax);
                    $scope.findCard(pax);
                };

                $scope.selectPassport = function (selected, pax) {
                    pax.document = selected.number;
                    pax.documentdate = selected.date;
                };

                $scope.findCard = function (pax, code) {
                    if (!code) {
                        code = $scope.booking.help.firstAirline;
                        pax.airline = pax.tmpairline = code;
                    }

                    if (!code) {
                        return;
                    }

                    pax.valueairline = $scope.booking.help.airlines[code];
                    var bonusCards = pax.bonusCards;
                    if (bonusCards && !pax.tmpairlinecard) {
                        for (var i = 0; i < bonusCards.length; i++) {
                            var card = bonusCards[i];
                            if (card.airlineCode === code) {
                                pax.tmpairlinecard = card.code;
                                pax.airlinecard = card.code;
                                return;
                            }
                        }
                    }
                };

                $scope.errorType = null;

                $scope.showError = function (type) {
                    $scope.errorType = type;
                    angular.element('#tt-booking-error-dialog').modal('show');
                };

                $scope.profileStage = false;
                $scope.profileData = null;

                $scope.profileStageCallback = function (stage, data) {
                    $scope.profileStage = stage;
                    if (stage === true) {
                        $scope.profileData = data;
                    }
                    else {
                        $scope.profileData = null;
                    }
                    $scope.reloadProfile();
                };

                $scope.reloadProfile = function () {
                    var values = [], names = {}, key, i;
                    var profile = $scope.profileData;

                    var addPassenger = function (passenger) {
                        var item = angular.copy(passenger);

                        if (!item.surname) {
                            item.surname = passenger.lastname;
                            item.name = passenger.firstname;
                        }

                        key = item.surname + ' ' + item.name;
                        if (!names[key]) {
                            values.push(item);
                            names[key] = 1;
                        }
                    };

                    if (profile) {
                        addPassenger(profile);

                        var passengers = profile.passengers;
                        if (passengers) {
                            for (i = 0; i < passengers.length; i++) {
                                addPassenger(passengers[i]);
                            }
                        }
                    }

                    var paxes = $scope.booking.paxes;
                    if ($scope.bookingStage === 1) {
                        for (i = 0; i < values.length && i < paxes.length; i++) {
                            var value = values[i], pax = paxes[i];
                            if (!pax.surname || pax.surname.length === 0) {
                                $scope.selectPax(value, pax);
                            }
                        }
                    }
                    else if ($scope.bookingStage > 1) {
                        for (i = 0; i < paxes.length; i++) {
                            addPassenger(paxes[i]);
                        }

                        if (values.length > 0) {
                            $scope.selectPax(values[0], $scope.booking.person);
                        }
                    }
                    $scope.booking.help.passengers = values;
                };

                $scope.profileLogin = function () {
                    if (triptop.profile) {
                        triptop.profile.open($scope.profileStageCallback);
                    }
                };

                $scope.processPayment = function (closeCallback) {
                    payment.closeCallback = closeCallback;
                    var paymentDiv = angular.element('<div id ="paymentDivId"><payment></payment></div>').remove();
                    angular.element('body').append(paymentDiv);
                    $compile(paymentDiv.contents())($scope);
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('css', 'booking', 'booking' + options.css_suffix);
                resourceLoader.get('css', 'libs', 'elusive-iconfont/css/elusive-webfont');

                resourceLoader.get('templates', 'booking', 'wizard', 'booking.common', function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);

                    var dialog = angular.element('body > #tt-booking-error-dialog');
                    if (!dialog || dialog.length === 0) {
                        var element = angular.element($element), body = angular.element('body');
                        var container = angular.element('<div class="triptop-container"></div>');
                        container.append(element.find('#tt-booking-error-dialog').remove());
                        body.append(container);
                    }
                });

                if (options.booking && options.booking.ready) {
                    options.booking.ready();
                }
            }
        };
    }]);

    widgets.config(function (resourceLoaderProvider) {
        resourceLoaderProvider.addDependency('booking.common', function (loader, success) {
            loader.css('libs', 'bootstrap/css/less/bootstrap' + options.css_suffix);
            loader.css('libs', 'datepicker/css/bootstrap-datepicker');

            loader.js('libs', 'jquery/jquery.min', function () {
                var count = 8, runner = function () {
                    count--;
                    if (count === 0 && success) {
                        success();
                    }
                };
                loader.module('booking', 'common', runner, null, 'bookingpersonselect');
                loader.module('booking', 'flights', runner, null, 'bookingflightdigest');
                loader.module('booking', 'charters', runner, null, 'bookingcharterdigest');
                loader.module('booking', 'packages', runner, null, 'bookingpackagedigest');
                loader.module('booking', 'hotels', runner, null, 'bookinghoteldigest');
                loader.module('booking', 'transfers', runner, null, 'bookingtransferdigest');
                loader.js('libs', 'datepicker/bootstrap-datepicker', runner);
                loader.js('libs', 'bootstrap/js/bootstrap.min', runner);
            });
        });
    });

    widgets.directive('bookingprocess', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'booking', 'common/processbar', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('bookingcontent', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'booking', 'common/content', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('bookingconfirm', ['$compile', '$http', 'resourceLoader', function ($compile, $http, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                $scope.docUrlStart = options.paths.services.main + "/documents/";
                $scope.legacyDocUrlStart = options.paths.services.main + "/documents/legacyDocument/";

                var booking = triptop.booking.scope.booking;

                $scope.hasUnconfirmedServices = false;

                for(var p = 0; p < booking.products.length; p++) {
                    var product = booking.products[p];
                    if (product.availability == 'RQ') {
                        $scope.hasUnconfirmedServices = true;
                        break;
                    }
                }

                $scope.getPaxPicType = function (pax) {
                    if (pax.type == 'CHILD' || pax.type == 'YOUTH') {
                        return 2;
                    }
                    if (pax.type == 'INFANT') {
                        return 3;
                    }
                    if (pax.sex == 'female') {
                        return 1;
                    }
                    return 0;
                };

                $scope.totalPrices = null;
                if(booking.hasTransfers === true) {
                    cart.ConfirmationBodyRequest($http, $scope.booking.cartId, options.locale).send(function (data) {
                        $scope.confirm = data;
                        $scope.documents = data.documents;
                        $scope.siteName = data.affiliate;
                        booking.prices = {
                            total: data.total,
                            fare: data.fare,
                            tax: data.tax,
                            agent: data.fees,
                            creditcard: data.onlineMarkup ? data.onlineMarkup : 0,
                            currency: { code: data.currency, symbol: api.getCurrencySymbol(data.currency) }
                        };
                    });
                }
                else {
                    var docketId = booking.docketId;
                    if (docketId) {
                        api.cart.GetDocketDocumentsRequest($http, docketId).send(function (resp) {
                            $scope.documents = resp;
                        });
                    }

                    api.SiteInfoRequest($http).send(function (resp) {
                        $scope.siteName = resp.name;
                    });
                }
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'booking', 'common/confirm', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('bookingformation', ['$compile', '$timeout', 'resourceLoader', function ($compile, $timeout, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                $scope.changeBookingType = function (type, cardOwner) {
/*
                    var key = angular.element('#initialScript').attr('key');
                    if(key !== '19956D1AD0') {
                        if(type === 'creditcard') {
                            var title = angular.element('#triptop-booking .tt-warning').text();
                            api.message(title, angular.element('#triptop-booking .tt-creditcard').text());
                            $timeout(function () {
                                $scope.booking.type = 'reserve';
                                widgets.safeApply($scope);
                            }, 100);
                        }
                    }
*/
                    $scope.booking.type = type;
                    $scope.booking.cardOwner = cardOwner ? cardOwner : '0';
                };

                $scope.changePersonSex = function (value) {
                    $scope.booking.person.sex = value;
                };

                $scope.selectPax = function (pax) {
                    triptop.booking.scope.selectPax(pax, $scope.booking.person);
                };

                $scope.gotoStage3 = function () {
                    if (!$scope.booking.agree) {
                        return;
                    }

                    $scope.bookingForm.submited = true;

                    if ($scope.bookingForm.$valid !== true) {
                        $element.find('[ng-form] .ng-pristine.ng-invalid')
                            .removeClass('ng-pristine').addClass('ng-dirty');
                        api.alert(api.getErrorLocalizedMessage(5));
                        return;
                    }

                    if(!($scope.booking.hasTransfers === true && $scope.booking.siteType !== 4)) {
                        $scope.bookingForm.$valid = true;

                        var email = $scope.bookingForm.email.$modelValue;
                        if (!email || email.length === 0) {
                            $scope.bookingForm.$valid = false;
                            $scope.bookingForm.email.$error = { required: true };
                            $element.find('[ng-form] input[name=email]').removeClass('ng-pristine ng-valid').addClass('ng-dirty ng-invalid');
                            return;
                        }
                        else {
                            $scope.bookingForm.$valid = true;
                            $scope.bookingForm.email.$error = undefined;
                            $element.find('[ng-form] input[name=email]').removeClass('ng-dirty ng-invalid').addClass('ng-pristine ng-valid');
                        }

                        var phone = $scope.bookingForm.phone.$modelValue;
                        if (!phone || phone.length === 0) {
                            $scope.bookingForm.$valid = false;
                            $scope.bookingForm.phone.$error = { required: true };
                            $element.find('[ng-form] input[name=phone]').removeClass('ng-pristine ng-valid').addClass('ng-dirty ng-invalid');
                            return;
                        }
                        else {
                            $scope.bookingForm.$valid = true;
                            $scope.bookingForm.phone.$error = undefined;
                            $element.find('[ng-form] input[name=phone]').removeClass('ng-dirty ng-invalid').addClass('ng-pristine ng-valid');
                        }
                    }

                    $scope.gotoStage(3);
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'booking', 'common/formation', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);

                    var dialog = angular.element('body > #tt-booking-view-terms');
                    if (!dialog || dialog.length === 0) {
                        var element = angular.element($element), body = angular.element('body');
                        var container = angular.element('<div class="triptop-container"></div>');
                        container.append(element.find('#tt-booking-view-terms').remove());
                        body.append(container);
                    }
                });
            }
        };
    }]);

    widgets.directive('bookingpaxesheader', ['$compile', '$http', 'resourceLoader', function ($compile, $http, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'booking', 'common/paxes/header', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);

                    var dialog = angular.element('body > #tt-booking-dialog-magic');
                    if (!dialog || dialog.length === 0) {
                        var element = angular.element($element), body = angular.element('body');
                        var container = angular.element('<div class="triptop-container"></div>');
                        container.append(element.find('#tt-booking-dialog-magic').remove());
                        body.append(container);
                    }
                });
            }
        };
    }]);

    widgets.directive('bookingpaxesfooter', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'booking', 'common/paxes/footer', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);
}(window.triptop));