'use strict';

(function (triptop, $) {
    if (!triptop || !triptop.options || !triptop.widgets || !triptop.api) {
        throw new Error('Application not configured');
    }

    var options = triptop.options;
    var widgets = triptop.widgets;
    var payment = triptop.api.paymentWizard;

    triptop.booking.flights = {
        init: function (success) {
            var scope = triptop.booking.scope, booking = scope.booking, products = booking.flights.products;
            if (!products || products.length === 0) {
                return;
            }

            booking.help = { paxes: {}, prices: { fare: 0, tax: 0, options: 0, discount: 0, total: 0 }, segments: {}, airlines: {} };

            var types = ['adult', 'senior', 'youth', 'child', 'infant'];
            for (var i = 0; i < products.length; i++) {
                var temp = [];
                var product = products[i], paxes = product.paxes;
                for (var key in types) {
                    var paxcount = paxes[types[key]] ? paxes[types[key]] : 0;
                    for (var j = 0; j < paxcount; j++) {
                        temp.push({sex: 'male'});
                    }
                }
                if (temp.length > booking.paxes.length) {
                    booking.paxes = temp;
                    booking.help.paxes = paxes;
                }

                var prices = product.prices;
                if (!booking.help.prices.currency) {
                    booking.help.prices.currency = prices.currency;
                }

                booking.help.prices.fare += prices.fare;
                booking.help.prices.tax += prices.tax;
                if (prices.options && prices.options.system) {
                    booking.help.prices.options += prices.options.system;
                }
                if (prices.markups && prices.markups.system) {
                    booking.help.prices.discount += prices.markups.system;
                }

                var segments = product.segments;
                if (segments && segments.length > 0) {
                    for (var s = 0; s < segments.length; s++) {
                        var segment = angular.copy(segments[s]);
                        var direction = segment.direction;
                        var value = booking.help.segments[direction];
                        if (!value) {
                            segment.stops = 0;
                            booking.help.segments[direction] = segment;
                        }
                        else {
                            value.to = segment.to;
                            value.stops = value.stops + 1;
                        }

                        booking.help.airlines[segment.airline.code] = segment.airline.name;
                        if (!booking.help.firstAirline) {
                            booking.help.firstAirline = segment.airline.code;
                        }
                    }
                }
            }

            booking.docketTypeStr = 'flight';
            if(booking.flights.products[0].segments[0].airline.code === 'PS') {
                booking.passportRequired = true;
            }

            if (success) {
                success();
            }
        },
        cancel: function () {
            if (triptop.booking.cancelCallback) {
                triptop.booking.cancelCallback(false);
            }
        },
        terminate: function () {
            if (triptop.booking.cancelCallback) {
                triptop.booking.cancelCallback(true);
            }
        },
        process: function (success, interrupt, error) {
            var scope = triptop.booking.scope, booking = scope.booking, inprocess = scope.inprocess;
            if (booking.type === 'creditcard') {
                scope.processPayment(function () {
                    triptop.booking.checkCallback(inprocess, function (state) {
                            if (state && typeof state == 'string') {
                                state = parseInt(state, 10);
                            }
                            if (state > 3) {
                                booking.payment = true;
                                if (success) {
                                    success();
                                }
                            }
                            else {
                                if (interrupt) {
                                    interrupt();
                                }
                            }
                        },
                        function (e) {
                            scope.showError('PAYMENT_ERROR');
                            if (error) {
                                error();
                            }
                        });
                });

                triptop.booking.bookingCallback(inprocess,
                    function (vsId, sessionId) {
                        triptop.booking._sendPooling(vsId, sessionId, error);
                    },
                    function (e) {
                        scope.showError('BOOKING_ONLINE_ERROR');
                        if (error) { error(); }
                    });
            }
            else {
                triptop.booking.bookingCallback(inprocess, function (docketId, pnr) {
                    if (docketId) {
                        inprocess.docketId = parseInt(docketId, 10);
                        scope.booking.docketId = inprocess.docketId;
                        scope.booking.pnrStr = pnr;
                        if (success) {
                            success();
                        }
                    }
                    else {
                        scope.showError('BOOKING_RESERVE_ERROR');
                        if (error) {
                            error();
                        }
                    }
                }, function (e) {
                    scope.showError('BOOKING_RESERVE_ERROR');
                    if (error) {
                        error();
                    }
                });
            }
        },
        _final: function () {
            var scope = triptop.booking.scope;
            if (triptop.booking.finalCallback) {
                triptop.booking.finalCallback(scope.inprocess);
            }
        },
        rules: function (success, error) {
            var scope = triptop.booking.scope, booking = scope.booking;
            if (triptop.booking.rulesCallback) {
                triptop.booking.rulesCallback(booking, success, error);
            }
        }
    };

    widgets.directive('bookingflightdigest', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                $scope.showDetails = false;

                $scope.openDetails = function () {
                    $scope.showDetails = true;
                };

                $scope.hideDetails = function () {
                    $scope.showDetails = false;
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'booking', 'flights/digest/digest', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.controller('PaxRowController', ['$scope', function ($scope) {
        $scope.isMouseEnter = false;

        $scope.changeBirthDay = function (pax) {
            triptop.booking.scope.checkPaxType(pax);
        };

        $scope.contentMouseEnter = function (value) {
            $scope.isMouseEnter = value;
        };

        $scope.changeMeal = function () {
            var value = $scope.pax.tmpmeals;
            var options = triptop.meals;
            for (var i = 0; i < options.length; i++) {
                var option = options[i];
                if (option.id == value) {
                    $scope.pax.valuemeals = option.name;
                }
            }
        };

        $scope.changeSpecial = function () {
            var value = $scope.pax.tmpspecials;
            var options = triptop.meals;
            for (var i = 0; i < options.length; i++) {
                var option = options[i];
                if (option.id == value) {
                    $scope.pax.valuespecials = option.name;
                }
            }
        };
    }]);

    widgets.config(function (resourceLoaderProvider) {
        resourceLoaderProvider.addDependency('booking.flights.options', function (loader, success) {
            var counter = 3, func = function () {
                counter--;
                if (counter === 0 && success) {
                    success();
                }
            };
            loader.data('data', 'countries/countries', func);
            loader.data('data', 'flights/specials', func);
            loader.data('data', 'flights/meals', func);
        });
    });

    widgets.directive('bookingflightpaxes', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                $scope.gotoStage2 = function () {
                    $scope.paxesList.submited = true;

                    if ($scope.paxesList.$valid !== true) {
                        $($element).find('[ng-form] .ng-pristine.ng-invalid')
                            .removeClass('ng-pristine').addClass('ng-dirty');
                        return;
                    }

                    var scope = triptop.booking.scope;
                    var tmp = angular.copy(scope.booking.help.paxes);
                    var paxes = scope.booking.paxes;

                    var types = 'ADULTS_ONLY';
                    if(tmp.senior === 0 && tmp.youth === 0) {
                        if(tmp.adult === 0) {
                            types = 'CHILD_ONLY';
                        }
                        else if(tmp.infant > 0 || tmp.child > 0) {
                            types = 'ADULTS_WITH_CHILD';
                        }
                    }
                    else {
                        types = 'MIXED';
                    }

                    var checked, counter;
                    for (var i = 0; i < paxes.length; i++) {
                        var pax = paxes[i];
                        scope.checkPaxType(pax);
                        var type = pax.type.toLowerCase();

                        checked = false;
                        if(types === 'MIXED') {
                            checked = true;
                        }
                        else if(types === 'ADULTS_WITH_CHILD') {
                            if(type === 'infant' || type === 'child') {
                                checked = true;
                            }
                            else {
                                pax.type = 'ADULT';
                            }
                        }
                        else if(types === 'ADULTS_ONLY') {
                            pax.type = 'ADULT';
                        }

                        if(checked && tmp[type] > 0) {
                            tmp[type] = tmp[type] - 1;
                        }
                    }

                    checked = false;
                    if(types === 'MIXED') {
                        counter = tmp.adult > 0 ? tmp.adult : 0 +
                            tmp.senior > 0 ? tmp.senior : 0 +
                            tmp.youth > 0 ? tmp.youth : 0 +
                            tmp.child > 0 ? tmp.child : 0 +
                            tmp.infant > 0 ? tmp.infant : 0;

                        checked = counter === 0;
                    }
                    else if(types === 'ADULTS_WITH_CHILD') {
                        counter = tmp.child > 0 ? tmp.child : 0 +
                            tmp.infant > 0 ? tmp.infant : 0;

                        checked = counter === 0;
                    }
                    else if(types === 'ADULTS_ONLY') {
                        checked = true;
                    }

                    if (!checked) {
                        $($element).find('[ng-form] .tt-booking-paxes-type')
                            .addClass('tt-booking-paxes-type-dirty');
                        return;
                    }
                    else {
                        $($element).find('[ng-form] .tt-booking-paxes-type')
                            .removeClass('tt-booking-paxes-type-dirty');
                    }

                    $scope.gotoStage(2);
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'booking', 'flights/paxes/paxes', 'booking.flights.options', function (template) {
                    $scope.countries = triptop.countries;
                    $scope.specials = triptop.specials;
                    $scope.meals = triptop.meals;

                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('bookingflightoptions', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                $scope.showOptions = false;

                $scope.openOptions = function (pax) {
                    pax.tmpmeals = pax.meals;
                    pax.tmpspecials = pax.specials;
                    $scope.showOptions = true;
                };

                $scope.hideOptions = function (pax) {
                    if (pax.tmpmeals) {
                        pax.meals = pax.tmpmeals;
                        delete pax.tmpmeals;
                    }
                    if (pax.tmpspecials) {
                        pax.specials = pax.tmpspecials;
                        delete pax.tmpspecials;
                    }
                    $scope.showOptions = false;
                };

                $scope.closeOptions = function (pax) {
                    delete pax.tmpmeals;
                    delete pax.tmpspecials;
                    $scope.showOptions = false;
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'booking', 'flights/options/options', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('bookingflightbonus', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                $scope.showBonus = false;

                var pax = $scope.pax;
                pax.airline = pax.airline ? pax.airline : $scope.booking.help.firstAirline;
                pax.valueairline = $scope.booking.help.airlines[pax.airline];

                $scope.openBonus = function () {
                    pax.tmpairline = pax.airline;
                    pax.tmpairlinecard = pax.airlinecard;
                    $scope.showBonus = true;
                };

                $scope.hideBonus = function () {
                    if (pax.tmpairline) {
                        pax.airline = pax.tmpairline;
                        delete pax.tmpairline;
                    }
                    if (pax.tmpairlinecard) {
                        pax.airlinecard = pax.tmpairlinecard;
                        delete pax.tmpairlinecard;
                    }
                    $scope.showBonus = false;
                };

                $scope.closeBonus = function () {
                    delete pax.tmpairline;
                    delete pax.tmpairlinecard;
                    $scope.showBonus = false;
                };

                $scope.changeAirline = function () {
                    var code = pax.tmpairline;
                    if (code && code.length > 0) {
                        pax.valueairline = $scope.booking.help.airlines[code];
                        triptop.booking.scope.findCard(pax, code);
                    }
                    else {
                        pax.airline = $scope.booking.help.firstAirline;
                        pax.valueairline = pax.tmpairline = $scope.booking.help.airlines[pax.airline];
                        pax.airlinecard = pax.tmpairlinecard = '';
                    }
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'booking', 'flights/options/bonus', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('bookingflightconfirm', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'booking', 'flights/confirm/confirm', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('bookingflightconfirmheader', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'booking', 'flights/confirm/header', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);
}(window.triptop, window.jQuery));