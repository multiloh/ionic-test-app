'use strict';

(function (triptop, $) {
    if (!triptop || !triptop.options || !triptop.widgets || !triptop.api) {
        throw new Error('Application not configured');
    }

    var options = triptop.options;
    var widgets = triptop.widgets;
    var payment = triptop.api.paymentWizard;

    triptop.booking.hotels = {
        init: function (success) {
            var scope = triptop.booking.scope, booking = scope.booking, products = booking.hotels.products;
            if (!products || products.length === 0) {
                return;
            }

            booking.help = { paxes: {}, prices: { fare: 0, tax: 0, options: 0, discount: 0, total: 0 } };

            var types = ['adult', 'child'];
            for (var i = 0; i < products.length; i++) {
                var temp = [];
                var product = products[i], paxes = product.room.paxes;
                if (paxes) {
                    for (var key in types) {
                        var paxcount = paxes[types[key]] ? paxes[types[key]] : 0;
                        for (var j = 0; j < paxcount; j++) {
                            temp.push({sex: 'male'});
                        }
                    }
                }

                booking.paxes = temp;
                booking.help.paxes = paxes;

                var prices = product.prices;
                if (!booking.help.prices.currency) {
                    booking.help.prices.currency = prices.currency;
                }

                booking.help.prices.fare += prices.fare;
                booking.help.prices.tax += prices.tax;
            }

            if (success) {
                success();
            }
        },
        cancel: function () {
            if (triptop.booking.cancelCallback) {
                triptop.booking.cancelCallback(false);
            }
        },
        terminate: function () {
            if (triptop.booking.cancelCallback) {
                triptop.booking.cancelCallback(true);
            }
        },
        process: function (success, interrupt, error) {
            var scope = triptop.booking.scope, booking = scope.booking, inprocess = scope.inprocess;
            if (booking.type === 'creditcard') {
                scope.processPayment(function () {
                    triptop.booking.checkCallback(inprocess, function (state) {
                            if (state && typeof state == 'string') {
                                state = parseInt(state, 10);
                            }
                            if (state > 3) {
                                booking.payment = true;
                                if (success) {
                                    success();
                                }
                            }
                            else {
                                if (interrupt) {
                                    interrupt();
                                }
                            }
                        },
                        function (e) {
                            scope.showError('PAYMENT_ERROR');
                            if (error) {
                                error();
                            }
                        });
                });

                triptop.booking.bookingCallback(inprocess,
                    function (vsId, sessionId) {
                        triptop.booking._sendPooling(vsId, sessionId, error);
                    },
                    function (e) {
                        scope.showError('BOOKING_ONLINE_ERROR');
                        if (error) {
                            error();
                        }
                    }
                );
            } else {
                triptop.booking.bookingCallback(inprocess, function (docketId, pnr) {
                        if (docketId) {
                            inprocess.docketId = parseInt(docketId, 10);
                            scope.booking.docketId = inprocess.docketId;
                            scope.booking.pnrStr = pnr;
                            if (success) {
                                success();
                            }
                        } else {
                            scope.showError('BOOKING_RESERVE_ERROR');
                            if (error) {
                                error();
                            }
                        }
                    },
                    function (e) {
                        scope.showError('BOOKING_RESERVE_ERROR');
                        if (error) {
                            error();
                        }
                    });
            }
        },
        _final: function () {
            var scope = triptop.booking.scope;
            if (triptop.booking.finalCallback) {
                triptop.booking.finalCallback(scope.inprocess);
            }
        },
        rules: function (success, error) {
        }
    };

    widgets.directive('bookinghoteldigest', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                $scope.showDetails = false;

                $scope.openDetails = function () {
                    $scope.showDetails = true;
                };

                $scope.hideDetails = function () {
                    $scope.showDetails = false;
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'booking', 'hotels/digest/digest', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.config(function (resourceLoaderProvider) {
        resourceLoaderProvider.addDependency('booking.hotel.options', function (loader, success) {
            loader.data('data', 'countries/countries', success);
        });
    });

    widgets.directive('bookinghotelpaxes', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                $scope.checkPaxType = function (pax) {
                    if (!pax.birthday || isNaN(pax.birthday.getYear())) {
                        pax.type = null;
                        return;
                    }

                    var diff = new Date(new Date().getTime() - pax.birthday.getTime());
                    diff = Math.abs(diff.getUTCFullYear() - 1970);
                    if (diff < 12) {
                        pax.type = 'CHILD';
                    }
                    else {
                        pax.type = 'ADULT';
                    }
                };

                $scope.checkType = function (pax) {
                    var diff = new Date(new Date().getTime() - pax.birthday.getTime());
                    diff = Math.abs(diff.getUTCFullYear() - 1970);
                    var ages = [], products = triptop.booking.scope.booking.products;
                    for (var i = 0, length = products.length; i < length; i++) {
                        var product = products[i];
                        if (product.room && product.room.ages) {
                            ages = ages.concat(product.room.ages);
                        }
                    }
                    if (ages.length > 0) {
                        var age = Math.max.apply(Math, ages);
                        return diff <= age;
                    }
                    return true;
                };

                $scope.changeBirthDay = function (pax) {
                    $scope.checkPaxType(pax);
                };

                $scope.gotoStage2 = function () {
                    $scope.paxesList.submited = true;

                    if ($scope.paxesList.$valid !== true) {
                        $($element).find('[ng-form] .ng-pristine.ng-invalid')
                            .removeClass('ng-pristine').addClass('ng-dirty');
                        return;
                    }

                    var scope = triptop.booking.scope;
                    var tmp = angular.copy(scope.booking.help.paxes);

                    var paxes = scope.booking.paxes;
                    for (var i = 0; i < paxes.length; i++) {
                        var pax = paxes[i];
                        $scope.checkPaxType(pax);
                        var type = pax.type.toLowerCase();
                        if (type === 'child' && !$scope.checkType(pax)) {
                            continue;
                        }
                        tmp[type] = tmp[type] - 1;
                    }

                    var count = tmp.adult > 0 ? tmp.adult : 0 +
                        tmp.child > 0 ? tmp.child : 0;

                    console.error('Fail checkTypes:' + count);
                    count = 0;
                    if (count > 0) {
                        $($element).find('[ng-form] .tt-booking-paxes-type')
                            .addClass('tt-booking-paxes-type-dirty');
                        return;
                    }
                    else {
                        $($element).find('[ng-form] .tt-booking-paxes-type')
                            .removeClass('tt-booking-paxes-type-dirty');
                    }

                    $scope.gotoStage(2);
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'booking', 'hotels/paxes/paxes', 'booking.hotel.options', function (template) {
                    $scope.countries = triptop.countries;

                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('bookinghotelconfirm', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'booking', 'hotels/confirm/confirm', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('bookinghotelconfirmheader', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'booking', 'hotels/confirm/header', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);
}(window.triptop, window.jQuery));