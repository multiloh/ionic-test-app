'use strict';

(function (triptop) {
    if (!triptop || !triptop.options || !triptop.widgets || !triptop.api) {
        throw new Error('Application not configured');
    }

    var options = triptop.options;
    var widgets = triptop.widgets;
    var api = triptop.api;
    var transfers = api.transfers;
    var payment = api.paymentWizard;
    var cart = api.cart;

    triptop.booking.transfers = {
        init: function (success) {
            var scope = triptop.booking.scope, booking = scope.booking, products = booking.transfers.products;
            if (!products || products.length === 0) {
               return;
            }

            for (var i = 0, length = products.length; i < length; i++) {
                var p = products[i];
                if (p.originalProduct.availability == 'RQ') {
                    booking.paymentUnavailable = true;
                    booking.type = 'reserve';
                }

                booking.prices.fare += p.prices.fare * 100;
                booking.prices.tax += p.prices.tax * 100;
                booking.prices.options += p.prices.options * 100;
                booking.prices.discount += p.prices.discount * 100;
                booking.prices.creditcard += p.prices.creditcard * 100;
                booking.prices.total += p.prices.total * 100;
                booking.prices.currency = p.prices.currency;
            }

            booking.help.prices = booking.prices;

            if(success) { success(); }
        },
        cancel: function () {
            if(triptop.booking.cancelCallback) {
                triptop.booking.cancelCallback();
            }
        },
        process: function (success, interrupt, error) {
            angular.injector(["ng"]).invoke(function($http) {
                var scope = triptop.booking.scope;
                var booking = scope.booking;
                var products = booking.transfers.products;

                var user;
                if (triptop.profile && triptop.profile.user) {
                    user = triptop.profile.user.id;
                }

                var cartId;

                var orderProduct = function (success, interrupt) {
                    cart.TakeNewCartRequest($http, user).send(function (data) {
                        if(data.id) {
                            cartId = booking.cartId = data.id;
                            booking.displayCaptcha = data.shouldEnterCaptcha;
                        }
                        else {
                            cartId = booking.cartId = data;
                        }

                        var counter = products.length;
                        var callback = function () {
                            counter--;
                            if(counter === 0) {
                                finishBookingCallback(success, interrupt);
                            }
                        };

                        for (var i = 0, length = counter; i < length; i++) {
                            addProduct(products[i], callback);
                        }
                    }, errorCallback);
                };

                var addProduct = function (product, successCallback) {
                    cart.AddProductRequest($http, cartId, "Transfer", product.id, product.amount, booking.prices.agent).send(function () {
                        cart.CheckAvailabilityRequest($http, cartId).send(function () {
                            var makeJson = function (location, airline, flight, terminal, hotel, address, remarks) {
                                return {
                                    location: location,
                                    airline: airline ? airline.name : '',
                                    flight: flight,
                                    terminal: terminal,
                                    name: hotel ? hotel : '',
                                    address: address,
                                    remarks: remarks
                                };
                            };

                            var polling = cart.AvailabilityPollRequest($http, cartId);

                            var pollingCallback = function (data) {
                                if (data.status == "SUCCESS") {
                                    var pickupLocation = product.locations[0];
                                    var dropOffLocation = product.locations[1];

                                    var pickup = makeJson(product.pickUp, pickupLocation.airline, pickupLocation.flightNumber, pickupLocation.terminal, pickupLocation.hotelName, pickupLocation.hotelAddress, pickupLocation.comments);
                                    var dropoff = makeJson(product.dropOff, dropOffLocation.airline, dropOffLocation.flightNumber, dropOffLocation.terminal, dropOffLocation.hotelName, dropOffLocation.hotelAddress, dropOffLocation.comments);

                                    cart.EditTransferOrderDetails($http, cartId, product.id, product.paxNumber, product.luggageNumber, pickup, dropoff).send(successCallback, errorCallback);
                                }
                                else {
                                    setTimeout(function () {
                                        polling.send(pollingCallback, errorCallback);
                                    }, 1000);
                                }
                            };
                            polling.send(pollingCallback, errorCallback);
                        }, errorCallback);
                    }, errorCallback);
                };

                var payment = function () {
                    scope.payDocketId = booking.docketId;

                    if (booking.type === 'creditcard') {
                        scope.processPayment(function () {
                            var polling = cart.ConfirmationStatusRequest($http, cartId);
                            polling.counter = 3;
                            var pollingCallback = function (data) {
                                if (data.status == "SUCCESS") {
                                    success();
                                }
                                else if (data.status == "FAILED") {
                                    if (interrupt) { interrupt(); }
                                }
                                else {
                                    polling.counter--;
                                    if(polling.counter > 0) {
                                        setTimeout(function () {
                                            polling.send(pollingCallback, errorCallback);
                                        }, 1000);
                                    }
                                    else {
                                        if (interrupt) { interrupt(); }
                                    }
                                }
                            };
                            polling.send(pollingCallback, errorCallback);
                        },
                        errorCallback);
                    }
                    else {
                        success();
                    }
                };

                var finishBookingCallback = function (success, interrupt) {
                    cart.EditOrderDetailsRequest($http, cartId, booking.person).send(function () {
                        var willPayNow = booking.type === 'creditcard';

                        cart.UnpaidReservationRequest($http, cartId, willPayNow, booking.captchaId, booking.captchaAnswer).send(function (data) {
                            booking.docketId = data;

                            var polling = cart.UnpaidStatusRequest($http, cartId);

                            var pollingCallback = function (data) {
                                var status = data.status;

                                if (status == 'SUCCESS') {
                                    success();
                                    return;
                                }

                                setTimeout(function () {
                                    polling.send(pollingCallback, errorCallback);
                                }, 1000);
                            };
                            polling.send(pollingCallback, errorCallback);
                        },
                        function (code) {                     
                            if (code == '209') {
                                if (interrupt) { interrupt(209); }
                                return;
                            }
                            if (errorCallback) { errorCallback(); }
                        });
                    }, errorCallback);
                };

                orderProduct(payment, interrupt);
            });
        },
        final: function () {
        }
    };

    var errorCallback = function (err) {
        api.alert(api.getErrorLocalizedMessage(404));
    };

    widgets.directive('bookingtransferdigest', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                $scope.showDetails = false;

                $scope.openDetails = function () {
                    $scope.showDetails = true;
                };

                $scope.hideDetails = function () {
                    $scope.showDetails = false;
                };

                $scope.imgUrlBase = options.paths.templates.transfers + "/../../img/";
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'booking', 'transfers/digest', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('bookingtransferparams', ['$compile', '$http', 'resourceLoader', function ($compile, $http, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                $scope.gotoStage2 = function () {
                    var scope = triptop.booking.scope, booking = scope.booking, products = booking.transfers.products;

                    if (products.length == 2) {
                        products[1].paxNumber = products[0].paxNumber;
                        products[1].luggageNumber = products[0].luggageNumber;
                    }

                    for (var i = 0, length = products.length; i < length; i++) {
                        var product = products[i];

                        for (var l = 0, size = product.locations.length; l < size; l++) {
                            var location = product.locations[l];
                            if (location.subType == 'CENTER') {
                                $('#hotelName-' + product.direction).removeClass('ng-dirty ng-invalid').addClass('ng-pristine ng-valid');
                                $('#hotelAddress-' + product.direction).removeClass('ng-dirty ng-invalid').addClass('ng-pristine ng-valid');

                                if (!location.hotelName && !location.hotelAddress) {
                                    $('#hotelName-' + product.direction).removeClass('ng-pristine ng-valid').addClass('ng-dirty ng-invalid');
                                    $('#hotelAddress-' + product.direction).removeClass('ng-pristine ng-valid').addClass('ng-dirty ng-invalid');
                                    api.alert(api.getErrorLocalizedMessage(9));
                                    return;
                                }
                            }
                            if (location.subType == 'AIRPORT') {
                                $('#airline-' + product.direction).removeClass('ng-dirty ng-invalid').addClass('ng-pristine ng-valid');
                                if (!location.airline) {
                                    $('#airline-' + product.direction).removeClass('ng-pristine ng-valid').addClass('ng-dirty ng-invalid');
                                    api.alert(api.getErrorLocalizedMessage(11));
                                    return;
                                }
                                $('#flightNumber-' + product.direction).removeClass('ng-dirty ng-invalid').addClass('ng-pristine ng-valid');
                                if(!location.flightNumber) {
                                    $('#flightNumber-' + product.direction).removeClass('ng-pristine ng-valid').addClass('ng-dirty ng-invalid');
                                    api.alert(api.getErrorLocalizedMessage(11));
                                    return;
                                }
                            }
                        }
                    }

                    $scope.gotoStage(2);
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'booking', 'transfers/params', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);

                    var loadAirlines = function (product) {
                        transfers.AirlinesRequest($http, product.id).send(function (data) {
                            product.airlines = data;
                            widgets.safeApply($scope);
                        });
                    };

                    var scope = triptop.booking.scope, booking = scope.booking, products = booking.transfers.products;
                    for (var i = 0, length = products.length; i < length; i++){
                        var product = products[i];
                        if(!product.airlines) {
                            loadAirlines(product);
                        }
                    }
                });
            }
        };
    }]);

    widgets.directive('bookingtransferconfirmheader', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'booking', 'transfers/header', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('bookingtransferconfirm', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'booking', 'transfers/confirm', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('confirmtransferinfo', ['$compile', '$http', 'resourceLoader', function ($compile, $http, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'booking', 'transfers/info', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);
}(window.triptop));