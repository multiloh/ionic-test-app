'use strict';

(function (triptop, $) {
    if (!triptop || !triptop.options || !triptop.widgets || !triptop.api) {
        throw new Error('Application not configured');
    }

    var options = triptop.options;
    var widgets = triptop.widgets;

    triptop.booking.packages = {
        init: function (success) {
            var scope = triptop.booking.scope, booking = scope.booking, products = booking.packages.products;
            if (!products || products.length === 0) {
                return;
            }

            booking.help = { complects: [], paxes: {}, prices: { fare: 0, tax: 0, options: 0, discount: 0, total: 0 }, segments: {}, airlines: {} };

            var types = ['adult', 'child', 'infant'];
            for (var i = 0; i < products.length; i++) {
                var temp = [];
                var product = products[i], complects = product.complects, paxes = {}, count;
                if(complects && complects.length > 0) {
                    booking.help.complects = booking.help.complects.concat(complects);

                    for (var r = 0; r < complects.length; r++) {
                        var complect = complects[r];
                        if(complect === 'INFANT') {
                            count = paxes['infant'];
                            count = count ? count++ : 1;
                            paxes['infant'] = count;
                        }
                        else {
                            count = paxes['adult'];
                            count = (count ? count : 0) + complect.split('A').length - 1;
                            paxes['adult'] = count;
                            count = paxes['child'];
                            count = (count ? count : 0) + complect.split('C').length - 1;
                            paxes['child'] = count;
                        }
                    }
                }
                product.paxes = paxes;

                for (var key in types) {
                    var paxcount = paxes[types[key]] ? paxes[types[key]] : 0;
                    for (var j = 0; j < paxcount; j++) {
                        temp.push({sex: 'male'});
                    }
                }

                booking.paxes = temp;
                booking.help.paxes = paxes;

                var prices = product.prices;
                if (!booking.help.prices.currency) {
                    booking.help.prices.currency = prices.currency;
                }

                booking.help.prices.fare += prices.fare;
                booking.help.prices.tax += prices.tax;
                if (prices.options && prices.options.system) {
                    booking.help.prices.options += prices.options.system;
                }
                if (prices.markups && prices.markups.system) {
                    booking.help.prices.discount += prices.markups.system;
                }

                var segments = product.segments;
                if (segments && segments.length > 0) {
                    for (var s = 0; s < segments.length; s++) {
                        var segment = angular.copy(segments[s]);
                        var direction = segment.direction;
                        var value = booking.help.segments[direction];
                        if (!value) {
                            segment.stops = 0;
                            booking.help.segments[direction] = segment;
                        }
                        else {
                            value.to = segment.to;
                            value.stops = value.stops + 1;
                        }

                        booking.help.airlines[segment.airline.code] = segment.airline.name;
                        if(!booking.help.firstAirline) {
                            booking.help.firstAirline = segment.airline.code;
                        }
                    }
                }
            }
            if(success) { success(); }
        },
        cancel: function () {
            if (triptop.booking.cancelCallback) {
                triptop.booking.cancelCallback(false);
            }
        },
        terminate: function () {
            if (triptop.booking.cancelCallback) {
                triptop.booking.cancelCallback(true);
            }
        },
        process: function (success, interrupt, error) {
            var scope = triptop.booking.scope, booking = scope.booking, inprocess = scope.inprocess;
            triptop.booking.bookingCallback(inprocess, function (docketId, pnr) {
                if (docketId) {
                    inprocess.docketId = parseInt(docketId, 10);
                    scope.booking.docketId = inprocess.docketId;
                    scope.booking.pnrStr = pnr ? pnr : 'PKG' + inprocess.docketId;
                    if(success) { success(); }
                }
                else {
                    scope.showError('BOOKING_RESERVE_ERROR');
                    if(error) { error(); }
                }
            },
            function (e) {
                scope.showError('BOOKING_RESERVE_ERROR');
                if(error) { error(); }
            });
        },
        _final: function () {
            var scope = triptop.booking.scope;
            if (triptop.booking.finalCallback) {
                triptop.booking.finalCallback(scope.inprocess);
            }
        },
        rules: function (success, error) {}
    };

    widgets.directive('bookingpackagedigest', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                $scope.showDetails = false;

                $scope.openDetails = function () {
                    $scope.showDetails = true;
                };

                $scope.hideDetails = function () {
                    $scope.showDetails = false;
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'booking', 'packages/digest/digest', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.config(function (resourceLoaderProvider) {
        resourceLoaderProvider.addDependency('booking.package.options', function (loader, success) {
            loader.data('data', 'countries/countries', success);
        });
    });

    widgets.directive('bookingpackagepaxes', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                $scope.checkPaxType = function (pax) {
                    if (!pax.birthday || isNaN(pax.birthday.getYear())) {
                        pax.type = null;
                        return;
                    }

                    var diff = new Date(new Date().getTime() - pax.birthday.getTime());
                    diff = Math.abs(diff.getUTCFullYear() - 1970);
                    if (diff < 2) {
                        pax.type = 'INFANT';
                    }
                    else if (diff < 12) {
                        pax.type = 'CHILD';
                    }
                    else {
                        pax.type = 'ADULT';
                    }
                };

                $scope.changeBirthDay = function(pax) {
                    $scope.checkPaxType(pax);
                };

                $scope.gotoStage2 = function () {
                    $scope.paxesList.submited = true;

                    if ($scope.paxesList.$valid !== true) {
                        $($element).find('[ng-form] .ng-pristine.ng-invalid')
                            .removeClass('ng-pristine').addClass('ng-dirty');
                        return;
                    }

                    var scope = triptop.booking.scope;
                    var tmp = angular.copy(scope.booking.help.paxes);
                    var tmpcomplects = angular.copy(scope.booking.help.complects);
                    var paxes = scope.booking.paxes;
                    for (var i = 0; i < paxes.length; i++) {
                        var pax = paxes[i];
                        $scope.checkPaxType(pax);
                        var type = pax.type.toLowerCase();
                        tmp[type] = tmp[type] - 1;

                        var t = type.charAt(0).toUpperCase();
                        for (var j = 0; j < tmpcomplects.length; j++) {
                            var complect = tmpcomplects[j];
                            if (t === 'I') {
                                if (complect === 'INFANT') {
                                    pax.complect = complect;
                                    tmpcomplects.splice(j, 1);
                                    break;
                                }
                            }
                            else {
                                var p = complect.indexOf(t);
                                if (p != -1) {
                                    pax.complect = scope.booking.help.complects[j];
                                    tmpcomplects[j] = complect.replace(t, '');
                                    break;
                                }
                            }
                        }
                    }

                    var count = tmp.adult > 0 ? tmp.adult : 0 +
                        tmp.child > 0 ? tmp.child : 0 +
                        tmp.infant > 0 ? tmp.infant : 0;

                    if (count > 0) {
                        $($element).find('[ng-form] .tt-booking-paxes-type')
                            .addClass('tt-booking-paxes-type-dirty');
                        return;
                    }
                    else {
                        $($element).find('[ng-form] .tt-booking-paxes-type')
                            .removeClass('tt-booking-paxes-type-dirty');
                    }

                    $scope.gotoStage(2);
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'booking', 'packages/paxes/paxes', 'booking.package.options', function (template) {
                    $scope.countries = triptop.countries;

                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('bookingpackageconfirm', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'booking', 'packages/confirm/confirm', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('bookingpackageconfirmheader', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'booking', 'packages/confirm/header', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);
}(window.triptop, window.jQuery));