'use strict';

(function (triptop) {
    if (!triptop || !triptop.widgets) {
        throw new Error('Application not configured');
    }

    var widgets = triptop.widgets;

    widgets.directive('bookingpersonselect', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div class="tt-dropdown"></div>',
            link: function ($scope, $element, $attrs) {
                var modify = function (passengers, value) {
                    resourceLoader.get('templates', 'booking', 'common/paxes/passengers', null, function (template) {
                        $element.html(template);
                        $compile($element.contents())($scope);
                    });
                };
                $scope.$watch('booking.help.passengers', function (passengers, value) {
                    modify(passengers, value);
                });
                modify($scope.booking.help.passengers);
            }
        };
    }]);

    widgets.directive('bookingpassportselect', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div class="tt-dropdown"></div>',
            link: function ($scope, $element, $attrs) {
                var modify = function (passports, value) {
                    resourceLoader.get('templates', 'booking', 'common/paxes/passports', null, function (template) {
                        $element.html(template);
                        $compile($element.contents())($scope);
                    });
                };
                $scope.$watch('booking.help.passports', function (passports, value) {
                    modify(passports, value);
                });
                modify($scope.booking.help.passports);
            }
        };
    }]);

    widgets.directive('confirmflightsegment', ['$compile', '$http', 'resourceLoader', function ($compile, $http, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'booking', 'common/segment', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('hotelroomboarding', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'booking', 'hotels/boarding', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('hotelroomtype', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'booking', 'hotels/roomtype', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.filter('seats', function () {
        return function (product) {
            var seats = 10;
            if (!product || !product.segments) {
                return seats;
            }
            var segments = product.segments;
            for (var i = 0; i < segments.length; i++) {
                var availableSeats = segments[i].availableSeats;
                if (!availableSeats) {
                    continue;
                }
                if (seats > availableSeats) {
                    seats = availableSeats;
                }
            }
            return seats;
        };
    });

    var pad = function (num, size) {
        var s = "000000000" + num;
        return s.substr(s.length - size);
    };

    widgets.filter('flightTime', function () {
        return function (time) {
            if (!time) {
                return '';
            }
            return pad(Math.floor(time / 60), 2) + ':' + pad(Math.floor(time % 60), 2);
        };
    });

    widgets.filter('productprice', function () {
        return function (prices, type) {
            if (!prices) {
                return '';
            }
            var value = '';
            if (type === 'faretax') {
                value = (prices.fare + prices.tax) / 100;
                return value.toFixed(2) + ' ' + prices.currency.symbol;
            }
            else if (type === 'options') {
                if (!prices.options || !prices.options.system) {
                    return '';
                }
                value = (prices.options.system) / 100;
                return (value > 0 ? ' + ' : ' - ') + Math.abs(value).toFixed(2) + ' ' + prices.currency.symbol;
            }
            else if (type === 'discount') {
                if (!prices.markups || !prices.markups.system) {
                    return '';
                }
                value = (prices.markups.system) / 100;
                return (value > 0 ? ' + ' : ' - ') + Math.abs(value).toFixed(2) + ' ' + prices.currency.symbol;
            }
            else if (type === 'total') {
                value = ((prices.markups && prices.markups.system) ? prices.markups.system : 0) +
                    ((prices.options && prices.options.system) ? prices.options.system : 0);
                if (value === 0) {
// ???                     return '';
                }
                value = (value + prices.fare + prices.tax) / 100;
                return ' = ' + value.toFixed(2) + ' ' + prices.currency.symbol;
            }
            return value;
        };
    });

    widgets.filter('bookingprice', function () {
        return function (products, type) {
            var scope = triptop.booking.scope;
            if (!products || products.length === 0) {
                return '';
            }
            var value = '', price = 0;
            if (type === 'faretax') {
                price = scope.booking.help.prices.fare + scope.booking.help.prices.tax;
                if (price) {
                    price = price / 100;
                }
            }
            else if (type === 'creditcard') {
                price = scope.booking.prices.creditcard;
                if (price) {
                    value = price >= 0 ? ' + ' : ' - ';
                    price = price / 100;
                }
            }
            else if (type === 'total') {
                var prices = scope.booking.help.prices;
                var fee = scope.booking.prices.agent;
                price = prices.fare + prices.tax + prices.options + prices.discount +
                    (fee && fee.length > 0 ? parseInt(fee, 10) * 100 : 0);

                if (scope.booking.type === 'creditcard' && scope.booking.prices.creditcard) {
                    price += scope.booking.prices.creditcard;
                }
                price = price / 100;
                value = ' = ';
            }
            else {
                price = scope.booking.help.prices[type];
                if (price) {
                    value = price >= 0 ? ' + ' : ' - ';
                    price = price / 100;
                }
            }
            value += Math.abs(price).toFixed(2) + ' ' + scope.booking.help.prices.currency.symbol;
            return value;
        };
    });

    widgets.filter('paxcount', function () {
        return function (paxes, type) {
            if (!paxes.paxcount) {
                paxes.paxcount = (paxes.adult ? paxes.adult : 0) +
                    (paxes.senior ? paxes.senior : 0) +
                    (paxes.youth ? paxes.youth : 0) +
                    (paxes.child ? paxes.child : 0) +
                    (paxes.infant ? paxes.infant : 0);
            }
            if (!paxes.tempcount) {
                paxes.tempcount = paxes.paxcount;
            }
            var symbol = (paxes.tempcount !== paxes.paxcount) ? ' + ' : '';
            paxes.tempcount = paxes.tempcount - (paxes[type] ? paxes[type] : 0);
            if (paxes.tempcount === 0) {
                delete paxes.tempcount;
            }
            return symbol + paxes[type];
        };
    });

    widgets.filter('inprocessprice', function () {
        return function (inprocess, type) {
            if (!inprocess || !inprocess.prices || !inprocess.prices.fare) {
                return '';
            }
            var value = '';
            var price = inprocess.prices[type];
            if (price) {
                value = (type !== 'fare' && type !== 'total') ? (price >= 0 ? '   ' : ' - ') : '';
                price = price / 100;
            }

            var symbol = '';
            if(inprocess.prices.currency && inprocess.prices.currency.symbol) {
                symbol = inprocess.prices.currency.symbol;
            }
            else if(inprocess.prices.currencySymbol) {
                symbol = inprocess.prices.currencySymbol;
            }

            value += Math.abs(price).toFixed(2) + ' ' + symbol;
            return value;
        };
    });    
}(window.triptop));