'use strict';

(function (triptop, $) {
    if (!triptop || !triptop.options || !triptop.widgets || !triptop.api) {
        throw new Error('Application not configured');
    }

    var options = triptop.options;
    var widgets = triptop.widgets;
    var api = triptop.api;
    var paymentWizard = api.paymentWizard;

    triptop.payment = {
        scope: null,
        init: function () {
            var booking = this.scope.booking;
            if (!booking || !booking.products || booking.products.length === 0) {
                return;
            }

            booking.help = { segments: {} };

            var products = booking.products;
            for (var i = 0; i < products.length; i++) {
                var product = products[i];

                if (!booking.prices.currency && product.prices) {
                    booking.prices.currency = product.prices.currency;
                }

                var segments = product.segments;
                if (segments && segments.length > 0) {
                    for (var s = 0; s < segments.length; s++) {
                        var segment = angular.copy(segments[s]);
                        var direction = segment.direction;
                        var value = booking.help.segments[direction];
                        if (!value) {
                            segment.stops = 0;
                            segment.list = [segments[s]];
                            booking.help.segments[direction] = segment;
                        }
                        else {
                            value.to = segment.to;
                            value.stops = value.stops + 1;
                            value.list.push(segments[s]);
                        }
                    }
                }
            }
        }
    };

    var getFrameUrl = function () {
        var loc = window.location.toString();
        var pos = loc.indexOf('#');
        loc = pos != -1 ? loc.substr(0, pos) : loc;

        var scope = triptop.payment.scope;
        var params = {
            docketId: scope.booking.docketId,
            bookingCurrencyCode: scope.selectedCurrency,
            locale: options.locale,
            originalUrl: encodeURIComponent(loc),
            cardOwnerId: scope.cardOwner,
            'productId[]': scope.productIds
        };

        return api.buildUrl(options.paths.services.main + "/pay/portalform", params);
    };

    widgets.directive('payment', ['$compile', 'resourceLoader', '$http', function ($compile, resourceLoader, $http) {
        return {
            restrict: 'AE',
            scope: true,
            replace: true,
            template: '<div id="triptop-payment" class="triptop-container"></div>',
            controller: function ($scope, $element) {
                triptop.payment.scope = $scope;

                $scope.waitImg = triptop.options.paths.services.main + "/../images/wait.gif";
                $scope.frameLoaded = false;
                $scope.doStopPoller = false;

                $scope.recalcInprocess = function () {
                    var prices = $scope.totalPrices[$scope.selectedCurrency];
                    var scope = triptop.booking.scope;
                    scope.inprocess.prices = prices;
                    scope.inprocess.currency = {
                        symbol: prices.currencySymbol
                    };
                };

                var URL_POLLER_TIMEOUT = 3000;

                var windowUrlPoller = function () {
                    if ($scope.doStopPoller) {
                        return;
                    }

                    var param = window.location.hash.substring(1);
/*
                    if (api.endsWith(param, "loaded") && $scope.frameLoaded === false) {
                        window.location.hash = "";
                        $scope.frameLoaded = true;
                        $scope.$apply();
                    }
*/
                    if (api.endsWith(param, "success")) {
                        $scope.recalcInprocess();
                    }

                    if (api.endsWith(param, "close")) {
                        window.location.hash = "";
                        $scope.closePaymentWizard();
                    }

                    setTimeout(windowUrlPoller, URL_POLLER_TIMEOUT);
                };

                $scope.iframeLoadedCallBack = function () {
                    $scope.frameLoaded = true;
                    widgets.safeApply($scope, windowUrlPoller);
                };

                $scope.selectedCurrency = "USD";
                $scope.frameUrl = undefined;

                $scope.loadDocket = function (docketId, productIds) {
                    $scope.paymentStage = 3;
                    $scope.booking = {docketId: docketId, prices: {}};

                    var counter = 2, updateStage = function () {
                        counter--;
                        if (counter === 0) {
                            triptop.payment.init();
                        }
                    };

                    paymentWizard.CurrencyRequest($http, docketId, productIds, options.locale).send(function (resp) {
                        var scope = triptop.payment.scope;
                        scope.totalPrices = resp.total;
                        scope.booking.prices = scope.totalPrices[scope.selectedCurrency];
                        updateStage();
                    }, function () {
                        $scope.closePaymentWizard();
                    });

                    paymentWizard.ProductsRequest($http, docketId, productIds, options.locale).send(function (resp) {
                        var scope = triptop.payment.scope;
                        scope.booking.products = resp.products;
                        scope.booking.products[0].paxes = resp.paxes;
                        triptop.booking.scope.booking.pnrStr = resp.products[0].pnr;
                        $scope.selectedCurrency = resp.products[0].currencyCode;
                        $scope.frameUrl = getFrameUrl();
                        updateStage();
                    }, function () {
                        $scope.closePaymentWizard();
                    });
                };

                if (!$scope.cardOwner && triptop.booking && triptop.booking.scope && triptop.booking.scope.booking) {
                    $scope.cardOwner = triptop.booking.scope.booking.cardOwner;
                }
                if (!$scope.cardOwner) {
                    $scope.cardOwner = 0;
                }

                var productIds = $scope.productIds;
                if (!productIds) {
                    productIds = [];
                }

                var docketId = $scope.payDocketId;
                if (docketId) {
                    $scope.loadDocket(docketId, productIds);
                }
                else {
                    if (options.devmode === true && options.paymentInit) {
                        $scope.booking = options.paymentInit();
                        triptop.payment.init();

                        $scope.frameUrl = getFrameUrl();
                        $scope.paymentStage = 3;
                    }
                    else {
                        $scope.paymentStage = 1;
                    }
                }

                $scope.gotoStage = function (stage) {
                    $scope.paymentStage = stage;
                };

                $scope.closePaymentWizard = function () {
                    $scope.doStopPoller = true;
                    if (paymentWizard.closeCallback) {
                        paymentWizard.closeCallback();
                    }
                    paymentWizard.close($);
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('css', 'payment', 'payment' + options.css_suffix);
                resourceLoader.get('css', 'libs', 'elusive-iconfont/css/elusive-webfont');

                resourceLoader.get('templates', 'payment', 'wizard', 'payment.jquery.min', function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.config(function (resourceLoaderProvider) {
        resourceLoaderProvider.addDependency('payment.jquery.min', function (loader, success) {
            loader.css('libs', 'bootstrap/css/less/bootstrap' + options.css_suffix);
            loader.js('libs', 'jquery/jquery.min', function () {
                loader.js('libs', 'bootstrap/js/bootstrap.min', success);
            });
        });
    });

    widgets.directive('paymentflightdigest', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'payment', 'flights/digest', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('transferdigest', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'payment', 'transfers/digest', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('hoteldigest', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                var coords = triptop.payment.scope.booking.products[0].coordinates;
                var urlStart = "http://maps.google.com/maps?q=";
                if (parseFloat(coords.latitude) === 0.0 && parseFloat(coords.longitude) === 0.0) {
                    $scope.googleMapsUrl = "";
                } else {
                    $scope.googleMapsUrl = urlStart + coords.latitude + "," + coords.longitude + "&output=embed";
                }
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'payment', 'hotels/digest', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.filter('paxcount', function () {
        return function (paxes, type) {
            if (!paxes.paxcount) {
                paxes.paxcount = (paxes.adult ? paxes.adult : 0) +
                    (paxes.senior ? paxes.senior : 0) +
                    (paxes.youth ? paxes.youth : 0) +
                    (paxes.child ? paxes.child : 0) +
                    (paxes.infant ? paxes.infant : 0);
            }
            if (!paxes.tempcount) {
                paxes.tempcount = paxes.paxcount;
            }
            var symbol = (paxes.tempcount !== paxes.paxcount) ? ' + ' : '';
            paxes.tempcount = paxes.tempcount - (paxes[type] ? paxes[type] : 0);
            if (paxes.tempcount === 0) {
                delete paxes.tempcount;
            }
            return symbol + paxes[type];
        };
    });

    widgets.directive('paymentpricing', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'payment', 'common/pricing', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('paymentcontent', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'payment', 'common/content', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('paymentfooter', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'payment', 'common/footer', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('paymentinitial', ['$compile', 'resourceLoader', '$http', function ($compile, resourceLoader, $http) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'payment', 'common/initial', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('paymentcurrency', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'payment', 'common/currency', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('paymentprocess', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                $scope.onClick = function (item) {
                    var scope = triptop.payment.scope;
                    scope.selectedCurrency = item;
                    scope.frameUrl = getFrameUrl();
                    $scope.booking.prices = scope.totalPrices[item];
                };
                resourceLoader.get('templates', 'payment', 'common/process', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('paymentconfirm', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'payment', 'common/confirm', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.filter('paymentprice', function () {
        return function (booking, type) {
            if (!booking || !booking.prices || !booking.prices.fare) {
                return '';
            }
            var value = '';
            var price = booking.prices[type];
            if (price) {
                value = (type !== 'fare' && type !== 'total') ? (price >= 0 ? '   ' : ' - ') : '';
                price = price / 100;
            }
            value += Math.abs(price).toFixed(2) + ' ' + booking.prices.currencySymbol;
            return value;
        };
    });
}(window.triptop, window.jQuery));