'use strict';

(function (triptop) {
    if (!triptop || !triptop.widgets) {
        throw new Error('Application not configured');
    }

    var widgets = triptop.widgets;
    var api = triptop.api;
    var cabinet = api.cabinet;

	widgets.directive('editor', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
		return {
			restrict: 'AE',
			replace: true,
			template: '<div></div>',
			link: function ($scope, $element, $attrs) {
				resourceLoader.get('templates', 'profile', 'editor/main', 'profile.editor', function (template) {
					$scope.countries = triptop.countries;
					$element.html(template);
					$compile($element.contents())($scope);
				});
			}
		};
	}]);

	widgets.config(function (resourceLoaderProvider) {
		resourceLoaderProvider.addDependency('profile.editor', function (loader, success) {
            var counter = 2, func = function() {
                counter--;
                if (counter === 0 && success) { success(); }
            };
			loader.css('libs', 'datepicker/css/bootstrap-datepicker');
			loader.js('libs', 'datepicker/bootstrap-datepicker', func);
            loader.data('data', 'countries/countries', func);
		});
	});

	widgets.directive('credentials', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
		return {
			restrict: 'AE',
			replace: true,
			template: '<div></div>',
			link: function ($scope, $element, $attrs) {
				resourceLoader.get('templates', 'profile', 'editor/credentials', 'profile.editor', function (template) {
					$element.html(template);
					$compile($element.contents())($scope);
				});
			}
		};
	}]);

    widgets.directive('passenger', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'profile', 'editor/passenger', 'profile.editor', function (template) {
					$scope.countries = triptop.countries;
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('bonuscards', ['$compile', 'resourceLoader', '$http', function ($compile, resourceLoader, $http) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {

                if (!$scope.airlines) {
                    var airlinesRequest = api.cabinet.AirlinesRequest($http);
                    airlinesRequest.send(function (resp) {
                        $scope.airlines = resp;
                    });
                }

                var successCallback = function (resp) {
                    $scope.selectView($scope.bonusCardsPrevView);
                };

                var failure = function (code) {
                    api.alert(api.getErrorLocalizedMessage(code));
                };

                var submitProfile = function() {
                    cabinet.RequestWithLoginRetry($http, $scope, function ($http, $scope) {
                        return cabinet.EditProfileRequest($http, $scope.user, $scope.profile);
                    }, successCallback, failure);
                };

                $scope.submitBonusCards = function () {
                    switch ($scope.bonusCardsPrevView) {
                        case 'addpassenger':
                            $scope.passenger.bonusCards = $scope.bonusCards;
                            $scope.switchToPassEditor();
                            break;
                        case 'editpassenger':
                            $scope.submitPassenger(successCallback);
                            break;
                        case 'editor':
                            submitProfile();
                            break;
                    }
                };

                $scope.bonusCardsBack = function(){
                    $scope.selectView($scope.bonusCardsPrevView);
                };

                $scope.addBonusCard = function () {
                    $scope.bonusCards.push({airlineCode: "", code: ""});
                };

                $scope.deleteBonusCard = function(){
                    $scope.bonusCards.splice($scope.selectedBonusCard, 1);
                };
                $scope.selectBonusCard = function ($event, $index) {
                    if ($scope.selectedBonusCard === $index) {
                        return;
                    }
                    if ($scope.selectedBonusCard !== -1) {
                        $($element).find('.selectedRow').removeClass('selectedRow');
                    }
                    else {
                    }
                    $($($event.target).parents('tr')[0]).addClass('selectedRow');
                    $scope.selectedBonusCard = $index;

                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'profile', 'editor/bonuscards', 'jquery.datepicker',
                    function (template) {
                        $element.html(template);
                        $compile($element.contents())($scope);
                    });
            }
        };
    }]);
}(window.triptop));