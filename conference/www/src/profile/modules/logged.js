'use strict';

(function (triptop) {
    if (!triptop || !triptop.widgets || !triptop.api) {
        throw new Error('Application not configured');
    }

    var widgets = triptop.widgets;
    var api = triptop.api;

    widgets.directive('logged', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        var modify = false;
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                $scope.isOpened = false;

                $scope.openCabinet = function () {
                    this.isOpened = true;
                };

                $scope.closeCabinet = function () {
                    this.isOpened = false;
                };
            },
            link: function ($scope, $element, $attrs) {
                modify = function (isOpened, value) {
                    if (isOpened === value) {
                        return;
                    }

                    if (isOpened === true) {
                        resourceLoader.get('module', 'profile', 'profile', null, function () {
                            $element.html('<profile></profile>');
                            $compile($element.contents())($scope);
                        });
                    }
                    else {
                        $element.html('<closed></closed>');
                        $compile($element.contents())($scope);
                    }
                };
                $scope.$watch('isOpened', function (isOpened, value) {
                    modify(isOpened, value);
                });
                modify($scope.isOpened);
                
                triptop.profile.scope.$watch('openRequired', function (openRequired, value) {
                    if($scope.isOpened !== true && openRequired === true) {
                        triptop.profile.scope.openRequired = false;
                        $scope.openCabinet();
                    }
                });
            }
        };
    }]);

    widgets.directive('closed', ['$compile', '$http', 'resourceLoader', function ($compile, $http, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'profile', 'logged/closed', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);
}(window.triptop));