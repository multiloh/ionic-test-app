'use strict';

(function (triptop) {
    if (!triptop || !triptop.widgets || !triptop.api) {
        throw new Error('Application not configured');
    }

    var options = triptop.options;
    var widgets = triptop.widgets;
    var api = triptop.api;
    var modifyUnloggedState = false;

    widgets.directive('unlogged', ['$compile', function ($compile) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                $scope.unloggedState = 0;

                $scope.closeLogin = function () {
                    this.unloggedState = 0;
                };

                $scope.openLogin = function () {
                    this.unloggedState = 1;
                };

                $scope.openRegistration = function () {
                    this.unloggedState = 2;
                };

                $scope.openChangePassword = function () {
                    this.unloggedState = 3;
                };
            },
            link: function ($scope, $element, $attrs) {
                modifyUnloggedState = function (unloggedState, value) {
                    $scope.unloggedState = unloggedState;
                    if (unloggedState === value) {
                        return;
                    }

                    if (unloggedState === 1) {
                        $element.html('<login></login>');
                        $compile($element.contents())($scope);
                    }
                    else if (unloggedState === 2) {
                        $element.html('<registration></registration>');
                        $compile($element.contents())($scope);
                    }
                    else if (unloggedState === 3) {
                        $element.html('<forgotpass></forgotpass>');
                        $compile($element.contents())($scope);
                    }
                    else {
                        $element.html('<invite></invite>');
                        $compile($element.contents())($scope);
                    }
                };
                $scope.$watch('unloggedState', function (unloggedState, value) {
                    modifyUnloggedState(unloggedState, value);
                });
                triptop.profile.scope.$watch('loginRequired', function (loginRequired, value) {
                    if($scope.unloggedState !== 1 && loginRequired === true) {
                        triptop.profile.scope.loginRequired = false;
                        $scope.openLogin();
                    }
                });
                
                modifyUnloggedState($scope.unloggedState);
            }
        };
    }]);

    widgets.config(function (resourceLoaderProvider) {
        resourceLoaderProvider.addDependency('cabinet.jquery.min', function (loader, success) {
            loader.css('libs', 'bootstrap/css/less/bootstrap' + options.css_suffix);
            loader.js('libs', 'jquery/jquery.min', function () {
                loader.js('libs', 'jquery/jquery.md5');
                loader.js('libs', 'bootstrap/js/bootstrap.min', success);
            });
        });
        resourceLoaderProvider.addDependency('cabinet.jquery.datepicker', function (loader, success) {
            loader.css('libs', 'datepicker/css/bootstrap-datepicker');
            loader.js('libs', 'bootstrap/js/bootstrap.min');
            loader.js('libs', 'datepicker/bootstrap-datepicker', success);
        });
    });

    widgets.directive('invite', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'profile', 'unlogged/invite', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('forgotpass', ['$compile', 'resourceLoader', '$http', function ($compile, resourceLoader, $http) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                var $ = window.jQuery;
                $scope.changePassword = function () {
                    if ($scope.user.password != $scope.user.confirm) {
                        api.alert( $("#passwordsShouldMatchMsg").text());
                        return;
                    }

                    var user = $scope.user;
                    user.md5 = $.md5(user.password);

                    api.cabinet.ForgotPasswordRequest($http, user).send(
                        function (resp) {
                            api.alert( $("#passwordChangedMsg").text());
                            $scope.unloggedState = 1;
                        }, function (code) {
                            api.alert( api.getErrorLocalizedMessage(code));
                        });
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'profile', 'unlogged/forgotpass', 'cabinet.jquery.min', function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('login', ['$compile', '$http', 'resourceLoader', function ($compile, $http, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                $scope.user.isLogged = false;
                $scope.user.password = undefined;

                $scope.submitLogin = function () {
                    var $ = window.jQuery;

                    if ($scope.loginForm.$valid !== true) {
                        $($element).find('[ng-form] .ng-pristine.ng-invalid')
                            .removeClass('ng-pristine').addClass('ng-dirty');
                        return;
                    }

                    var user = $scope.user;
                    user.md5 = $.md5(user.password);
                    api.cabinet.LoginRequest($http, user).send(function (resp) {
                        user.sid = resp.id;
                        triptop.profile.user = user;
                        triptop.profile.user.isLogged = true;
                        user.isLogged = true;
                        user.id = resp.userId;
                        delete user.password;

                        var stored = api.toJson(user);
                        if (user.remember === true) {
                            localStorage.triptopLoginService = stored;
                        }
                        else {
                            delete localStorage.triptopLoginService;
                        }
                        sessionStorage.triptopLoginService = stored;

                        $scope.getProfile(user);
                        $scope.closeLogin();
                    }, function (code) {
                        api.alert( api.getErrorLocalizedMessage(code));
                    });
                };

                $scope.getProfile = function(user){
                    api.cabinet.GetProfileRequest($http, user).send(function (resp) {
                        triptop.profile.profile = resp;
                        $scope.profile = resp;
                        api.cabinet.GetDocketsRequest($http, user).send(function (resp) {
                            $scope.profile.dockets = resp.dockets;
                        });
                        api.cabinet.GetPassengersRequest($http, user).send(function (resp) {
                            $scope.profile.passengers = resp.passengers;
                        });
                    });
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'profile', 'unlogged/login', 'cabinet.jquery.min', function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('registration', ['$compile', '$http', 'resourceLoader', function ($compile, $http, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                $scope.user.isLogged = false;
                $scope.user.password = undefined;

                if (!triptop.profile.postRegistrationMessage) {
                    triptop.profile.postRegistrationMessage = angular.element('#postRegisterMessage').text();
                }

                var $ = window.jQuery;

                $scope.submitRegistration = function () {
                    var user = $scope.user;
                    if ($scope.registrationForm.$valid === true) {
                        if ($scope.user.birthday != null && $scope.user.birthday > new Date()) {
                            api.alert($("#birthdayMsg").text());
                            return;
                        }

                        var regReq = api.cabinet.RegistrationRequest($http, user);
                        regReq.send(function (resp) {
                            var alertAcceptCallback = function(){
                                triptop.profile.user = user;
                                user.isLogged = true;
                                user.md5 = $.md5(user.password);
                                delete user.password;
                                delete user.confirm;
                                user.sid = resp.id;
                                $scope.getProfile(user);
                                modifyUnloggedState(0);
                            };
                            api.alert(triptop.profile.postRegistrationMessage, alertAcceptCallback);
                        }, function (error) {
                            api.alert(api.getErrorLocalizedMessage(error), function () {});
                        });
                    }
                    else {
                        $($element).find('[ng-form] .ng-pristine.ng-invalid').removeClass('ng-pristine').addClass('ng-dirty');
                    }
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'profile', 'unlogged/registration', 'cabinet.jquery.datepicker', function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('passwordVerify', function () {
        return {
            require: 'ngModel',
            link: function ($scope, $element, $attrs, $ctrl) {
                $ctrl.$parsers.unshift(function (value) {
                    var origin = $scope.$eval($attrs['passwordVerify']);
                    if (origin !== value) {
                        $ctrl.$setValidity('confirm', false);
                        return undefined;
                    }
                    else {
                        $ctrl.$setValidity('confirm', true);
                        return value;
                    }
                });
            }
        };
    });
}(window.triptop));