'use strict';

(function (triptop) {
    if (!triptop || !triptop.widgets) {
        throw new Error('Application not configured');
    }

    var widgets = triptop.widgets;

	widgets.directive('messages', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
		return {
			restrict: 'AE',
			replace: true,
			template: '<div></div>',
			link: function ($scope, $element, $attrs) {
				resourceLoader.get('templates', 'profile', 'messages/main', null, function (template) {
					$element.html(template);
					$compile($element.contents())($scope);
				});
			}
		};
	}]);
}(window.triptop));