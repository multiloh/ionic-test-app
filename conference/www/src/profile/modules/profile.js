'use strict';

(function (triptop, $) {
    if (!triptop || !triptop.widgets || !triptop.api) {
        throw new Error('Application not configured');
    }

    var options = triptop.options;
    var widgets = triptop.widgets;
    var api = triptop.api;

    widgets.directive('profile', ['$compile', '$window', 'resourceLoader', function ($compile, $window, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                $scope.signOut = function () {
                    var res = $window.confirm($("#signOutMsg").text());

                    if (res === true) {
                        delete localStorage.triptopLoginService;
                        delete sessionStorage.triptopLoginService;

                        this.closeCabinet();
                        this.profile = null;
                        triptop.profile.user.isLogged = false;
                        this.user = {isLogged: false};
                    }
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'profile', 'profile/main', 'jquery.bootstrap', function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.config(function (resourceLoaderProvider) {
        resourceLoaderProvider.addDependency('jquery.bootstrap', function (loader, success) {
            loader.css('libs', 'bootstrap/css/less/bootstrap' + options.css_suffix);
            loader.js('libs', 'jquery/jquery.min', function () {
                loader.js('libs', 'jquery/jquery.md5');
                loader.js('libs', 'bootstrap/js/bootstrap.min', success);
            });
        });
    });

    widgets.directive('passportedit', ['$compile', '$http', 'resourceLoader', function ($compile, $http, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                $scope.editPassport = function () {
                    $scope.state = 'edit';
                    var idx = $scope.passports.indexOf($scope.selectedPassport);
                    $scope.currentPassport = $scope.passports[idx];
                };

                $scope.checkCurrentPassport = function (okCallBack) {
                    if (!$scope.currentPassport.number || $scope.currentPassport.date == null) {
                        api.alert( $('#wrongPassport').text());
                        return;
                    }

                    for (var i = 0; i < $scope.passports.length; i++) {
                        if ($scope.passports[i].number === $scope.currentPassport.number && $scope.passports[i] != $scope.currentPassport) {
                            api.alert( $('#passportAlreadyExists').text());
                            return;
                        }
                    }
                    okCallBack();
                };

                $scope.mainButtonClick = function () {
                    if ($scope.state == 'edit') {
                        $scope.checkCurrentPassport(function () {
                            $scope.selectedPassport = $scope.currentPassport;
                            $scope.currentPassport = angular.copy($scope.selectedPassport);
                            $scope.state = 'list';
                        });
                    }
                    else if ($scope.passports.length === 0) { // add
                        $scope.checkCurrentPassport(function () {
                            $scope.passports.push(angular.copy($scope.currentPassport));
                            $scope.currentPassport = {number: "", date: null};
                            $scope.state = 'add';
                        });
                    }
                    else if ($scope.state == 'add') { // submit
                        $scope.checkCurrentPassport(function () {
                            $scope.passports.push(angular.copy($scope.currentPassport));
                            $scope.selectedPassport = $scope.passports[$scope.passports.length - 1];
                            $scope.state = 'list';
                        });
                    }
                    else if ($scope.state == 'list') {
                        $scope.currentPassport = {number: "", date: null};
                        $scope.state = 'add';
                    }
                };

                $scope.onPassportChange = function () {
                    if (!($scope.currentPassport.number && $scope.currentPassport.date)) {
                        return;
                    }

                    if ($scope.state == 'list' && $scope.passports.length === 0) {
                        $scope.state = 'edit';
                        $scope.passports.push($scope.currentPassport);
                    }
                };

                $scope.cancelPassport = function () {
                    if ($scope.state == 'add') {
                        $scope.currentPassport = angular.copy($scope.selectedPassport);
                    }
                    else if ($scope.state == 'edit') {
                        var idx = $scope.passports.indexOf($scope.currentPassport);
                        $scope.passports.splice(idx, 1);
                        if ($scope.passports.length === 0) {
                            $scope.currentPassport = {number: "", date: null};
                        } 
                        else {
                            if (idx > 0) {
                                idx--;
                            }
                            $scope.selectedPassport = $scope.passports[idx];
                            $scope.currentPassport = angular.copy($scope.selectedPassport);
                        }
                    }
                    $scope.state = "list";
                };

                $scope.deletePassport = function () {
                    if ($scope.passports.length === 1) {
                        $scope.passports = [];
                        $scope.currentPassport = {number: "", date: null};
                        return;
                    }

                    var delIdx = $scope.passports.indexOf($scope.selectedPassport);

                    var idx = delIdx;
                    if (idx > 0) {
                        idx--;
                    }
                    $scope.currentPassport = angular.copy($scope.selectedPassport);
                    $scope.passports.splice(delIdx, 1);
                    $scope.selectedPassport = $scope.passports[idx];

                    $scope.state = "list";
                };

                $scope.selectPassport = function (pass) {
                    $scope.selectedPassport = pass;
                    $scope.currentPassport = angular.copy(pass);
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'profile', 'editor/passportedit', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('tabview', ['$compile', '$window', '$http', 'resourceLoader', function ($compile, $window, $http, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                if (!$scope.passports) {
                    $scope.passports = [];
                }

                $scope.docUrlStart = triptop.options.paths.services.main + "/documents/";
                $scope.legacyDocUrlStart = triptop.options.paths.services.main + "/documents/legacyDocument/";

                $scope.setCurrentPassport = function () {
                    if ($scope.passports.length > 0) {
                        $scope.selectedPassport = $scope.passports[0];
                        $scope.currentPassport = angular.copy($scope.passports[0]);
                    } else {
                        $scope.currentPassport = {
                            number: '',
                            date: null
                        };
                    }
                    $scope.state = 'list';
                };

                $scope.selectedView = 'profile';
                var cabinet = triptop.api.cabinet;

                $scope.notConfirmedMessage = function () {
                    api.alert($("#notConfirmedMsg").text());
                };

                $scope.submitEditPass = function () {
                    var oldMd5 = $.md5($scope.user.curpassword);
                    if ($scope.user.md5 != oldMd5) {
                        api.alert($("#wrongPassMsg").text());
                        return;
                    }

                    if ($scope.user.newpassword != $scope.user.confirm) {
                        api.alert($("#wrongPassConfirmationMsg").text());
                        return;
                    }

                    var newMd5 = $.md5($scope.user.newpassword);

                    var setPasswordCallback = function () {
                        if ($scope.profile.emailConfirmed === false) {
                            $scope.notConfirmedMessage();
                            return;
                        }
                        api.alert($("#newPasswordSetMsg").text());
                        $scope.selectView("profile");
                        delete $scope.user.password;
                        delete $scope.user.newpassword;
                        delete $scope.user.confirm;
                    };

                    var failureCallback = function (err) {
                        api.alert("Can't set password.");
                    };

                    cabinet.RequestWithLoginRetry($http, $scope, function ($http, $scope) {
                        return cabinet.SetPasswordRequest($http, $scope.user, newMd5);
                    }, setPasswordCallback, failureCallback);

                };

                $scope.submitEditEmail = function () {

                    var getProfileSuccessCallback = function (resp) {
                        $scope.profile = resp;

                        if ($scope.profile.emailConfirmed === false) {
                            $scope.notConfirmedMessage();
                            return;
                        }

                        var md5 = $.md5($scope.user.password);
                        if ($scope.user.md5 != md5) {
                            api.alert($("#wrongPassMsg").text());
                            return;
                        }

                        var successCallback = function () {
                            api.alert($("#checkEmailMsg").text().replace("%s", $scope.profile.email));
                        };
                        var errorCallback = function (code) {
                            api.alert(api.getErrorLocalizedMessage(code));
                        };

                        cabinet.RequestWithLoginRetry($http, $scope, function ($http, $scope) {
                            return cabinet.ChangeEmailRequest($http, $scope.user, $scope.user.newemail);
                        }, successCallback, errorCallback);
                    };

                    cabinet.RequestWithLoginRetry($http, $scope, function ($http, $scope) {
                        return api.cabinet.GetProfileRequest($http, $scope.user);
                    }, getProfileSuccessCallback);
                };

                $scope.editProfileBonusCards = function(){
                    $scope.bonusCards = $scope.profile.bonusCards;
                    $scope.bonusCardsPrevView = $scope.selectedView;
                    $scope.selectView("bonuscards");
                };

                $scope.submitEditInfo = function () {
                    if ($scope.profile.birthday != null && $scope.profile.birthday > new Date()) {
                        api.alert($("#birthdayMsg").text());
                        return;
                    }

                    var editSuccess = function (resp) {
                        $scope.selectView("profile");
                    };

                    var failure = function (code) {
                        api.alert(api.getErrorLocalizedMessage(code));
                        $scope.closeCabinet();
                    };

                    cabinet.RequestWithLoginRetry($http, $scope, function ($http, $scope) {
                        return cabinet.EditProfileRequest($http, $scope.user, $scope.profile);
                    }, editSuccess, failure);
                };

                $scope.payDocket = function (index) {
                    var dockets = $scope.profile.dockets;
                    var docket = dockets[$scope.selectedDocket];

                    if (!(docket.statusId === 1 || docket.statusId === 3)) {
                        api.alert( $('#docketCantBePaid').text());
                        return;
                    }

                    $scope.payDocketId = docket.id;
                    api.paymentWizard.open($('#triptop-profile'), $scope, $compile, $, function () {});
                };

                $scope.cancelDocket = function (index) {
                    var dockets = $scope.profile.dockets;
                    var docket = dockets[$scope.selectedDocket];

                    var msgCallback = function(){
                        api.alert($('#docketCancelRequested').text());
                    };

                    cabinet.RequestWithLoginRetry($http, $scope, function ($http, $scope) {
                        return cabinet.CancelDocketRequest($http, $scope.user, docket.id);
                    }, msgCallback, errorCallback);
                };

                $scope.deletePassenger = function () {
                    var res = $window.confirm($("#deletePassengerMsg").text());
                    if (res === false) {
                        return;
                    }
                    var id = $scope.profile.passengers[$scope.selectedPassenger].id;
                    $scope.profile.passengers.splice($scope.selectedPassenger, 1);

                    var errorCallback = function (code) {
                        api.alert(api.getErrorLocalizedMessage(code));
                    };

                    var successCallback = function () {
                    };

                    cabinet.RequestWithLoginRetry($http, $scope, function ($http, $scope) {
                        return cabinet.DeletePassengerRequest($http, $scope.user, id);
                    }, successCallback, errorCallback);
                };

                $scope.setDefault = function () {
                    var idx = $scope.passports.indexOf($scope.selectedPassport);
                    $scope.passports.splice(idx, 1);
                    $scope.passports.unshift($scope.selectedPassport);
                    $scope.selectedPassport = $scope.passports[0];
                    $scope.currentPassport = angular.copy($scope.passports[0]);
                };

                var errorCallback = function (code) {
                    api.alert( api.getErrorLocalizedMessage(code));
                };

                $scope.submitPassenger = function (successCallback) {
                    if (!successCallback) {
                        successCallback = function(){
                            $scope.selectView("profile");
                        };
                    }
                    $scope.passenger.passports = $scope.passports;
                    if ($scope.editingPassenger === true) { // edit
                        cabinet.RequestWithLoginRetry($http, $scope, function ($http, $scope) {
                            return cabinet.EditPassengerRequest($http, $scope.user, $scope.passenger);
                        }, successCallback, errorCallback);
                    } else { // add
                        $scope.profile.passengers.push($scope.passenger);
                        cabinet.RequestWithLoginRetry($http, $scope, function ($http, $scope) {
                            return cabinet.AddPassengerRequest($http, $scope.user, $scope.passenger);
                        }, successCallback, errorCallback);
                    }
                };

                $scope.selectView = function ($name, $event) {
                    if (this.selectedView === $name) {
                        return;
                    }
                    if ($event) {
                        var tab = $($event.target).parent();
                        tab.parent().find('.active').removeClass('active');
                        tab.addClass('active');
                    }
                    this.selectedView = $name;
                };

                $scope.editPassengerBonusCards = function(){
                    $scope.bonusCards = $scope.passenger.bonusCards;
                    $scope.bonusCardsPrevView = $scope.selectedView;
                    $scope.selectView("bonuscards");
                };
            },
            link: function ($scope, $element, $attrs) {
                $scope.switchToPassEditor = function(){
                    $element.html('<passenger></passenger>');
                    $compile($element.contents())($scope);
                };
                var modify = function (selectedView, value) {
                    if (selectedView === value) {
                        return;
                    }

                    if (selectedView === 'profile') {
                        resourceLoader.get('templates', 'profile', 'profile/profile', null, function (template) {
                            $element.html(template);
                            $compile($element.contents())($scope);
                        });
                    }
                    else if (selectedView === 'editor') {
                        resourceLoader.get('module', 'profile', 'editor', null, function () {
                            $element.html('<editor></editor>');
                            $scope.passports = $scope.profile.passports;
                            $scope.state = 'list';
                            $scope.setCurrentPassport();
                            $compile($element.contents())($scope);
                        });
                    }
                    else if (selectedView === 'credentials') {
                        resourceLoader.get('module', 'profile', 'editor', null, function () {
                            $element.html('<credentials></credentials>');
                            $compile($element.contents())($scope);
                        });
                    }
                    else if (selectedView === 'addpassenger') {
                        resourceLoader.get('module', 'profile', 'editor', null, function () {
                            $scope.editingPassenger = false;
                            $scope.passenger = {
                                sex: "male",
                                passports: [],
                                bonusCards: []
                            };
                            $scope.state = 'list';
                            $scope.passports = $scope.passenger.passports;
                            $scope.setCurrentPassport();
                            $scope.switchToPassEditor();
                        });
                    }
                    else if (selectedView === 'editpassenger') {
                        resourceLoader.get('module', 'profile', 'editor', null, function () {
                            $scope.editingPassenger = true;
                            $scope.passenger = $scope.profile.passengers[$scope.selectedPassenger];
                            $scope.passports = $scope.passenger.passports;
                            $scope.setCurrentPassport();
                            $element.html('<passenger></passenger>');
                            $compile($element.contents())($scope);
                        });
                    } else if (selectedView === 'bonuscards') {
                        resourceLoader.get('module', 'profile', 'editor', null, function () {
                            $element.html('<bonuscards></bonuscards>');
                            $compile($element.contents())($scope);
                        });
                    } else if (selectedView === 'viewdocket') {
                        resourceLoader.get('module', 'profile', 'dockets', null, function () {
                            $element.html('<view></view>');
                            $compile($element.contents())($scope);
                        });
                    }
                    else if (selectedView === 'paydocket') {
                        resourceLoader.get('module', 'profile', 'dockets', null, function () {
                            $element.html('<pay></pay>');
                            $compile($element.contents())($scope);
                        });
                    }
                    else if (selectedView === 'changedocket') {
                        resourceLoader.get('module', 'profile', 'dockets', null, function () {
                            $element.html('<change></change>');
                            $compile($element.contents())($scope);
                        });
                    }
                    else if (selectedView === 'canceldocket') {
                        resourceLoader.get('module', 'profile', 'dockets', null, function () {
                            $element.html('<cancel></cancel>');
                            $compile($element.contents())($scope);
                        });
                    }
                    else if (selectedView === 'messages') {
                        resourceLoader.get('module', 'profile', 'messages', null, function () {
                            $element.html('<messages></messages>');
                            $compile($element.contents())($scope);
                        });
                    }
                    else if (selectedView === 'offers') {
                        resourceLoader.get('module', 'profile', 'offers', null, function () {
                            $element.html('<offers></offers>');
                            $compile($element.contents())($scope);
                        });
                    }
                };
                $scope.$watch('selectedView', function (selectedView, value) {
                    modify(selectedView, value);
                });
                modify($scope.selectedView);
            }
        };
    }]);

    widgets.directive('passengers', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                $scope.selectedPassenger = -1;

                $scope.selectPassenger = function ($event, $index) {
                    if ($scope.selectedPassenger === $index) {
                        return;
                    }
                    if ($scope.selectedPassenger !== -1) {
                        $($element).find('.selectedRow').removeClass('selectedRow');
                    }
                    else {
                        $($element).find('button[disabled]').removeClass('disabled').removeAttr('disabled');
                    }
                    $($event.target).parents('tr').addClass('selectedRow');
                    $scope.selectedPassenger = $index;
                };

            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'profile', 'profile/passengers', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('orders', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                $scope.selectedDocket = -1;
                $scope.selectDocket = function ($event, $index) {
                    if ($scope.selectedDocket === $index) {
                        return;
                    }
                    if ($scope.selectedDocket !== -1) {
                        $($element).find('.selectedRow').removeClass('selectedRow');
                    }
                    else {
                        $($element).find('button[disabled]').removeClass('disabled').removeAttr('disabled');
                    }
                    $($event.target).parents('tbody').addClass('selectedRow');
                    $scope.selectedDocket = $index;
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'profile', 'profile/orders', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);
}(window.triptop, window.jQuery));