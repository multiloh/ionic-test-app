'use strict';
/*global alert */

(function (triptop) {
    if (!triptop || !triptop.options || !triptop.widgets || !triptop.api) {
        throw new Error('Application not configured');
    }

    var options = triptop.options;
    var widgets = triptop.widgets;
    var api = triptop.api;

    triptop.profile = {
        scope: null,
        callbacks: { checking:[] },
        fireCallbacks: function (name, args, multi) {
            var items = this.callbacks[name];
            if(!items) { return; }

            for (var i = 0; i < items.length; i++) {
                var callback = items[i];
                if(callback) {
                    callback.apply(null, args);
                }
                if (!multi) {
                    delete items[i];
                }
            }
        },
        check: function (callback) {
            if(callback) {
                if(this.scope) {
                    var logged = this.scope.user.isLogged;
                    callback(logged, logged ? this.scope.profile : null);
                }
                else {
                    this.callbacks.checking.push(callback);
                }
            }
        },
        open: function (callback) {
            if (callback) {
                this.callbacks.checking.push(callback);
            }
            if (this.scope) {
                if (this.scope.user.isLogged) {
                    this.scope.openRequired = true;
                } else {
                    this.scope.loginRequired = true;
                }
                widgets.safeApply(this.scope);
            }
        },
        fire: function (action, callback, data) {
            if (this.scope && action === 'bookingSuccess') {
                if (this.user && this.user.isLogged) {
                    this.scope.reloadData();
                    this.scope.mergePassengers(data.paxes, data.person);
                }
            }
        },
        registerLink: function (target, captions) {
            if(this.scope) {
                var elem = angular.element(target);
                elem.on('click', function(event) {
                    triptop.profile.open(function(isLogged) {
                        if(isLogged) {
                            triptop.profile.open();
                        }
                    });
                    event.preventDefault();
                });
                var modify = function (isLogged) {
                    elem.text(captions[(isLogged === true ? 1 : 0)]);
                };
                this.scope.$watch('user.isLogged', function (isLogged, value) {
                    modify(isLogged);
                });
                modify(this.scope.user.isLogged);
            }
        }
    };

    widgets.directive('cabinet', ['$compile', '$http', 'resourceLoader',
        function ($compile, $http, resourceLoader) {
        return {
            restrict: 'AE',
            scope: true,
            replace: true,
            template: '<div id="triptop-profile" class="triptop-container"></div>',
            controller: function ($scope, $element) {
                triptop.profile.scope = $scope;

                var stored = sessionStorage.triptopLoginService;
                if (!stored) {
                    stored = localStorage.triptopLoginService;
                }
                if(stored) {
                    $scope.user = angular.fromJson(stored);
                    triptop.profile.user = $scope.user;
                }

                if (!$scope.user || !$scope.user.isLogged) {
                    $scope.user = {isLogged: false};
                }
                $scope.profile = null;

                $scope.reloadData = function () {
                    api.cabinet.GetDocketsRequest($http, $scope.user).send(function (resp) {
                        $scope.profile.dockets = resp.dockets;
                    });
                };

                // add booking passengers to the personal cabinet
                $scope.mergePassengers = function (paxes, payer) {
                    var profile = $scope.profile;
                    if (!profile) {
                        return;
                    }

                    var paxMergeCallback = function (resp) {
                        if (resp.status != 6) {
                            return;
                        }

                        api.cabinet.GetProfileRequest($http, $scope.user).send(function (profile) {
                            $scope.profile = profile;
                            triptop.profile.profile = profile;
                            api.cabinet.GetPassengersRequest($http, $scope.user).send(function (resp) {
                                var passengers = resp.passengers;
                                if (passengers) {
                                    profile.passengers = passengers;
                                    triptop.profile.profile.passengers = passengers;
                                }
                            });

                            api.cabinet.GetDocketsRequest($http, $scope.user).send(function (resp) {
                                $scope.profile.dockets = resp.dockets;
                            });
                        });

                    };

                    var paxMergeErrorCallback = function (code) {
                        api.alert(api.getErrorLocalizedMessage(code), function () {});
                    };

                    var getRequest = function (pax) {
                        return function () {
                            return api.cabinet.MergePaxRequest($http, $scope.user, pax);
                        };
                    };

                    for (var i = 0; i < paxes.length; i++) {
                        var pax = paxes[i];

                        api.cabinet.RequestWithLoginRetry($http, $scope,
                            getRequest(pax), paxMergeCallback, paxMergeErrorCallback);
                    }

                    var payerRequest = api.cabinet.MergePayerRequest($http, $scope.user, payer);
                    api.cabinet.RequestWithLoginRetry($http, $scope,function(){return payerRequest;},
                        paxMergeCallback, paxMergeErrorCallback);
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'common', 'modal', null, function (template) {
                    angular.element('body').append(template);
                });

                resourceLoader.get('css', 'profile', 'profile' + options.css_suffix);

                var element = angular.element($element);
                angular.element('body').append(element.remove());

                $scope.$watch('user.isLogged', function (isLogged, value) {
                    var user = triptop.profile.user;
                    if (isLogged === true) {
                        api.cabinet.RequestWithLoginRetry($http, $scope, function ($http, $scope) {
                            return api.cabinet.GetProfileRequest($http, user);
                        }, function (profile) {
                            api.cabinet.GetDocketsRequest($http, user).send(function (resp) {
                                profile.dockets = resp.dockets;
                            });
                            api.cabinet.GetPassengersRequest($http, user).send(function (resp) {
                                var passengers = resp.passengers;
                                if(passengers) {
                                    profile.passengers = passengers;
                                }
                                triptop.profile.fireCallbacks('checking', [true, profile]);
                            });
                            $scope.profile = profile;
                            resourceLoader.get('module', 'profile', 'logged', null, function () {
                                $element.html('<logged></logged>');
                                $compile($element.contents())($scope);
                            });
                        }, function () {
                            resourceLoader.get('module', 'profile', 'unlogged', null, function () {
                                $element.html('<unlogged></unlogged>');
                                $compile($element.contents())($scope);
                            });
                        });
                    }
                    else {
                        triptop.profile.fireCallbacks('checking', [false]);
                        resourceLoader.get('module', 'profile', 'unlogged', null, function () {
                            $element.html('<unlogged></unlogged>');
                            $compile($element.contents())($scope);
                        });
                    }
                });

                if(options.profile && options.profile.ready) {
                    options.profile.ready();
                }
            }
        };
    }]);
}(window.triptop));