'use strict';

/**
 * @file Set of auxiliary functions.
 * @author amglin@trip-top.com
 * @module triptop/api/utils
 */

(function (triptop) {
    if (!triptop || !triptop.options || !triptop.widgets || !triptop.api) {
        throw new Error('Application not configured');
    }

    // Application settings, main module and utility namespace
    var options = triptop.options;
    var widgets = triptop.widgets;
    var api = triptop.api;

    /**
     * Application dictionaries.
     * @namespace triptop.dictionaries
     */
    if(!triptop.dictionaries) { triptop.dictionaries = {}; }

    /**
     * Currencies dictionary.
     * @member {Array.<object>} currencies
     * @memberof triptop.dictionaries
     */
    if(!triptop.dictionaries.currencies) {
        triptop.dictionaries.currencies = {
            USD: { left: '$' },
            EUR: { left: '€' },
            ILS: { right: '₪' },
            GBP: { left: '£' },
            RUB: { right: 'руб' }
        };
    }

    /**
     * Returns Date() object.
     * @function parseDate
     * @memberof triptop.api
     * @param {string} str Date value as string.
     * @param {string} delim Delimeter char.
     * @return {date} Date object. 
     */
    api.parseDate = function (str, delim) {
        if(!str) { return null; }
        var ret, parts = str.split(delim ? delim : '/');
        if (parts.length == 5) {
            ret = new Date(parts[2], parts[1] - 1, parts[0], parts[3], parts[4], 0, 0);
        }
        else if (parts.length == 3) {
            ret = new Date(parts[2], parts[1] - 1, parts[0]);
        }
        else {
            ret = new Date();
        }
        return new Date(ret.getTime() - ret.getTimezoneOffset()*60*1000);
    };

    /**
     * Returns document type.
     * @function getDocType
     * @memberof triptop.api
     * @return {string} Returns constant defining document type.
     */
    api.getDocType = function () {
        var s = '';
        var d = 'no_doctype';
        if (document.doctype) {
            s = document.doctype.publicId;
            if (!s || s.length === 0) {
                return 'html_5';
            }
            s = s.toLowerCase();
        }
        else {
            var p = document.body.parentNode;
            if (!p) {
                return d;
            }
            s = p.parentNode.firstChild.nodeValue;
            if (!s || s.length === 0) {
                return d;
            }
            s = s.toLowerCase();
            if (s.indexOf('public') < 0) {
                return 'html_5';
            }
        }

        var v = '';
        if (s.indexOf('xhtml 1.1') > 0) {
            return 'xhtml_1_1';
        }
        else if (s.indexOf('xhtml 1.0') > 0) {
            v = 'xhtml_1';
        }
        else if (s.indexOf('html 4') > 0) {
            v = 'html_4';
        }
        else {
            return d;
        }
        if (s.indexOf('transitional') > 0) {
            v += '_transitional';
        }
        else if (s.indexOf('frameset') > 0) {
            v += '_frameset';
        }
        else {
            v += '_strict';
        }
        return v;
    };

    /**
     * Returns unique id.
     * @function createUUID
     * @memberof triptop.api
     * @return {string} Returns unique id. 
     * @deprecated For old application only.
     */
    api.createUUID = function () {
        var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split(''),
            uuid = new Array(36), rnd = 0, r;
        for (var i = 0; i < 36; i++) {
            if (i == 8 || i == 13 || i == 18 || i == 23) {
                uuid[i] = '-';
            }
            else if (i == 14) {
                uuid[i] = '4';
            }
            else {
                if (rnd <= 0x02) {
                    rnd = 0x2000000 + (Math.random() * 0x1000000) | 0;
                }
                r = rnd & 0xf;
                rnd = rnd >> 4;
                uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r];
            }
        }
        return uuid.join('');
    };

    /**
     * Compress date.
     * @function compressDate
     * @memberof triptop.api
     * @param {date} date Date value.
     * @return {number} Returns compressed value for specific date.
     * @deprecated For old application only.
     */
    api.compressDate = function (date) {
        var year = date.getYear();
        var month = date.getMonth();
        var day = date.getDate();
        var hours = date.getHours();
        var minutes = date.getMinutes();
        return minutes | hours << 6 | day << 11 | (month + 1) << 16 | year << 20;
    };

    /**
     * Decompress date.
     * @function decompressDate
     * @memberof triptop.api
     * @param {number} compressed Compressed value.
     * @return {date} Returns date for specific compressed value.
     * @deprecated For old application only.
     */
    api.decompressDate = function (compressed) {
        var year = compressed >> 20;
        var month = ( compressed >> 16 ) & 0x0f;
        var day = ( compressed >> 11 ) & 0x1f;
        var hours = ( compressed >> 6 ) & 0x1f;
        var minutes = compressed & 0x3f;
        return new Date(year + 1900, month - 1, day, hours, minutes);
    };
/*
    api._Request = function ($http, params) {
        if (!api.requestManager) {
            api.requestManager = { id: 0 };
        }
        return {
            path: '',
            params: function () {
                if (!params) {
                    return '';
                }

                var data = {
                    key: options.key,
                    locale: options.locale
                };
                angular.extend(data, params);

                var parts = [];
                angular.forEach(data, function(value, key) {
                    if(value) {
                        if(angular.isObject(value)) {
                            value = api.toJson(value);
                        }
                        if(angular.isString(value)) {
                            parts.push(encodeURIComponent(key) + '=' + encodeURIComponent(value));
                        }
                    }
                }, parts);

                return parts.join('&');
            },
            send: function (success, error) {
                var url = options.paths.services.main + this.path;
                url = url + ((url.indexOf('?') == -1) ? '?' : '&') + this.params();
                $http.jsonp(url)
                    .success(function (json) {
                        if (json.error && error) {
                            error(json.error);
                        }
                        else if (success) {
                            success(json);
                        }
                    }).error(function (json) {
                        if(error) {
                            error(json);
                        }
                    });
            }
        };
    };
*/
    /**
     * Tag directive <processwait> to implement waiting process. Using template 'common/templates/wait.html'.
     * @member {angular.Directive} processwait
     * @memberof triptop.widgets
     */
    widgets.directive('processwait', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'common', 'wait', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    /**
     * Tag directive <loadmodule> to implement loading other modules.
     * @member {angular.Directive} loadmodule
     * @memberof triptop.widgets
     * @property {string} module Attribute specifies the name of the module.
     */
    widgets.directive('loadmodule', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                var module = $attrs.module;
                if(!module) { return; }

                var tags = module.split('/');
                if(tags.length < 3) { return; }

                if(tags.length < 4) { tags.push(tags[2]); }

                resourceLoader.module(tags[0], tags[1], function() {
                    $element.html('<' + tags[3] + '></' + tags[3] + '>');
                    $compile($element.contents())($scope);
                }, undefined, tags[2]);
            }
        };
    }]);

    /**
     * Attribute directive 'datepicker' to implement bootstrap datepicker.
     * @member {angular.Directive} datepicker
     * @memberof triptop.widgets
     * @property {string} startDate Attribute specifies the start date offset.
     * @property {string} endDate Attribute specifies the end date offset.
     */
    widgets.directive('datepicker', function (dateFilter) {
        return {
            require: 'ngModel',
            replace: true,
            template: '<input style="display:block !important;"/>',
            link: function ($scope, $element, $attrs, $ctrl) {
                var elem = window.jQuery($element);

                $ctrl.$formatters.unshift(function (modelValue) {
                    if(modelValue && modelValue.length > 0) {
                        elem.bootstrapDatepicker('_setDate', modelValue);
                        elem.bootstrapDatepicker('update');                        
                    }
                    return dateFilter(modelValue, 'dd.MM.yyyy');
                });

                $ctrl.$parsers.unshift(function (viewValue) {
                    return viewValue ? api.parseDate(viewValue, '.') : '';
                });

                elem.bootstrapDatepicker({
                    format: "dd.mm.yyyy",
                    autoclose: true,
                    todayHighlight: true,
                    rtl: options.locale === 'he_IL' ? true : false,
                    startDate: $attrs.startDate ? $attrs.startDate : undefined,
                    endDate: $attrs.endDate ? $attrs.endDate : undefined
                }).on('changeDate', function (e) {
                    $ctrl.$setViewValue(e.target.value);
                }).on('clearDate', function (e) {
                    $ctrl.$setViewValue(null);
                });
            }
        };
    });

    /**
     * Attribute directive 'jquery-datepicker' to implement bootstrap datepicker.
     * @member {angular.Directive} jquery-datepicker
     * @memberof triptop.widgets
     * @property {string} months Attribute specifies the number of months to display.
     */
    widgets.directive('jqueryDatepicker', function () {
        return {
            require: 'ngModel',
            replace: true,
            template: '<input style="display:block !important;"/>',
            link: function ($scope, $element, $attrs, $ctrl, ngModel) {
                var elem = window.jQuery($element),
                    months = $attrs.months ? $attrs.months : 2,
                    date,
                    startDate = 0, endDate = 0,
                    shiftDate = function(date) {
                        if (/^[\-+]\d+[dmwy]([\s,]+[\-+]\d+[dmwy])*$/.test(date)) {
                            var part_re = /([\-+]\d+)([dmwy])/,
                                parts = date.match(/([\-+]\d+)([dmwy])/g),
                                part, dir;
                            date = new Date();
                            date.setHours(0, 0, 0, 0);

                            for (var i=0; i<parts.length; i++) {
                                part = part_re.exec(parts[i]);
                                dir = parseInt(part[1], 10);
                                switch(part[2]){
                                    case 'd':
                                        date.setUTCDate(date.getUTCDate() + dir);
                                        break;
                                    case 'm':
                                        date.setMonth(date.getMonth() + dir);
                                        break;
                                    case 'w':
                                        date.setUTCDate(date.getUTCDate() + dir * 7);
                                        break;
                                    case 'y':
                                        date.setMonth(date.getMonth() + dir * 12);
                                        break;
                                }
                            }
                            return date;
                        }

                        return 0;
                    },
                    parseDate = function(dateString) {
                        var dateParts = dateString.replace(/\b0(?=\d)/g, '').split(".");
                        return new Date(dateParts[2], (parseInt(dateParts[1], 10) - 1), parseInt(dateParts[0], 10));
                    },
                    checkDate = function(date, arr) {
                        for (var i = 0; i < arr.length; i++) {
                            if (date.setHours(0, 0, 0, 0).valueOf() == arr[i].date.valueOf()) {
                                return arr[i].value;
                            }
                        }
                        return null;
                    },
                    dateToString = function(date) {
                        var dd = date.getDate();
                        var mm = date.getMonth() + 1; //January is 0!
                        var yyyy = date.getFullYear();

                        if (dd < 10) {
                            dd = '0' + dd;
                        }
                        if (mm < 10) {
                            mm = '0' + mm;
                        }
                        var dateString = dd + '.' + mm + '.' + yyyy;

                        return dateString;
                    };


                /* Array of test dates & prices */
                var arr = [];
                for (var i = 0; i < 5; i++) {
                    arr.push({
                        date: new Date(2015, 1, 22 + i),
                        value: "$" + (100 + i)
                    });
                }

                if (ngModel) {
                    //date = $scope[$attrs.ngModel];
                    //date = dateToString(date);
                }
                else {
                    date = new Date();
                    date = dateToString(date);
                }

                //var kok = $ngModel;
                //console.log(kok);

                var now = new Date();
                now.setHours(0, 0, 0, 0);

                if ($attrs.startDate) {
                    startDate = shiftDate($attrs.startDate);
                }

                if ($attrs.endDate) {
                    endDate = shiftDate($attrs.endDate);
                }

                elem.DatePicker({
                    format: 'd.m.Y',
                    date: startDate ? dateToString(startDate) : date,
                    current: startDate ? dateToString(startDate) : date,
                    starts: 1,
                    calendars: months,
                    weeks: false,
                    position: 'bottom',
                    onRender: function(date) {
                        //if (!arr) {
                        if (arr) { // "Disabling" test array
                            var start = startDate ? startDate.valueOf() : now.valueOf(),
                                end = endDate.valueOf();
                            return {
                                disabled: elem.get(0).name == "time" ? (date.setHours(0, 0, 0, 0).valueOf() < start) :
                                            elem.get(0).name == "round" ? (date.setHours(0, 0, 0, 0).valueOf() <= $('input[name=time]').DatePickerGetDate()) :
                                                end ? (date.setHours(0, 0, 0, 0).valueOf() > end) : false,
                                className: date.setHours(0, 0, 0, 0).valueOf() == now.valueOf() ? 'datepickerSpecial' : false
                            };
                        }
                        else {
                            return {
                                disabled: !checkDate(date, arr),
                                className: checkDate(date, arr) ? 'datepickerPrice' : false,
                                value: checkDate(date, arr)
                            };
                        }
                    },
                    onBeforeShow: function() {
                        date = elem.val() ? elem.val() : startDate ? dateToString(startDate) : date;
                        elem.DatePickerSetDate(date, true);
                    },
                    onChange: function(formated, dates) {
                        elem.val(formated);
                        $ctrl.$setViewValue(parseDate(elem.val()));
                        elem.DatePickerHide();
                    }
                });
            }
        };
    });

    /**
     * Attribute directive 'timepicker' to implement timepicker.
     * @member {angular.Directive} timepicker
     * @memberof triptop.widgets
     * @property {string} timepicker Attribute specifies the default value.
     */
    widgets.directive('timepicker', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function ($scope, $element, $attrs, $ctrl) {
                var elem = window.jQuery($element);

                $scope.setTimePicker = function (date, time) {
                    if(!date) { return; }
                    if(time && time.length > 0) {
                        time = time.split(':');
                        if(time.length == 2) {
                            try {
                                date.setHours(parseInt(time[0], 10), parseInt(time[1], 10));
                            }
                            catch (e) {}
                        }
                    }
                    return date;
                };

                elem.timepicker({
                    timeFormat: 'H:i'
                }).on('changeTime', function (e) {
                    $ctrl.$setViewValue(e.target.value);
                });

                elem.timepicker('setTime', $scope.setTimePicker(new Date(), $attrs.timepicker));
            }
        };
    }]);

    /**
     * Attribute directive 'combobox' to implement combobox plugin.
     * @member {angular.Directive} combobox
     * @memberof triptop.widgets
     * @property {string} combobox Attribute specifies the binding object.
     * @property {string} lookup Attribute specifies the lookup function in the scope.
     */
    widgets.directive('combobox', function() {
        return {
            restrict: 'A',
            link: function($scope, $element, $attrs) {
                var lookup = $attrs.lookup, onLookup;
                if(lookup && lookup.length > 0 && $scope[lookup]) {
                    onLookup = function(query) {
                        if(query && query.length > 2) {
                            $scope[lookup].apply($scope, [query]);
                        }
                    };
                }
                
                var options = { onLookup: onLookup };
                var data = $attrs.combobox;
                if(data && data.length > 0) {
                    if(!$scope[data]) {
                        $scope[data] = null;
                    }

                    $scope.$watch(data, function() {
                        $scope.$evalAsync(function() {
                            var combobox = $element.data('combobox');
                            if(combobox) {
                                combobox.refresh(onLookup !== undefined);
                            }
                            else {
                                $element.combobox(options);
                            }
                        });
                    });
                }
                else {
                    $scope.$evalAsync(function() {
                        $element.combobox(options);
                    });
                }
            }
        };
    });

    /**
     * Attribute directive 'selectize' to implement selectize plugin.
     * @member {angular.Directive} selectize
     * @memberof triptop.widgets
     * @property {string} selectize Attribute specifies the binding object.
     * @property {string} method Attribute specifies the functions set in the scope.
     * @property {string} options Attribute specifies the options set.
     */
    widgets.directive('selectize', function () {
        return {
            restrict: 'A',
            scope: true,
            link: function ($scope, $element, $attrs) {
                var model = $attrs.selectize;
                if(!model) { return; }

                var config = {}, method = $attrs.method;
                if(method) {
                    method = method.charAt(0).toUpperCase() + method.slice(1);

                    config = $scope['config' + method];
                    if(config && angular.isFunction(config)) {
                        config = config();
                    }
                    else {
                        config = {};    
                    }

                    var getter = $scope['get' + method];
                    if(getter && angular.isFunction(getter)) {
                        config.getter = getter;
                    }

                    var setter = $scope['set' + method];
                    if(setter && angular.isFunction(setter)) {
                        config.setter = setter;
                    }

                    var find = $scope['find' + method];
                    if(find && angular.isFunction(find)) {
                        config.find = find;
                    }
                }

                var options = $attrs.options;
                if(options) {
                    options = options.split(',');
                    for (var i = options.length - 1; i >= 0; i--) {
                        options[i] = { value: options[i], text: options[i] };
                    }
                    config.options = options;
                }
                else if(config.getter) {
                    config.options = config.getter();
                }

                var flag = 0;

                config.onChange = function (value) {
                    if(flag === 0) {
                        flag = -1;
                        $scope.$eval(model + '=' + (angular.isNumber(value) ? value : '"' + value + '"'));
                        if(config.setter) {
                            config.setter(value);
                        }
                        flag = 0;
                    }
                };

                config.sortField = [
                    { field: "text", direction: "asc" }
                ];
                config.plugins = ['continue_editing'];

                $scope.selectize = $element.selectize(config)[0].selectize;

                $scope.$watch(model, function (value) {
                    if(flag === 0) {
                        flag = 1;
                        var option = $scope.selectize.getOption(value);
                        if(option.length === 0 && config.find) {
                            var item = config.find(value);
                            if(item) {
                                $scope.selectize.addOption(item);
                                $scope.selectize.setValue(value);
                            }
                        }
                        else {
                            $scope.selectize.setValue(value);
                        }
                        flag = 0;
                    }
                });
            }
        };
    });

    /**
     * Attribute directive 'slider' to implement slider plugin.
     * @member {angular.Directive} slider
     * @memberof triptop.widgets
     * @property {string} slider Attribute specifies the functions set in the scope.
     */
    widgets.directive('slider', function () {
        return {
            restrict: 'A',
            scope: true,
            link: function ($scope, $element, $attrs) {
                var method = $attrs.slider;
                if(!method) { return; }

                var config = {};
                if(method) {
                    method = method.charAt(0).toUpperCase() + method.slice(1);

                    config = $scope['config' + method];
                    if(config && angular.isFunction(config)) {
                        config = config();
                    }
                    else {
                        config = {};    
                    }

                    var change = $scope['change' + method];
                    if(change && angular.isFunction(change)) {
                        config.change = change;
                    }
                }
                config.tooltip = 'hide';

                $element.slider(config).on('slideStop', function(event) {
                    if(config.change) {
                        config.change(event, 'stop');
                    }
                }).on('slide', function(event) {
                    if(config.change) {
                        config.change(event);
                    }
                });
            }
        };
    });

    /**
     * Attribute directive 'iframeOnload' to implement handle on loading iframe.
     * @member {angular.Directive} iframeOnload
     * @memberof triptop.widgets
     */
    widgets.directive('iframeOnload', [function () {
        return {
            scope: {
                callBack: '&iframeOnload'
            },
            link: function($scope, $element, $attrs) {
                $element.on('load', function() {
                    return $scope.callBack();
                });
            }
        };
    }]);

    /**
     * Filter 'prices' to implement displaying price with currency symbol.
     * @function prices
     * @memberof triptop.widgets
     * @param {string|number} value Price value.
     * @param {string?} currency Currency code.
     * @return {angular.Filter} Returns filter 'prices'.
     */
    widgets.filter('prices', function() {
        return function(value, currency) {
            if(!currency) {
                currency = 'USD';
            }
            if(typeof value === 'number') {
                if(value === 0) {
                    return 'N/A';
                }
                value = value.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
            }

            var currencies = triptop.dictionaries.currencies;
            if(currencies[currency].right) {
                return value + ' ' + currencies[currency].right;
            }
            if(currencies[currency].left) {
                return currencies[currency].left + ' ' + value;
            }
            return value;
        };
    });

    /**
     * Filter 'airlineIcon' to implement displaying url for airline logo.
     * @function airlineIcon
     * @memberof triptop.widgets
     * @param {object} segment Segment object.
     * @return {angular.Filter} Returns filter 'airlineIcon'.
     */
    widgets.filter('airlineIcon', function() {
        return function(segment) {
            if(!segment.airline || !segment.airline.code) { return ''; }
            return '//static.trip-top.com/triptop/gallery/triptop-gallery/images/air/' + segment.airline.code.toLowerCase() + '.jpg';
        };
    });

    /**
     * Filter 'starsRange' to create array for displaying hotel rate.
     * @function starsRange
     * @memberof triptop.widgets
     * @param {number} stars Stars amount.
     * @return {angular.Filter} Returns filter 'starsRange'.
     */
    widgets.filter('starsRange', function() {
        return function(stars) {
            return new Array(stars);
        };
    });

    /**
     * Filter 'range' to create range array.
     * @function range
     * @memberof triptop.widgets
     * @param {Array.<object>} input Created array.
     * @param {number} min Minimum value for range.
     * @param {number} max Maximum value for range.
     * @param {number} step Step for range.
     * @return {angular.Filter} Returns filter 'range'.
     */
    widgets.filter('range', function() {
        return function(input, min, max, step) {
            min = parseInt(min, 10);
            max = parseInt(max, 10);
            step = step ? parseInt(step, 10) : 1;
            for (var i = min; i < max; i++) { input.push(i * step); }
            return input;
        };
    });

    /**
     * Filter 'orderObjectBy' to create range array.
     * @function orderObjectBy
     * @memberof triptop.widgets
     * @param {Array.<object>} items Sorted array.
     * @param {string} field Property for sorting.
     * @param {boolean} reverse Sort direction.
     * @return {angular.Filter} Returns filter 'orderObjectBy'.
     */
    widgets.filter('orderObjectBy', function () {
        return function (items, field, reverse) {
            var filtered = [];
            angular.forEach(items, function (item) {
                filtered.push(item);
            });
            filtered.sort(function (a, b) {
                return (a[field] > b[field]);
            });
            if (reverse) {
                filtered.reverse();
            }
            return filtered;
        };
    });
}(window.triptop));