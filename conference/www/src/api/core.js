'use strict';

/**
 * @file Implementation of the basic functions of processing resources. 
 * @author amglin@trip-top.com
 * @module triptop/api/core
 */

/**
 * @module triptop/booking
 */

/**
 * @module triptop/common
 */

/**
 * @module triptop/hotels
 */

/**
 * @module triptop/insurance
 */

/**
 * @module triptop/packages
 */

/**
 * @module triptop/payment
 */

/**
 * @module triptop/profile
 */

/**
 * @module triptop/rentacar
 */

/**
 * @module triptop/transfers
 */

/** 
 * Global angular namespace.
 * @namespace angular
 */

/**
 * Creates a new angular module.
 * @class angular.Module
 */

/**
 * Creates a new angular factory.
 * @class angular.Factory
 */

/**
 * Creates a new angular provider.
 * @class angular.Provider
 */

/**
 * Creates a new angular directive.
 * @class angular.Directive
 */

/**
 * Creates a new angular сonfig.
 * @class angular.Config
 */

/**
 * Creates a new angular contoller.
 * @class angular.Contoller
 */

/**
 * Creates a new angular service.
 * @class angular.Service
 */

/**
 * Creates a new angular filter.
 * @class angular.Filter
 */

(function (triptop) {
    if (!triptop || !triptop.options || !triptop.options.paths) {
        throw new Error('Application not configured');
    }

    // Application settings
    var options = triptop.options;
    options.css_suffix = options.locale && options.locale === 'he_IL' ? '_rtl' : '';

    /**
     * Main angular application module and namespace
     * @namespace triptop.widgets
     */
    var widgets = triptop.widgets = angular.module('triptopWidgets', ['ngAnimate'])
        .config(function ($sceProvider) {
            $sceProvider.enabled(false);
        });

    /**
     * Creates a new resource сache.
     * @class ResourceCacheFactory
     * @extends {angular.Factory}
     * @memberof triptop.widgets
     */
    widgets.factory('$resourceCache', ['$cacheFactory', function ($cacheFactory) {
        return $cacheFactory('$resourceCache');
    }]);

    /**
     * Provider for processing resources.
     * @class ResourceLoaderProvider
     * @extends {angular.Provider}
     * @memberof triptop.widgets
     */
    widgets.provider('resourceLoader', ['$injector', '$provide', '$controllerProvider', '$compileProvider', '$filterProvider', function ($injector, $provide, $controllerProvider, $compileProvider, $filterProvider) {
        var providers = {
            $injector: $injector,
            $provide: $provide,
            $controllerProvider: $controllerProvider,
            $compileProvider: $compileProvider,
            $filterProvider: $filterProvider
        };

        // Map of dependencies
        this.dependencies = {};

        /**
         * Added new dependency for specific resource.
         * @function addDependency
         * @memberof triptop.widgets.ResourceLoaderProvider
         * @private
         * @param {string} name Resource name.
         * @param {object} dependency Dependency object.
        */
        this.addDependency = function (name, dependency) {
            if (!this.dependencies[name] && angular.isFunction(dependency)) {
                this.dependencies[name] = dependency;
            }
            return this;
        };

        // Getter for resourceLoader provider
        this.$get = ['$http', '$document', '$resourceCache', '$templateCache', function ($http, $document, $resourceCache, $templateCache) {
            var dependencies = this.dependencies;

            return {
                // Private function invoked inner angular queue
                getInvokeQueues: function (name) {
                    if (!widgets._invokeQueue) {
                        return false;
                    }
                    for (var i = 0; i < widgets._invokeQueue.length; i++) {
                        var queue = widgets._invokeQueue[i];
                        try {
                            if (queue[2][0] == name) {
                                return widgets._invokeQueue.slice(i);
                            }
                        }
                        catch (e) {}
                    }
                    return [];
                },
                /**
                 * Loading resources
                 * @function get
                 * @memberof triptop.widgets.ResourceLoaderProvider
                 * @param {string} type Type of loaded resource.
                 * @param {string} module Module name.
                 * @param {string} name Name of loaded resource.
                 * @param {string} dependency Name of dependency object.
                 * @param {function} success Success callback function.
                 * @param {function} error Error callback function.
                 */
                get: function (type, module, name, dependency, success, error) {
                    var _this = this;
                    var run = function () {
                        var method = _this[type];
                        if (method) {
                            method.apply(_this, [module, name, success, error]);
                        }
                        else if (success) {
                            success();
                        }
                    };
                    if (dependency) {
                        this.dependency.apply(this, [module, dependency, run]);
                    }
                    else {
                        run();
                    }
                },
                // Private function invoked resource from cache
                _applyResourceCache: function (resourceId, data, defaultApply) {
                    var inCache = $resourceCache.get(resourceId);
                    if (angular.isArray(inCache)) {
                        for (var i = 0; i < inCache.length; i++) {
                            var f = inCache[i];
                            if(f) { f(data); }
                        }
                        $resourceCache.put(resourceId, 1);
                    }
                    else if (angular.isFunction(defaultApply)) {
                        defaultApply(data);
                    }
                },
                /**
                 * Loading dependency
                 * @function dependency
                 * @memberof triptop.widgets.ResourceLoaderProvider
                 * @param {string} module Module name.
                 * @param {string} name Name of loaded dependency.
                 * @param {function} success Success callback function.
                 * @param {function} error Error callback function.
                 */
                dependency: function (module, name, success, error) {
                    var dependencyId = 'dependency:' + name, inCache = $resourceCache.get(dependencyId);
                    if (name && dependencies[name] && !inCache) {
                        $resourceCache.put(dependencyId, [success]);
                        try {
                            var _this = this;
                            dependencies[name].apply(this, [this, function() { _this._applyResourceCache(dependencyId); } ]);
                        }
                        catch (e) {
                            $resourceCache.remove(dependencyId);
                            if (error) { error(); }
                        }
                    }
                    else if (angular.isArray(inCache)) {
                        inCache.push(success);
                    }
                    else {
                        if (success) { success(); }
                    }
                },
                /**
                 * Loading module
                 * @function module
                 * @memberof triptop.widgets.ResourceLoaderProvider
                 * @param {string} module Name of parents module.
                 * @param {string} name Name of loaded module.
                 * @param {function} success Success callback function.
                 * @param {function} error Error callback function.
                 */
                module: function (module, name, success, error, first) {
                    var moduleId = 'module:' + module + ':' + name, invokeQueues = this.getInvokeQueues(first ? first : name), inCache = $resourceCache.get(moduleId);
                    if (name && invokeQueues.length === 0 && !inCache) {
                        $resourceCache.put(moduleId, [success]);
                        var _this = this;
                        this.get('js', module, name, null, function () {
                            var invokeQueues = _this.getInvokeQueues(first ? first : name);
                            for (var i = 0; i < invokeQueues.length; i++) {
                                var invokeArgs = invokeQueues[i];
                                try {
                                    var defName = invokeArgs[2][0] + 'Def';
                                    if(!$resourceCache.get(defName)) {
                                        var provider = providers[invokeArgs[0]];
                                        if (provider) {
                                            provider[invokeArgs[1]].apply($compileProvider, invokeArgs[2]);
                                            $resourceCache.put(defName, 1);
                                        }
                                    }
                                }
                                catch (e) {
                                    if (error) { error(e); }
                                    return;
                                }
                            }
                            _this._applyResourceCache(moduleId);
                        });
                    }
                    else if (angular.isArray(inCache)) {
                        inCache.push(success);
                    }
                    else {
                        if (success) { success(); }
                    }
                },
                /**
                 * Loading javascript
                 * @function js
                 * @memberof triptop.widgets.ResourceLoaderProvider
                 * @param {string} module Module name.
                 * @param {string} name Name of loaded javascript resource.
                 * @param {function} success Success callback function.
                 * @param {function} error Error callback function.
                 */
                js: function (module, name, success, error) {
                    this._js(module, name, success, error, false);
                },
                /**
                 * Loading localized static dataset
                 * @function data
                 * @memberof triptop.widgets.ResourceLoaderProvider
                 * @param {string} module Module name.
                 * @param {string} name Name of loaded dataset.
                 * @param {function} success Success callback function.
                 * @param {function} error Error callback function.
                 */
                data: function (module, name, success, error) {
                    this._js(module, name, success, error, true);
                },
                // Private function loaded javascript resources
                _js: function (module, name, success, error, localized) {
                    var scriptId = 'script:' + module + ":" + name, scriptElement;
                    if(name == 'jquery/jquery.min') {
                        var jq = false;
                        if(typeof jQuery == 'undefined') {
                            jq = true;
                        }
                        else {
                            var version = parseInt(jQuery.fn.jquery.split('.').join(''), 10);
                            if(version < 16 || (version > 100 && version < 160)) {
                                jq = true;
                            }
                        }
                        if(jq === false) {
                            $resourceCache.put(scriptId, 1);
                        }
                    }

                    var inCache = $resourceCache.get(scriptId);
                    if (!inCache) {
                        $resourceCache.put(scriptId, [success]);
                        scriptElement = $document[0].createElement('script');
                        scriptElement.charset = 'UTF-8';
                        var path = module ? options.paths.js[module] : '';
                        if (module && localized === true && options.devmode !== true) {
                            path = path + (options.locale ? (options.locale + '/') : '');
                        }
                        scriptElement.src = path + name + '.js?v=' + options.version;
                        var _this = this;
                        if (scriptElement.readyState) {
                            scriptElement.onreadystatechange = function() {
                                if (scriptElement.readyState == "loaded" || scriptElement.readyState == "complete") {
                                    scriptElement.onreadystatechange = null;
                                    _this._applyResourceCache(scriptId);
                                }
                            };
                        }
                        else {
                            scriptElement.onload = function () { _this._applyResourceCache(scriptId); };
                        }
                        scriptElement.onerror = function () {
                            $resourceCache.remove(scriptId);
                            if (error) { error(); }
                        };
                        $document[0].documentElement.appendChild(scriptElement);
                    }
                    else if (angular.isArray(inCache)) {
                        inCache.push(success);
                    }
                    else {
                        if (success) { success(); }
                    }
                },
                /**
                 * Loading CSS resources
                 * @function css
                 * @memberof triptop.widgets.ResourceLoaderProvider
                 * @param {string} module Module name.
                 * @param {string} name Name of loaded css resource.
                 * @param {function} success Success callback function.
                 * @param {function} error Error callback function.
                 */
                css: function (module, name, success, error) {
                    var cssId = 'css:' + name, styleElement, inCache = $resourceCache.get(cssId);
                    if (!inCache) {
                        $resourceCache.put(cssId, [success]);
                        var path = module ? options.paths.css[module] : '', _this = this;
                        styleElement = $document[0].createElement('link');
                        styleElement.rel = 'stylesheet';
                        styleElement.type = 'text/css';
                        styleElement.href = path + name + '.css?v=' + options.version;
                        styleElement.onload = styleElement.onreadystatechange = function () {
                            _this._applyResourceCache(cssId);
                        };
                        styleElement.onerror = function () {
                            $resourceCache.remove(cssId);
                            if (error) { error(); }
                        };
                        $document[0].documentElement.getElementsByTagName('head')[0].appendChild(styleElement);
                    }
                    else if (angular.isArray(inCache)) {
                        inCache.push(success);
                    }
                    else {
                        if (success) { success(); }
                    }
                },
                /**
                 * Loading template resources
                 * @function templates
                 * @memberof triptop.widgets.ResourceLoaderProvider
                 * @param {string} module Module name.
                 * @param {string} name Name of loaded template resource.
                 * @param {function} success Success callback function.
                 * @param {function} error Error callback function.
                 */
                templates: function (module, name, success, error) {
                    var path = options.paths.templates[module];
                    if (options.devmode !== true) {
                        path = path + (options.locale ? (options.locale + '/') : '');
                    }
                    var templateId = 'template:' + name, inCache = $resourceCache.get(templateId);
                    if (!inCache) {
                        $resourceCache.put(templateId, [success]);
                    }
                    else if (angular.isArray(inCache)) {
                        inCache.push(success);
                    }
                    var _this = this;
                    if (options.devmode === true || options.apimode !== 'embedded') {
                        $http({method: 'GET', url: path + name + '.html?v=' + options.version, cache: $templateCache}).
                            success(function (data, status) {
                                _this._applyResourceCache(templateId, data, success);
                            }).
                            error(function (data, status) {
                                $resourceCache.remove(templateId);
                                if (error) { error(data || "Request failed"); }
                            });
                    }
                    else {
                        var url = path + name + '.html.js?v=' + options.version;
                        var cached = $templateCache.get(url);
                        if (!cached) {
                            var key = (options.locale ? (options.locale + '/') : '') + name;
                            if (!triptop.callback) {
                                triptop.callback = function (key, data) {
                                    if (triptop.callbacks[key]) {
                                        triptop.callbacks[key](data);
                                        delete triptop.callbacks[key];
                                    }
                                };
                                triptop.callbacks = {};
                            }
                            triptop.callbacks[key] = function (data) {
                                $templateCache.put(url, data);
                                _this._applyResourceCache(templateId, data, success);
                            };
                            $http.jsonp(url + '&callback=triptop.widgets.callback');
                        }
                        else {
                            _this._applyResourceCache(templateId, cached, success);
                        }
                    }
                }
            };
        }];
    }]);

    /**
     * Safety angular.$scope.$apply function call.
     * @function safeApply
     * @memberof triptop.widgets
     */
    widgets.safeApply = function ($scope, fn) {
        var phase = $scope.$root.$$phase;
        if(phase == '$apply' || phase == '$digest') {
            if(fn) { fn(); }
        }
        else {
            $scope.$apply(fn);
        }
    };

    // Invoking ready callback function
    if (options.ready) {
        options.ready();
    }
}(window.triptop));