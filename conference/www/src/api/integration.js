'use strict';

if(!window.triptop) { window.triptop = {}; }

var path = '//localhost:8080/triptop-widgets/';
var locale = window.__gwt_Locale;
var key = window.document.cl_init_config.key;

triptop.options = {
	locale: !locale ? 'en' : (locale === 'he-IL' ? 'he_IL' : locale),
	key: key,
	paths: {
		basedir: path + 'src/',
		css: {
			libs: path + 'libs/',
			booking: path + 'src/booking/less/',
			common: path + 'src/common/less/',
			profile: path + 'src/profile/less/',
            payment: path + 'src/payment/less/'
        },
		js: {
			libs: path + 'libs/',
			api: path + 'src/api/',
			booking: path + 'src/booking/modules/',
			common: path + 'src/common/modules/',
			data: path + 'src/common/data/',
			profile: path + 'src/profile/modules/',
            payment: path + '/src/payment/modules/'
        },
		templates: {
			booking: path + 'src/booking/templates/',
			common: path + 'src/common/templates/',
			profile: path + 'src/profile/templates/',
            payment: path + '/src/payment/templates/'
        },
		services: {
			main: '//il.trip-top.com/triptop-fo-api-war/api',
			mainapp: '//il.trip-top.com/main-server-app/dispatch'
		}
	},
	ready: function () {
		if(!window.triptopApi) { window.triptopApi = {}; }
		if(!window.triptopApi.ctrlFlags) { window.triptopApi.ctrlFlags = {}; }
		angular.extend(window.triptopApi.ctrlFlags, {
			'showHotelFilter': true,
			'hotelFilterBudget': [
				{title: '0$ - 100$', range: [0, 10000]},
				{title: '100$ - 200$', range: [10000, 20000]},
				{title: '200$ - 300$', range: [20000, 30000]},
				{title: '300$ - 500$', range: [30000, 50000]},
				{title: '500$', range: [50000]}
			]
		});
	},
	version: undefined,
	apimode: 'embedded',
	devmode: true
};

var paths = triptop.options.paths;

document.write('<cabinet></cabinet>');
document.write('<booking></booking>');

var jq = false;
if(!window.jQuery) {
	jq = true;
}
else {
	var version = parseInt(jQuery.fn.jquery.split('.').join(''), 10);
	if(version < 160) {
		jq = true;
	}
}
if(jq === true) {
	document.write('\x3Cscript src="' + paths.js.libs + 'jquery/jquery.min.js">\x3C/script>');
}

if(!window.angular) {
	document.write('\x3Cscript src="' + paths.js.libs + 'angular/angular.min.js">\x3C/script>');
	document.write('\x3Cscript src="' + paths.js.libs + 'angular/angular-animate.min.js">\x3C/script>');
}

document.write('\x3Cscript src="' + paths.js.api + 'core.js?v=' + triptop.options.version + '">\x3C/script>');
document.write('\x3Cscript src="' + paths.js.api + 'api.js?v=' + triptop.options.version + '">\x3C/script>');
document.write('\x3Cscript src="' + paths.js.api + 'utils.js?v=' + triptop.options.version + '">\x3C/script>');
document.write('\x3Cscript src="' + paths.js.profile + 'cabinet.js?v=' + triptop.options.version + '">\x3C/script>');
document.write('\x3Cscript src="' + paths.js.booking + 'booking.js?v=' + triptop.options.version + '">\x3C/script>');
document.write('\x3Cscript src="' + paths.js.common + 'terms.js?v=' + triptop.options.version + '">\x3C/script>');
document.write('\x3Cscript src="' + paths.js.payment + 'payment.js?v=' + triptop.options.version + '">\x3C/script>');

var plugins = window.triptop.plugins;
if(plugins) {
	for (var i = 0; i < plugins.length; i++) {
		plugins[i](window.jQuery);
	}
}

document.write('\x3Cscript>jQuery(document).ready(function(){angular.bootstrap(document,["triptopWidgets"]);});\x3C/script>');