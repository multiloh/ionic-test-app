'use strict';

/**
 * @file Implementation of initialization and runing application.
 * @author amglin@trip-top.com
 * @module triptop/api/runner
 */

/** 
 * Global namespace for application.
 * @namespace triptop
 */
if(!window.triptop) { window.triptop = {}; }

/**
 * @function run
 * @memberof triptop
 * @description Implementation function of application launch. Writing and bootstrap the minimum set of libraries for running angular applications.
 * @param {object} options Settings to start the application.
 * @property {Array.<object>} options.modules List of running modules.
 * @property {string} options.modules.module Name of running module.
 * @property {string} options.modules.embedTo Selector to parent element.
 * @property {object} options.modules.options Module settings as {@link triptop.options}.
 * @throws Will throw an error if application not configured.
 */
window.triptop.run = function(options) {

	// Set application basedir. Replaced in the assembly process.
	var path = '';

	/**
	 * Application settings.
	 * @memberof triptop
     * @instance
	 * @property {string} locale Localization option (values: 'en', 'ru', 'he_IL').
	 * @property {string} key Affiliate site access key.
	 * @property {object} paths Mixin object provides resource paths for modules.
	 * @property {function} ready Ready callback function.
	 * @property {number} version Version number (generated in the assembly).
	 * @property {boolean} devmode Switch to development mode (removed in the assembly).
	 */
	triptop.options = {
		locale: !options.locale ? 'en' : (options.locale === 'he-IL' ? 'he_IL' : options.locale),
		key: options.key,
		paths: {
			basedir: path + 'src/',
			css: {
				libs: path + 'libs/',
				common: path + 'src/common/less/'
			},
			js: {
				libs: path + 'libs/',
				api: path + 'src/api/',
				common: path + 'src/common/modules/',
				data: path + 'src/common/data/'
			},
			templates: {
				common: path + 'src/common/templates/'
			},
			services: {
				main: '//il.trip-top.com/triptop-fo-api-war/api',
				mainapp: '//il.trip-top.com/main-server-app/dispatch'
			}
		},
		ready: options.ready,
		version: undefined,
		apimode: 'embedded',
		devmode: true
	};

	// Application path settings
	var paths = triptop.options.paths, i;

	// Check the version and the need to download jQuery
	var jq = false;
	if(!window.jQuery) {
		jq = true;
	}
	else {
		var version = parseInt(jQuery.fn.jquery.split('.').join(''), 10);
		if(version < 100) {
			version = version * 10;
		}
		if(version < 160) {
			jq = true;
		}
	}

	// Loading jQuery if necessary
	if(jq === true) {
		//document.write('\x3Cscript src="' + paths.js.libs + 'jquery/jquery.min.js">\x3C/script>');
		document.write('\x3Cscript src="' + paths.js.libs + 'jquery/jquery.js">\x3C/script>');
	}

	// Loading AngularJS if necessary
	if(!window.angular) {
		document.write('\x3Cscript src="' + paths.js.libs + 'angular/angular.min.js">\x3C/script>');
		document.write('\x3Cscript src="' + paths.js.libs + 'angular/angular-animate.min.js">\x3C/script>');
	}

	// Loading application modules
	document.write('\x3Cscript src="' + paths.js.api + 'api.js">\x3C/script>');
	document.write('\x3Cscript src="' + paths.js.api + 'core.js">\x3C/script>');
	document.write('\x3Cscript src="' + paths.js.api + 'utils.js">\x3C/script>');

	/**
	 * List of application plugins
	 * @memberof triptop
	 * @type {Array.<object>}
	 * @instance
	 * @deprecated Used start options {@link triptop.run}.
	 */
	var plugins = window.triptop.plugins;
	if(plugins) {
		// Running application plugins
		for (i = 0; i < plugins.length; i++) {
			plugins[i](window.jQuery);
		}
	}

	// Bootstrap angular application
	document.write('\x3Cscript>jQuery(document).ready(function(){angular.bootstrap(document,["triptopWidgets"]);var triptop=window.triptop;');
	var modules = options.modules;
	if(modules) {
		for (i = 0; i < modules.length; i++) {
			// Writing to the document load of application functional modules
			var module = modules[i].module;
			if(module) {
				document.write('triptop.api.loader.get("' + module + '",{');
				var embedTo = modules[i].embedTo;
				if(embedTo) {
					//document.write('parent:angular.element("' + embedTo + '")');
					document.write('parent:angular.element(document.getElementById("' + embedTo + '"))');
					//document.write('parent:document.getElementById("' + embedTo + '")'); // THIS
				}
				document.write('});');
				var opts = modules[i].options;
				if(opts) {
					triptop.options[module] = opts;
				}
			}
		}
	}
	document.write('});\x3C/script>');
};