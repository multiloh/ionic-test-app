'use strict';

if(!window.triptop) { window.triptop = {}; }

var path = '//localhost:8080/triptop-widgets/';

triptop.options = {
	paths: {
		basedir: path + 'src/',
		css: {
			libs: path + 'libs/',
			profile: path + 'src/profile/less/',
            payment: path + 'src/payment/less/'
        },
		js: {
			libs: path + 'libs/',
			api: path + 'src/api/',
			data: path + 'src/common/data/',
			profile: path + 'src/profile/modules/',
            payment: path + '/src/payment/modules/'
        },
		templates: {
			profile: path + 'src/profile/templates/',
            payment: path + '/src/payment/templates/'
        },
		services: {
			main: '//il.trip-top.com/triptop-fo-api-war/api',
			mainapp: '//il.trip-top.com/main-server-app/dispatch'
		}
	},
	apimode: 'embedded',
	devmode: true
};

var paths = triptop.options.paths;

document.write('<cabinet></cabinet>');

if(!window.angular) {
	document.write('\x3Cscript src="' + paths.js.libs + 'angular/angular.min.js">\x3C/script>');
	document.write('\x3Cscript src="' + paths.js.libs + 'angular/angular-animate.min.js">\x3C/script>');
}

document.write('\x3Cscript src="' + paths.js.api + 'core.js">\x3C/script>');
document.write('\x3Cscript src="' + paths.js.api + 'api.js">\x3C/script>');
document.write('\x3Cscript src="' + paths.js.api + 'utils.js">\x3C/script>');
document.write('\x3Cscript src="' + paths.js.profile + 'cabinet.js">\x3C/script>');
document.write('\x3Cscript src="' + paths.js.payment + 'payment.js">\x3C/script>');

var plugins = window.triptop.plugins;
if(plugins) {
	for (var i = 0; i < plugins.length; i++) {
		plugins[i](window.jQuery);
	}
}

document.write('\x3Cscript>jQuery(document).ready(function(){angular.bootstrap(document,["triptopWidgets"]);});\x3C/script>');