'use strict';

if(!window.triptop) { window.triptop = {}; }

var path = '//localhost:8080/triptop-widgets/';
var locale = window.__gwt_Locale;

triptop.options = {
    locale: !locale ? 'en' : (locale === 'he-IL' ? 'he_IL' : locale),
    key:0,
    paths: {
        basedir: path + 'src/',
        css: {
            libs: path + 'libs/',
            payment: path + 'src/payment/less/'
        },
        js: {
            libs: path + 'libs/',
            api: path + 'src/api/',
            data: path + 'src/common/data/',
            payment: path + '/src/payment/modules/'
        },
        templates: {
            payment: path + '/src/payment/templates/'
        },
        services: {
            main: '//localhost:8080/triptop-fo-api-war/api',
            mainapp: '//localhost:8080/main-server-app/dispatch'
        }
    },
    apimode: 'embedded',
    devmode: true
};

var paths = triptop.options.paths;

if(!window.angular) {
    document.write('\x3Cscript src="' + paths.js.libs + 'angular/angular.min.js">\x3C/script>');
    document.write('\x3Cscript src="' + paths.js.libs + 'angular/angular-animate.min.js">\x3C/script>');
}

document.write('\x3Cscript src="' + paths.js.api + 'core.js">\x3C/script>');
document.write('\x3Cscript src="' + paths.js.api + 'api.js">\x3C/script>');
document.write('\x3Cscript src="' + paths.js.api + 'utils.js">\x3C/script>');
document.write('\x3Cscript src="' + paths.js.payment + 'payment.js">\x3C/script>');

var plugins = window.triptop.plugins;
if(plugins) {
    for (var i = 0; i < plugins.length; i++) {
        plugins[i](window.jQuery);
    }
}

document.write('\x3Cscript>jQuery(document).ready(function(){angular.bootstrap(document,["triptopWidgets"]);});\x3C/script>');