'use strict';

/**
 * @file Set of auxiliary functions.
 * @author amglin@trip-top.com
 * @module triptop/api/api
 * @deprecated Do not add anything.
 * @todo It is necessary to spread the functions of the respective modules.
 */
(function (triptop, $) {
    /**
     * Auxiliary namespace with utility functions.
     * @namespace triptop.api
     */
    var api = triptop.api = triptop.api || {},
        cabinet = api.cabinet = api.cabinet || {},
        paymentWizard = api.paymentWizard = api.paymentWizard || {},
        transfers = api.transfers = api.transfers || {};
    var cart = api.cart = api.cart || {};

    /**
     * Returns the error message text.
     * @function getErrorLocalizedMessage
     * @memberof triptop.api
     * @param {number} code Error code.
     * @return {string} Error message text. 
     */
    api.getErrorLocalizedMessage = function (code) {
        var msgMap = {
            1: 'emailNotConfirmed',
            2: 'unknownEmail',
            3: 'userOrPasswordIncorrect',
            4: 'userAlreadyExists',
            5: 'pleaseFillRedFields',
            6: 'creditCardDisabled',
            7: 'noDropOffOrPickUp',
            8: 'transferWaitingDuration',
            9: 'fillHotelOrAddress',
            10: 'wrongCaptcha',
            11: 'fillAirlineOrNumber',
            404: 'cantConnect'
        };
        return angular.element('#' + msgMap[code]).text();
    };

    /**
     * Returns the currency symbol.
     * @function getCurrencySymbol
     * @memberof triptop.api
     * @param {string} code Currency code.
     * @return {string} Error message text. 
     */
    api.getCurrencySymbol = function(code) {
        var symbols = {
            USD: '$',
            EUR: '€',
            ILS: '₪',
            GBP: '£',
            RUB: 'руб'
        };
        return symbols[code];
    };

    /**
     * Implementation String.endsWith.
     * @function endsWith
     * @memberof triptop.api
     * @param {string} str String value.
     * @param {string} suffix String suffix.
     * @return {boolean} Return true if this string ends with the specified suffix.
     */
    api.endsWith = function (str, suffix) {
        return str.indexOf(suffix, str.length - suffix.length) !== -1;
    };

    /**
     * The method converts a value to JSON string.
     * @function toJson
     * @memberof triptop.api
     * @param {string} value JSON object.
     * @return {boolean} Return JSON string.
     */
    api.toJson = function (value) {
        return JSON.stringify(value, api.dateReplacer);
    };

    // replaces Date's with corresponding ISO strings
    api.dateReplacer = function (key, value) {
        // angular has some special fields starting with $, that should not go to server
        if (typeof key === 'string' && key.charAt(0) === '$') {
            return undefined;
        }
        if (this[key] instanceof Date) {
            return api.toISOString(this[key]);
        }
        return value;
    };

    // deprecated method
    api.contains = function (a, obj) {
        var i = a.length;
        while (i--) {
            if (a[i] === obj) {
                return true;
            }
        }
        return false;
    };

    // replaces specified fields with js Date()'s in complex json object
    api.parseDates = function (obj, fields) {
        var c = angular.copy(obj);
        for (var f in c) {
            if (!c.hasOwnProperty(f)) {
                continue;
            }

            if (api.contains(fields, f)) {
                c[f] = api.parseDate(c[f], '.');
            } else if (c[f] instanceof Object || c[f] instanceof Array) {
                c[f] = api.parseDates(c[f], fields);
            }
        }
        return c;
    };

    /**
     * Show bootstrap modal dialog.
     * @function message
     * @memberof triptop.api
     * @param {string} title Title value.
     * @param {string} message Message value.
     * @param {function} callback Callback function on closed dialog.
     */
    api.message = function (title, message, callback) {
        var modalMessage = angular.element('#tt-modal-message');

        if(title) {
            modalMessage.find('.modal-header').show().find('.modal-title').html(title);
            modalMessage.find('.modal-header button').hide();
        }
        else {
            modalMessage.find('.modal-header').hide();
        }

        modalMessage.find('.modal-body').html(message);

        var ok = modalMessage.find('.tt-ok');
        ok.click(function () {
            ok.unbind('click');
            modalMessage.modal('hide');
            if (callback) {
                callback();
            }
        });

        modalMessage.find('.tt-no').hide();
        modalMessage.find('.tt-yes').hide();
        
        modalMessage.modal({
            backdrop: false
        });
    };

    /**
     * Show bootstrap modal dialog without title.
     * @function message
     * @memberof triptop.api
     * @param {string} message Message value.
     * @param {function} callback Callback function on closed dialog.
     */
    api.alert = function (message, callback) {
        api.message(undefined, message, callback);
    };

    // deprecated method
    api.getUrlParameter = function (name) {
        name = name.replace(/[\[]/, '[').replace(/[\]]/, ']');
        var regexS = '[\\?&]' + name + '=([^&#]*)';
        var regex = new RegExp(regexS);
        var results = regex.exec(window.location.href);
        return results && results.length > 1 ? results[1] : '';
    };

    // deprecated method
    api.getUrlParams = function () {
        var params = {}, src = window.location.href;
        var index = src.lastIndexOf('?');
        if (index < 0) {
            return params;
        }
        src = src.substring(index + 1);
        var data = src.split('&');
        for (var i = 0; i < data.length; i++) {
            var param = data[i].split('=');
            params[param[0]] = param[1];
        }
        return params;
    };

    // Checking locale and installation RTL
    var lang = window.triptop.api.getUrlParameter('lang');
    if (!lang) {
        lang = triptop.options && triptop.options.locale ? triptop.options.locale : 'en';
    }
    else if (triptop.options) {
        triptop.options.locale = lang;
    }

    if (lang == 'he_IL') {
        document.getElementsByTagName('body')[0].setAttribute('dir', 'rtl');
    }

    // deprecated method
    function forEachSorted (obj, iterator, context) {
        var keys = sortedKeys(obj);
        for (var i = 0; i < keys.length; i++) {
            iterator.call(context, obj[keys[i]], keys[i]);
        }
        return keys;
    }

    // deprecated method
    function sortedKeys (obj) {
        var keys = [];
        for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
                keys.push(key);
            }
        }
        return keys.sort();
    }

    /**
     * Creates url for requests to server side.
     * @function buildUrl
     * @memberof triptop.api
     * @param {string} url The functional part of url request.
     * @param {object} params Mixin with parameters request.
     * @return {string} Url string for requests to server side.
     */
    api.buildUrl = function (url, params) {
        if (!params) {
            return url;
        }
        var parts = [];
        forEachSorted(params, function (value, key) {
            if (value == null || value === undefined) {
                return;
            }
            // handling of arrays of atomic types
            if (Array.isArray(value)) {
                value = value.toString();
            }
            // handling object types
            else
            {
                if (angular.isObject(value)) {
                    value = JSON.stringify(value, api.dateReplacer);
                }
            }
            parts.push(encodeURIComponent(key) + '=' + encodeURIComponent(value));
        });
        if (params.jsessionid) {
            url += ';jsessionid=' + params.jsessionid;
        }
        return url + ((url.indexOf('?') == -1) ? '?' : '&') + parts.join('&');
    };

    /**
     * Creates request to server side.
     * @function Request
     * @memberof triptop.api
     * @param {{angular.Service}} $http The angular HTTP service.
     * @param {object} params Mixin with parameters request.
     * @return {object} Request object.
     * @deprecated Used api._Request.
     */
    api.Request = function ($http, params, dateFields, serviceUrl) {
        if (!api.requestManager) {
            api.requestManager = { id: 0 };
        }

        if (!serviceUrl) {
            serviceUrl = triptop.options.paths.services.main;
        }

        return {
            urlStart: 0,
            httpService: $http,
            params: function () {
                return api.buildUrl(this.urlStart + '.jsonp',
                    angular.extend({
                        appSiteKey: triptop.options.key,
                        callback: 'JSON_CALLBACK'
                    }, params));
            },
            url: function () {
                return serviceUrl + api.buildUrl(this.urlStart,
                    angular.extend({
                        appSiteKey: triptop.options.key
                    }, params));
            },
            send: function (success, error, noCheckError) {
                var url = serviceUrl + this.params();
                this.httpService.jsonp(url).success(function (json) {
                    if (!noCheckError && json && json.error) {
                        if (error) {
                            error(json.error);
                        }
                    }
                    else if (success) {
                        if (dateFields) {
                            success(api.parseDates(json, dateFields));
                        }
                        else {
                            success(json);
                        }
                    }
                }).error(function (json) {
                    if (error) {
                        if (!json) {
                            error(json);
                        }
                        else {
                            error(404);
                        }
                    }
                });
            }
        };
    };

    /**
     * Creates request to server side.
     * @function _Request
     * @memberof triptop.api
     * @param {{string}} path The functinal path.
     * @param {object} params Mixin with parameters request.
     * @return {{triptop.api.Request}} Request object.
     */
    api._Request = function (path, params, serviceUrl) {
        var request = api.Request(null, params, null, serviceUrl);
        request.urlStart = path;
        angular.injector(['ng']).invoke(function($http) {
            request.httpService = $http;
        });
        request.polling = function (condition, success, error) {
            if(!condition) {
                return;
            }

            var _this = this;
            var poll = function () {
                if(_this.counter === undefined) {
                    _this.counter = 0;
                }
                if(++_this.counter > 120) {
                    if(error) { error(); }
                    return;
                }

                _this.send(function (data) {
                    if(condition(data)) {
                        if(success) { success(data); }
                    }
                    else {
                        setTimeout(poll, 1000);
                    }
                }, error, true);
            };
            poll();
        };
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    api.GetCaptchaRequest = function ($http, width, height) {
        var params = {
            width: width,
            height: height,
            action: 'new'
        };
        var request = api.Request($http, params, []);
        request.urlStart = '/captcha';
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    api.SiteInfoRequest = function ($http) {
        var params = {
            affiliateKey: triptop.options.key
        };
        var request = api.Request($http, params);
        request.urlStart = '/affiliates/sites/' + triptop.options.key + '/info';
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    api.formatDate = function (dt) {
        var day = dt.getDate();
        var month = dt.getMonth() + 1;
        var year = dt.getYear();
        return day + '.' + month + '.' + (1900 + year);
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    cabinet.RequestWithLoginRetry = function ($http, $scope, request, success, error) {
        request($http, $scope).send(success, function (err) {
            api.cabinet.LoginRequest($http, $scope.user).send(function (resp) {
                $scope.user.sid = resp.id;
                $scope.user.isLogged = true;
                request($http, $scope).send(success, error);
            }, error);
        });
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    cabinet.MergePaxRequest = function ($http, user, pax) {
        var paxObj = {
            sex: pax.sex,
            surname: pax.surname,
            name: pax.name,
            birthday: pax.birthday,
            country: pax.country,
            email: pax.email,
            phone: pax.phone,
            document: pax.document,
            documentdate: pax.documentdate
        };

        var params = {
            paxStr: JSON.stringify(paxObj, api.dateReplacer),
            email: user.email,
            passwordMd5: user.md5
        };

        var request = api.Request($http, params);
        request.urlStart = '/sessions/' + user.sid + '/mergePax';
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    cabinet.AirlinesRequest = function ($http) {
        var request = api.Request($http, {});
        request.urlStart = '/airlines/list';
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    cabinet.MergePayerRequest = function ($http, user, pax) {
        var paxObj = {
            sex: pax.sex,
            surname: pax.surname,
            name: pax.name,
            birthday: pax.birthday,
            country: pax.country,
            email: pax.email,
            phone: pax.phone,
            document: pax.document,
            documentdate: pax.documentdate
        };

        var params = {
            payerStr: JSON.stringify(paxObj, api.dateReplacer),
            email: user.email,
            passwordMd5: user.md5
        };

        var request = api.Request($http, params);
        request.urlStart = '/sessions/' + user.sid + '/mergePayer';
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    cabinet.LoginRequest = function ($http, user) {
        var request = api.Request($http, {});
        request.urlStart = '/login/' + user.email + '/' + user.md5;
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    cabinet.ForgotPasswordRequest = function ($http, user) {
        var request = api.Request($http, {});
        request.urlStart = '/forgotPassword/' + user.email + '/' + user.md5;
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    cabinet.GetProfileRequest = function ($http, user) {
        var params = {
            email: user.email,
            passwordMd5: user.md5
        };
        var request = api.Request($http, params, ['birthday', 'date']);
        request.urlStart = '/sessions/' + user.sid + '/getProfile';
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    cabinet.EditProfileRequest = function ($http, user, fullProfile) {
        var profile = {
            name: fullProfile.name,
            surname: fullProfile.surname,
            birthday: fullProfile.birthday,
            phone: fullProfile.phone,
            country: fullProfile.country,
            sex: fullProfile.sex,
            passports: fullProfile.passports,
            bonusCards: fullProfile.bonusCards
        };
        var params = {
            email: user.email,
            passwordMd5: user.md5,
            profile: JSON.stringify(profile, api.dateReplacer)
        };
        var request = api.Request($http, params);
        request.urlStart = '/sessions/' + user.sid + '/editProfile';
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    cabinet.LogoutRequest = function () {
        var request = api.Request();
        request.urlStart = 72;
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    api.toISOString = function (d, s, ss) {
        if(s === undefined) { s = true; }

        var nn = function (n) { return n < 10 ? '0' + n : n; };
        if(ss) {
            return d.getFullYear() + '-' +
                nn(d.getMonth() + 1) + '-' +
                nn(d.getDate());
        }

        var ms = function (n) { return n < 10 ? '00' + n : n < 100 ? '0' + n : n; };

        s = (s === true) ? (':' + nn(d.getSeconds()) + '.' + ms(d.getMilliseconds()) + 'Z') : '';

        return d.getFullYear() + '-' +
            nn(d.getMonth() + 1) + '-' +
            nn(d.getDate()) + 'T' +
            nn(d.getHours()) + ':' +
            nn(d.getMinutes()) + s;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    cabinet.RegistrationRequest = function ($http, user) {
        var profile = {
            email: user.email,
            name: user.name,
            surname: user.surname,
            birthday: user.birthday,
            phone: user.phone
        };
        var params = {
            profileJson: JSON.stringify(profile, api.dateReplacer),
            password: user.password
        };
        var request = api.Request($http, params);
        request.urlStart = '/register';
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    cabinet.ChangeEmailRequest = function ($http, user, newemail) {
        var params = {
            email: user.email,
            passwordMd5: user.md5,
            newEmail: newemail
        };
        var request = api.Request($http, params);
        request.urlStart = '/sessions/' + user.sid + '/changeEmail';
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    cabinet.CancelDocketRequest = function ($http, user, docketId) {
        var params = {
            email: user.email,
            passwordMd5: user.md5,
            docketId: docketId
        };
        var request = api.Request($http, params);
        request.urlStart = '/sessions/' + user.sid + '/cancelDocket';
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    transfers.CountriesRequest = function ($http) {
        var params = {
            locale: triptop.options.locale.substr(0,2).toUpperCase()
        };
        var request = api.Request($http, params);
        request.urlStart = '/transfers/countries';
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    transfers.CitiesRequest = function ($http, countryId) {
        var params = {
            locale: triptop.options.locale.substr(0,2).toUpperCase()
        };
        var request = api.Request($http, params);
        request.urlStart = '/transfers/countries/' + countryId + '/cities';
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    transfers.LocationsRequest = function ($http, cityId) {
        var params = {
            locale: triptop.options.locale.substr(0,2).toUpperCase()
        };
        var request = api.Request($http, params);
        request.urlStart = '/transfers/cities/' + cityId + '/locations';
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    transfers.StartSearchRequest = function ($http, pickupLocationId, dropoffLocationId, departureTime, paxNumber, luggageNumber) {
        var params = {
            pickupLocationId: pickupLocationId,
            dropoffLocationId: dropoffLocationId,
            departureTime: departureTime,
            paxNumber: paxNumber,
            luggageNumber: luggageNumber,
            action: 'start'
        };

        var request = api.Request($http, params);
        request.urlStart = '/transfers/searches';
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    transfers.ResultRequest = function ($http, searchId) {
        var params = {
            locale: triptop.options.locale.substr(0,2).toUpperCase()
        };

        var request = api.Request($http, params);
        request.urlStart = '/transfers/searches/' + searchId + '/processed';
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    transfers.PollSearchRequest = function ($http, searchId) {
        var request = api.Request($http, {});
        request.urlStart = '/transfers/searches/' + searchId + '/raw';
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    transfers.AirlinesRequest = function ($http, productId) {
        var request = api.Request($http, {});
        request.urlStart = '/transfers/' + productId + '/airlines';
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    cart.TakeNewCartRequest = function ($http, user) {
        var params = {
            action: 'new',
            locale: triptop.options.locale.substr(0,2).toUpperCase()
        };
        if (user && user.id) {
            params.userId = user.id;
        }
        var request = api.Request($http, params);
        request.urlStart = '/carts';
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    cart.AddProductRequest = function ($http, cartId, productTypeStr, productId, productAmount, manualMarkupAmount) {
        var params = {
            action: 'add',
            productAmount: productAmount,
            manualMarkupAmount: manualMarkupAmount 
        };
        var request = api.Request($http, params);
        request.urlStart = '/carts/' + cartId + '/' + productTypeStr + '/' + productId;
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    cart.CheckAvailabilityRequest = function ($http, cartId) {
        var params = {
            action: 'check'
        };
        var request = api.Request($http, params);
        request.urlStart = '/carts/' + cartId + '/availability';
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    cart.AvailabilityPollRequest = function ($http, cartId) {
        var request = api.Request($http, {});
        request.urlStart = '/carts/' + cartId + '/availability';
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    cart.ConfirmationStatusRequest = function ($http, cartId) {
        var request = api.Request($http, {});
        request.urlStart = '/carts/' + cartId + '/order/confirmation';
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    cart.ConfirmationBodyRequest = function ($http, cartId, locale) {
        var params = {
            locale: triptop.options.locale.substr(0,2).toUpperCase()
        };
        var request = api.Request($http, params);
        request.urlStart = '/carts/' + cartId + '/order/confirmation/body';
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    cart.ReservedProducts = function ($http, cartId, docketId) {
        var request = api.Request($http, {});
        request.urlStart = '/carts/' + cartId + '/order/reservation/' + docketId + '/products';
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    cart.EditTransferOrderDetails = function ($http, cartId, productId, paxNumber, luggageNumber, pickup, dropoff, comments, remarks) {
        var details = {
            luggageNumber: luggageNumber,
            paxNumber: paxNumber,
            pickup: pickup,
            dropoff: dropoff,
            comments: comments,
            remarks: remarks
        };
        var params = {
            action: 'edit',
            details: api.toJson(details)
        };
        var request = api.Request($http, params);
        request.urlStart = '/carts/' + cartId + '/order/Transfer/' + productId + '/details';
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    cart.EditOrderDetailsRequest = function ($http, cartId, person) {
        var params = {
            action: 'edit',
            details: api.toJson({
                payer: {
                    type: 'Adult',
                    title: person.sex == 'male' ? 'Mister' : 'Miss',
                    firstName: person.name,
                    lastName: person.surname,
                    middleName: '',
                    gender: person.sex == 'male' ? 'Male' : 'Female',
                    email: person.email,
                    phone: person.phone
                }
            })
        };
        var request = api.Request($http, params);
        request.urlStart = '/carts/' + cartId + '/order/details';
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    cart.GetDocumentsRequest = function ($http, cartId) {
        var request = api.Request($http, {});
        request.urlStart = '/carts/' + cartId + '/documents';
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    cart.GetDocketDocumentsRequest = function ($http, docketId) {
        var request = api.Request($http, {});
        request.urlStart = '/docket/' + docketId + '/documents';
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    cart.GetCartProducts = function ($http, cartId) {
        var request = api.Request($http, {});
        request.urlStart = '/carts/' + cartId;
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    cart.GetProductPNR = function ($http, cartId, productId) {
        var request = api.Request($http, {});
        request.urlStart = '/carts/' + cartId + '/order/reservation/' + productId + '/pnr';
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    cart.GetDocumentRequest = function ($http, documentId) {
        var request = api.Request($http, {});
        request.urlStart = '/documents/' + documentId;
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    cart.UnpaidReservationRequest = function ($http, cartId, willPayNow, captchaId, captchaAnswer) {
        var params = {
            action: 'make',
            willPayNow: willPayNow,
            captchaId: captchaId,
            captchaAnswer: captchaAnswer
        };
        var request = api.Request($http, params);
        request.urlStart = '/carts/' + cartId + '/order/reservation';
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    cart.UnpaidStatusRequest = function ($http, cartId) {
        var request = api.Request($http, {});
        request.urlStart = '/carts/' + cartId + '/order/reservation';
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    cabinet.GetDocketsRequest = function ($http, user) {
        var params = {
            email: user.email,
            passwordMd5: user.md5,
            action: 'get',
            locale: triptop.options.locale
        };
        var request = api.Request($http, params);
        request.urlStart = '/sessions/' + user.sid + '/dockets';
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    cabinet.GetPassengersRequest = function ($http, user) {
        var params = {
            email: user.email,
            passwordMd5: user.md5,
            action: 'list'
        };
        var request = api.Request($http, params, ['date', 'birthday']);
        request.urlStart = '/sessions/' + user.sid + '/passengers';
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    cabinet.SetPasswordRequest = function ($http, user, newPass) {
        var params = {
            email: user.email,
            passwordMd5: user.md5
        };
        var request = api.Request($http, params);
        request.urlStart = '/sessions/' + user.sid + '/changePassword/' + newPass;
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    cabinet.EditPassengerRequest = function ($http, user, passenger) {
        var params = {
            email: user.email,
            passwordMd5: user.md5,
            passenger: JSON.stringify(passenger, api.dateReplacer)
        };
        var request = api.Request($http, params);
        request.urlStart = '/sessions/' + user.sid + '/passengers/edit';
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    cabinet.DeletePassengerRequest = function ($http, user, id) {
        var params = {
            email: user.email,
            passwordMd5: user.md5,
            id: id
        };
        var request = api.Request($http, params);
        request.urlStart = '/sessions/' + user.sid + '/passengers/delete';
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    cabinet.AddPassengerRequest = function ($http, user, passenger) {
        var params = {
            email: user.email,
            passwordMd5: user.md5,
            passenger: JSON.stringify(passenger, api.dateReplacer)
        };
        var request = api.Request($http, params);
        request.urlStart = '/sessions/' + user.sid + '/passengers/add';
        return request;
    };

    // @deprecated
    // @todo This method should be moved to the appropriate module.
    // @todo Add card type parameter
    paymentWizard.openEmbedded = function (docketId, productIds, cardOwner, closeCallback) {
        angular.element(document).injector().invoke(function($compile, $rootScope) {
            var scope = $rootScope.$new();
            scope.payDocketId = docketId;
            scope.cardOwner = cardOwner;
            scope.productIds = productIds;
            
            paymentWizard.closeCallback = closeCallback;

            $('body').append('<div id="paymentDivId"></div>');
            var elem = $('#paymentDivId').html('<payment></payment>');

            $compile(elem.contents()[0])(scope);
        });
    };

    // @deprecated
    // @todo This method should be moved to the appropriate module.
    // @todo Merge with openEmbedded?
    paymentWizard.open = function (parentNode, $scope, $compile, $, closeCallback) {
        paymentWizard.closeCallback = closeCallback;
        parentNode.append('<div id="paymentDivId"></div>');
        var elem = $('#paymentDivId').html('<payment></payment>');
        $compile(elem.contents())($scope);
    };

    // @deprecated
    // @todo This method should be moved to the appropriate module.
    paymentWizard.close = function ($) {
        $('#paymentDivId').remove();
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    paymentWizard.CurrencyRequest = function ($http, docketId, productIds, locale) {
        var params = {
            docketId: docketId,
            'productId[]': productIds,
            locale: locale
        };
        var request = api.Request($http, params);
        request.urlStart = '/pay/prices';
        return request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    paymentWizard.ProductsRequest = function ($http, docketId, productIds, locale) {
        var params = {
            docketId: docketId,
            'productId[]': productIds,
            locale: locale,
            affiliateKey: triptop.options.key
        };
        var request = api.Request($http, params, ['date']);
        request.urlStart = '/pay/products';
        return  request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    paymentWizard.AssignDocketRequest = function ($http, docketId, userId) {
        var params = {
            docketId: docketId,
            userId: userId
        };
        var request = api.Request($http, params);
        request.urlStart = '/pay/assignDocket';
        return  request;
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    paymentWizard.QueryBookingRequest = function ($http, sessionid, vsid, success, error) {
        var params = {
            vsid: vsid,
            action: 38,
            callback: 'JSON_CALLBACK',
            uid: new Date().getTime(),
            jsessionid: sessionid
        };
        var url = api.buildUrl(triptop.options.paths.services.mainapp, params);
        return paymentWizard.PaymentRequest($http, url, success, error);
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    paymentWizard.PnrPollRequest = function ($http, sessionid, vsid, searchId, success, error) {
        var params = {
            vsid: vsid,
            action: 3,
            callback: 'JSON_CALLBACK',
            searchId: searchId,
            uid: new Date().getTime(),
            jsessionid: sessionid
        };
        var url = api.buildUrl(triptop.options.paths.services.mainapp, params);
        return paymentWizard.PaymentRequest($http, url, success, error);
    };

    // @deprecated This method should be used triptop.api._Request
    // @todo This method should be moved to the appropriate module.
    paymentWizard.PaymentRequest = function ($http, url, success, error) {
        return {
            send: function () {
                $http.jsonp(url)
                    .success(function (json) {
                        if (json.error) {
                            if (error) {
                                error(json.error);
                            }
                        }
                        else if (json.status === 3) {
                            if (error) {
                                error();
                            }
                        }
                        else if (success) {
                            success(json);
                        }
                    }).error(function (json) {
                        if (error) {
                            error(json ? json : 404);
                        }
                    });
            }
        };
    };

    /**
     * Provides methods for loading functional modules.
     * @namespace triptop.api.loader
     */    
    api.loader = {
        /**
         * Loading specific functional module.
         * @function get
         * @memberof triptop.api.loader
         * @param {string} module Name of the module.
         * @param {object} options The options of the module.
         * @param {function} success Success callback function.
         */
        get: function(module, options, success) {
            if(triptop[module]) {
                if(success) { success(); }
                return;
            }

            if(!options) { options = {}; }

            if(!options.basedir) {
                options.basedir = triptop.options.paths.basedir;
            }

            if(!options.parent) {
                options.parent = angular.element('body');
            }

            if(!triptop.options[module]) {
                triptop.options[module] = {};
            }
            
            api.loader[module](options, success);
        },
        /**
         * Loading functional module for regular flights.
         * @function flights
         * @memberof triptop.api.loader
         * @param {object} options The options of the module.
         * @param {function} success Success callback function.
         */
        flights: function(options, success) {
            var path = options.basedir;

            $.extend(true, triptop.options, {
                paths: {
                    css: {
                        flights: path + 'flights/less/'
                    },
                    js: {
                        flights: path + 'flights/modules/'
                    },
                    templates: {
                        common: path + 'common/templates/',
                        flights: path + 'flights/templates/'
                    }
                },
                flights: {
                    styling: options.styling ? options.styling : '',
                    ready: function() {
                        if(success) { success(); }
                    }
                }
            });

            angular.element(document).injector().invoke(function($compile, $rootScope, resourceLoader) {
                resourceLoader.module('flights', 'flights', function() {
                    options.parent.append($compile('<flights></flights>')($rootScope.$new()));
                });
            });
        },
        /**
         * Loading functional module for regular flights.
         * @function flightsTest
         * @memberof triptop.api.loader
         * @param {object} options The options of the module.
         * @param {function} success Success callback function.
         */
        flightsTest: function(options, success) {
            var path = options.basedir;

            $.extend(true, triptop.options, {
                paths: {
                    css: {
                        flights: path + 'flights-test/less/'
                    },
                    js: {
                        flights: path + 'flights-test/modules/'
                    },
                    templates: {
                        common: path + 'common/templates/',
                        flights: path + 'flights-test/templates/'
                    }
                },
                flights: {
                    styling: options.styling ? options.styling : '',
                    ready: function() {
                        if(success) { success(); }
                    }
                }
            });

            angular.element(document).injector().invoke(function($compile, $rootScope, resourceLoader) {
                resourceLoader.module('flights', 'flights', function() {
                    options.parent.append($compile('<flights></flights>')($rootScope.$new()));
                });
            });
        },
        /**
         * Loading functional module for hotels.
         * @function hotels
         * @memberof triptop.api.loader
         * @param {object} options The options of the module.
         * @param {function} success Success callback function.
         */
        hotels: function(options, success) {
            var path = options.basedir;

            $.extend(true, triptop.options, {
                paths: {
                    css: {
                        hotels: path + 'hotels/less/'
                    },
                    js: {
                        hotels: path + 'hotels/modules/'
                    },
                    templates: {
                        common: path + 'common/templates/',
                        hotels: path + 'hotels/templates/'
                    }
                },
                hotels: {
                    styling: options.styling ? options.styling : '',
                    ready: function() {
                        if(success) { success(); }
                    }
                }
            });

            angular.element(document).injector().invoke(function($compile, $rootScope, resourceLoader) {
                resourceLoader.module('hotels', 'hotels', function() {
                    options.parent.append($compile('<hotels></hotels>')($rootScope.$new()));
                });
            });
        },
        /**
         * Loading functional module for agent terminal.
         * @function terminal
         * @memberof triptop.api.loader
         * @param {object} options The options of the module.
         * @param {function} success Success callback function.
         */
        terminal: function(options, success) {
            var path = options.basedir;

            $.extend(true, triptop.options, {
                paths: {
                    css: {
                        terminal: path + 'terminal/less/'
                    },
                    js: {
                        terminal: path + 'terminal/modules/'
                    },
                    templates: {
                        terminal: path + 'terminal/templates/'
                    }
                },
                terminal: {
                    styling: options.styling ? options.styling : '',
                    ready: function() {
                        if(success) { success(); }
                    }
                }
            });

            angular.element(document).injector().invoke(function($compile, $rootScope, resourceLoader) {
                resourceLoader.module('terminal', 'terminal', function() {
                    options.parent.append($compile('<terminal></terminal>')($rootScope.$new()));
                });
            });
        },
        /**
         * Loading functional module for hotel results filter.
         * @function filters
         * @memberof triptop.api.loader
         * @param {object} options The options of the module.
         * @param {function} success Success callback function.
         */
        filters: function(options, success) {
            var path = options.basedir;

            $.extend(true, triptop.options, {
                paths: {
                    css: {
                        filters: path + 'filters/less/'
                    },
                    js: {
                        filters: path + 'filters/modules/'
                    },
                    templates: {
                        filters: path + 'filters/templates/'
                    }
                },
                filters: {
                    styling: options.styling ? options.styling : '',
                    ready: function() {
                        if(success) { success(); }
                    }
                }
            });

            angular.element(document).injector().invoke(function($compile, $rootScope, resourceLoader) {
                resourceLoader.module('filters', 'filter', function() {
                    options.parent.append($compile('<filter type="hotelsfilter"></filter>')($rootScope.$new()));
                });
            });
        },
        /**
         * Loading functional module for booking process.
         * @function booking
         * @memberof triptop.api.loader
         * @param {object} options The options of the module.
         * @param {function} success Success callback function.
         */
        booking: function(options, success) {
            var path = options.basedir;

            $.extend(true, triptop.options, {
                paths: {
                    css: {
                        booking: path + 'booking/less/'
                    },
                    js: {
                        booking: path + 'booking/modules/'
                    },
                    templates: {
                        booking: path + 'booking/templates/'
                    },
                    services: {
                        main: '//il.trip-top.com/triptop-fo-api-war/api',
                        mainapp: '//il.trip-top.com/main-server-app/dispatch'
                    }
                },
                booking: {
                    ready: function() {
                        if(success) { success(); }
                    }
                }
            });

            angular.element(document).injector().invoke(function($compile, $rootScope, resourceLoader) {
                resourceLoader.module('booking', 'booking', function() {
                    options.parent.append($compile('<booking></booking>')($rootScope.$new()));
                });
            });
        },
        /**
         * Loading functional module for cabinet.
         * @function profile
         * @memberof triptop.api.loader
         * @param {object} options The options of the module.
         * @param {function} success Success callback function.
         */
        profile: function(options, success) {
            var path = options.basedir;

            $.extend(true, triptop.options, {
                paths: {
                    css: {
                        profile: path + 'profile/less/'
                    },
                    js: {
                        profile: path + 'profile/modules/'
                    },
                    templates: {
                        profile: path + 'profile/templates/'
                    },
                    services: {
                        main: '//il.trip-top.com/triptop-fo-api-war/api',
                        mainapp: '//il.trip-top.com/main-server-app/dispatch'
                    }
                },
                profile: {
                    ready: function() {
                        if(success) { success(); }
                    }
                }
            });

            angular.element(document).injector().invoke(function($compile, $rootScope, resourceLoader) {
                resourceLoader.module('profile', 'cabinet', function() {
                    options.parent.append($compile('<cabinet></cabinet>')($rootScope.$new()));
                });
            });
        },
        /**
         * Loading functional module for application terms.
         * @function terms
         * @memberof triptop.api.loader
         * @param {object} options The options of the module.
         * @param {function} success Success callback function.
         */
        terms: function(options, success) {
            var path = options.basedir;

            $.extend(true, triptop.options, {
                paths: {
                    css: {
                        common: path + 'common/less/'
                    },
                    js: {
                        common: path + 'common/modules/'
                    },
                    templates: {
                        common: path + 'common/templates/' + (triptop.options.devmode ? 'en/' : '')
                    },
                    services: {
                        main: '//il.trip-top.com/triptop-fo-api-war/api',
                        mainapp: '//il.trip-top.com/main-server-app/dispatch'
                    }
                }
            });

            angular.element(document).injector().invoke(function(resourceLoader) {
                resourceLoader.module('common', 'terms', success, undefined, 'terms');
            });
        },
        /**
         * Loading functional module for payment wizard.
         * @function payment
         * @memberof triptop.api.loader
         * @param {object} options The options of the module.
         * @param {function} success Success callback function.
         */
        payment: function(options, success) {
            var path = options.basedir;

            $.extend(true, triptop.options, {
                paths: {
                    css: {
                        payment: path + 'payment/less/'
                    },
                    js: {
                        payment: path + 'payment/modules/'
                    },
                    templates: {
                        payment: path + 'payment/templates/'
                    },
                    services: {
                        main: '//il.trip-top.com/triptop-fo-api-war/api',
                        mainapp: '//il.trip-top.com/main-server-app/dispatch'
                    }
                },
                payment: {
                    ready: function() {
                        if(success) { success(); }
                    }
                }
            });

            angular.element(document).injector().invoke(function($compile, $rootScope, resourceLoader) {
                resourceLoader.module('payment', 'payment', function() {
                    options.parent.append($compile('<payment></payment>')($rootScope.$new()));
                });
            });
        },
        /**
         * Loading functional module for transfers.
         * @function transfers
         * @memberof triptop.api.loader
         * @param {object} options The options of the module.
         * @param {function} success Success callback function.
         */
        transfers: function(options, success) {
            var path = options.basedir;

            $.extend(true, triptop.options, {
                paths: {
                    css: {
                        transfers: path + 'transfers/less/'
                    },
                    js: {
                        transfers: path + 'transfers/modules/'
                    },
                    templates: {
                        transfers: path + 'transfers/templates/'
                    },
                    services: {
                        main: '//il.trip-top.com/triptop-fo-api-war/api',
                        mainapp: '//il.trip-top.com/main-server-app/dispatch'
                    }
                },
                transfers: {
                    ready: function() {
                        if(success) { success(); }
                    }
                }
            });

            angular.element(document).injector().invoke(function($compile, $rootScope, resourceLoader) {
                resourceLoader.module('transfers', 'transfers', function() {
                    options.parent.append($compile('<transfers></transfers>')($rootScope.$new()));
                });
            });
        },
        packages: function(options, success) {
            var path = options.basedir;

            $.extend(true, triptop.options, {
                paths: {
                    css: {
                        packages: path + 'packages/less/'
                    },
                    js: {
                        packages: path + 'packages/modules/'
                    },
                    templates: {
                        common: path + 'common/templates/',
                        packages: path + 'packages/templates/'
                    }
                },
                flights: {
                    styling: options.styling ? options.styling : '',
                    ready: function() {
                        if(success) { success(); }
                    }
                }
            });

            angular.element(document).injector().invoke(function($compile, $rootScope, resourceLoader) {
                resourceLoader.module('packages', 'packages', function() {
                    options.parent.append($compile('<packages></packages>')($rootScope.$new()));
                });
            });
        }
    };
}(window.triptop, window.jQuery));