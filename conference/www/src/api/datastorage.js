'use strict';

(function (triptop) {

	var com = com || {};
	com.triptop = com.triptop || {};
	com.triptop.widget = com.triptop.widget || {};
	com.triptop.widget.data = com.triptop.widget.data || {};
	com.triptop.widget.data.received = com.triptop.widget.data.received || {};

	if (!Array.prototype.indexOf) {
		Array.prototype.indexOf = function(obj, start) {
			for (var i = (start || 0), j = this.length; i < j; i++) {
				if (this[i] === obj) { return i; }
			}
			return -1;
		};
	}

	triptop.DataStorage = function DataStorage() {
		this.countries = new triptop.CountriesStorage();
		this.cities = new triptop.CitiesStorage();
		this.airports = new triptop.AirportsStorage();
		this.currencies = new triptop.CurrenciesStorage();

		this.getCountries = function() { return this.countries; };
		this.getCities = function() { return this.cities; };
		this.getAirports = function() { return this.airports; };
		this.getCurrencies = function() { return this.currencies; };
	};

	triptop.CountriesStorage = function CountriesStorage() {
		this.byId = [];
		this.byCode = [];
		this.byName = [];
		this.allTight = [];

		this.add = function(country) {
			if (!country) { return null; }
			if (this.byId[country.id]) { return country; }

			this.byId[country.id] = country;
			this.byCode[country.code] = country;
			this.byName[country.name] = country;
			this.allTight[this.allTight.length] = country;

			return country;
		};

		this.findById = function(id) { return this.byId[id]; };

		this.findByCode = function(code) { return this.byCode[code]; };

		this.findByName = function(name) { return this.byName[name]; };

		this.getAll = function() { return this.allTight; };
	};

	triptop.CurrenciesStorage = function CurrenciesStorage() {
		this.byId = {};
		this.byCode = {};
		this.allTight = [];

		this.add = function(currency) {
			if (!currency) { return null; }
			if (this.byId[currency.id]) { return currency; }

			this.byId[currency.id] = currency;
			this.byCode[currency.code] = currency;
			this.allTight[this.allTight.length] = currency;

			return currency;
		};

		this.findById = function(id) { return this.byId[id]; };

		this.findByCode = function(code) { return this.byCode[code]; };

		this.getAll = function() { return this.allTight; };
	};

	triptop.CitiesStorage = function CitiesStorage() {
		this.byId = [];
		this.byCountryId = [];
		this.allTight = [];
		this.byGroup = [];

		this.add = function(city, service) {
			if (!city) { return null; }
			if (this.byId[city.id]) {
				city = this.byId[city.id];
			}
			else {
				this.byId[city.id] = city;
				this.allTight[this.allTight.length] = city;
			}

			try {
				if (!this.byCountryId[city.country.id]) {
					this.byCountryId[city.country.id] = [];
				}
				var arr = this.byCountryId[city.country.id];
				if(arr.indexOf(city) == -1){
					arr[arr.length] = city;
				}

				if (service) {
					if (!this.byCountryId[service + "_" + city.country.id]) {
						this.byCountryId[service + "_" + city.country.id] = [];
					}
					arr = this.byCountryId[service + "_" + city.country.id];
					if(arr.indexOf(city) == -1){
						arr[arr.length] = city;
					}
				}
			}
			catch (e) {
				return null;
			}
			return city;
		};

		this.addToGroup = function(name, city) {
			if (!city) { return null; }
			if (this.byId[city.id]) {
				city = this.byId[city.id];
			}
			else {
				this.byId[city.id] = city;
				this.allTight[this.allTight.length] = city;
			}

			if (!this.byGroup[name]) {
				this.byGroup[name] = [];
			}
			var arr = this.byGroup[name];
			if (arr.indexOf(city) == -1) {
				arr[arr.length] = city;
			}
			return city;
		};

		this.findById = function(id) { return this.byId[id]; };

		this.getByCountryId = function(id) { return this.byCountryId[id]; };

		this.getAll = function() { return this.allTight; };

		this.getGroup = function(name) { return this.byGroup[name]; };

		this.setGroup = function(name, data) { this.byGroup[name] = data; };
	};

	triptop.AirportsStorage = function AirportsStorage() {
		this.all = [];
		this.byId = [];
		this.byCode = [];
		this.byCityId = [];
		this.byGroup = [];

		this.add = function(airport) {
			if (!airport) { return null; }
			if (this.byId[airport.id]) {
				airport = this.byId[airport.id];
			}
			else {
				if (!airport.city || !airport.city.id) { return null; }
				this.all[this.all.length] = airport;
				this.byId[airport.id] = airport;
				this.byCode[airport.code] = airport;
			}

			if (!this.byCityId[airport.city.id]) {
				this.byCityId[airport.city.id] = [];
			}
			var arr = this.byCityId[airport.city.id];
			if (arr.indexOf(airport) == -1) {
				arr[arr.length] = airport;
			}

			return airport;
		};

		this.addToGroup = function(name, airport) {
			if (!airport) { return null; }
			if (this.byId[airport.id]) {
				airport = this.byId[airport.id];
			}
			else {
				if (!airport.city || !airport.city.id) { return null; }
				this.all[this.all.length] = airport;
				this.byId[airport.id] = airport;
				this.byCode[airport.code] = airport;
			}

			if (!this.byGroup[name]) {
				this.byGroup[name] = [];
			}
			var arr = this.byGroup[name];
			if (arr.indexOf(airport) == -1) {
				arr[arr.length] = airport;
			}

			return airport;
		};

		this.findById = function(id) { return this.byId[id]; };

		this.findByCode = function(code) { return this.byCode[code]; };

		this.getByCityId = function(id) { return this.byCityId[id];	};

		this.getAll = function() { return this.all; };

		this.getGroup = function(name) { return this.byGroup[name]; };

		this.setGroup = function(name, data) { this.byGroup[name] = data; };
	};

	triptop.PreferredAirportsResponse = function PreferredAirportsResponse(status, defaultsAirports, preferredAirports, availableDirections, err) {
		this.status = status;
		this.defaultsAirports = defaultsAirports;
		this.preferredAirports = preferredAirports;
		this.availableDirections = availableDirections;
		this.err = err||null;
	};

	triptop.PreferredLocationsResponse = function PreferredLocationsResponse(status, defaultsLocations, preferredLocations, err) {
		this.status = status;
		this.defaultsLocations = defaultsLocations;
		this.preferredLocations = preferredLocations;
		this.err = err||null;
	};

	triptop.PreferredAirportsResponse = function PreferredAirportsResponse(status, defaultsAirports, preferredAirports, availableDirections, err) {
		this.status = status;
		this.defaultsAirports = defaultsAirports;
		this.preferredAirports = preferredAirports;
		this.availableDirections = availableDirections;
		this.err = err||null;
	};

	triptop.Currency = function Currency(id, title, code, symbol, prefix) {
		this.id = id;
		this.title = title;
		this.code = code;
		this.symbol = symbol;
		this.prefix = prefix||false;
	};

	triptop.Country = function Country(id, name, code) {
		this.id = id;
		this.code = code;
		this.name = name;
	};

	triptop.City = function City(id, country, name, terms) {
		this.id = id;
		this.country = country;
		this.name = name;
		this.terms = terms;
	};

	triptop.Airport = function Airport(id, city, code, name, terms) {
		this.id = id;
		this.city = city;
		this.code = code;
		this.name = name;
		this.label = name;
		this.terms = terms;
	};

	triptop.dataStorage = new triptop.DataStorage();

}(window.triptop));

var triptopAirport = { 
"IEV":4715, //Kiev
"ROM":2719, //Rome
"YTO":1184, //Toronto
"BJS":3762, //Beijing
"PAR":1971, //Paris
"BUH":4044, //Bucharest
"TYO":2801, //Tokyo
"BUE":100, //Buenos Aires
"MOW":4124, //Moscow
"NYC":11248, //New York (Ny)
"BER":2071, //Berlin
"AZP":3083, //Mexico City
"QRA":4364, //Johannesburg
"TBS":2064, //Tbilisi
"PRG":1554, //Prague
"AYT":4636, //Antalya
"MAD":4406, //Madrid
"LIM":3917, //Lima
"LON":1745, //London
"DEL":2388, //Delhi
"MHP":602, //Minsk
"BRU":606, //Brussels
"ETH":2657, //Eilat
"VNO":2918, //Vilnius
"ATH":2169, //Athens
"CAI":1680, //Cairo
"WAW":4008, //Warsaw
"EVN":174, //Yerevan
"TLL":1783, //Tallinn
"ALA":2808, //Almaty
"RHO":2205, //Rhodes
"RIX":2884, //Riga
"VIE":552, //Vienna
"TLV":2667, //Tel Aviv
"BCN":4389,  // Barcelona Airport
"AMS":3299,  // Schiphol
"BUD":2328,  // Budapest Ferihegy International
"LCA":1546,  // Larnaca
"WAS":12588, // Washington
"LAX":10768, // Los Angeles
"SFO":11912, // San Francisco
"MCO":11376, // Orlando Intl.
"YVR":1202,  // Vancouver	
"YMQ":1098,  // Montreal
"BOM":2376,  // Chatrapati Shivaji	
"KTM":3290,  // Tribhuvan Intl	
"BKK":4577,  // Suvarnabhumi  (New Bangkok International)
"HKG":2327,  // Hong Kong	
"SIN":4262,  // Singapore
"BOG":1389,  // El Dorado International
"SCL":1359,  // Arturo Merino Benitez Intl.	
"GIG":775,   // Rio De Janeiro	
"GUA":2260,  // La Aurora Intl
"MEX":3121,  // New Mexico	
"SJO":1488,  // Juan Santamar
"HAV":1535,// Havana
"SYD":534,//Sydney	
"MEL":508,//Melbourne	
"HBA":398,//Hobart	
"WLG":3368,//Wellington
"EDI":4208,//Edinburgh
"DUB":2639,//Dublin
"MCM":3163,//Monte Carlo
"MUC":2129,//Munich
"FRA":2098,//Frankfurt
"SHA":3867,//Shanghai
"HRB":3808,//Harbin
"OSA":2796,//РћСЃР°РєР°
"HKT":4585,//Phuket
"SGN":6965,//Ho Chi Minh
"HAN":6958,//Hanoi
"RIO":865,//Rio
"LPG":143,//La Plata
"MIA":10982,//Miami
"BOS":12268,//Boston
"CHI":9214,//Chicago
"SAO":866,//San Paulo
"SSA":873,//Salvador
"COR":106,//CГіrdoba
"PUQ":1358,//Punta Arenas
"PER":519,//Perth
"AKL":3328,//Auckland
"CHC":3333,//Christchurch
"LED":4119,//Piter
"BAK":553,//BAKU
"REK":2354,
"TIA":26,//TIRANA
"ALV":63,//ANDORRA
"SOF":900,//SOFIA
"SJJ":659,//SARAEVO
"CPH":1625,//COPENHAGEN
"LUX":2919,//LUXEMBURG
"QVU":7011,//LICTENSHT.
"KIV":3162,//CHISINAU
"ZBK":3184,//MONTENEGRO
"OHD":2921,//OHRID
"OSL":3440,
"LJU":4271,//Ljubljana
"BTS":4264,//BRATISLAVA
"SAI":4038,//SAN MARINO
"BEG":4240,//BELGRAD
"LIS":4021,//LISBON
"HEL":1848,
"ZAG":1525,//ZAGREB
"STO":4514,//STOKHOLM
"GVA":4526,//GENEVA
"NAN":1845,//NADI
"POM":3737,//Port Moresby
"HIR":4289,//Honiara
"VLI":6907,//Port Vila
"TBU":4615,//NukuК»alofa
"APW":4169,//Apia
"NOU":3326,//NoumГ©a
"NAS":579,//Nassau
"SAL":1693,//San Salvador
"GCM":1296,//Georgetown
"AXA":89,//The Valley(Anguilla)
"BBQ":92,//Codrington
"AUA":176,//Oranjestad(Aruba)
"BZE":617,//Belize
"BGI":598,//Bridgetown
"BDA":633,//Hamilton
"KIN":2732,//Kingston
"PTP":2248,//Pointe-ГЂ-Pitre
"GND":2241,//St. George'S
"DOM":1642,//Melville Hall
"PAP":2305,//Port-Au-Prince
"TGU":2326,//Tegucigalpa
"MNI":889,//Gerald's
"FDF":3054,//Fort-de-France
"MGA":3374,//Managua
"SLU":4161,//Castries
"SJU":4034,//San Juan
"PTY":3523,//Panama City
"JQB":1649,//Santo Domingo
"POS":4616,//Port Of Spain
"SVD":4166,//Kingstown
"SKB":1830,//Basseterre
"PLS":4689,//Providenciales
"TAS":6889,//Tashkent
"DPS":2512,//Denpasar
"PBH":634,//Paro
"MLE":3004,//HulhulГ©(Maldives)
"TPE":4553,//Taipei
"DIL":1658,//Dili
"AMM":2805,//Amman
"LPQ":2877,//Luang Prabang
"ULN":3178,//Ulan Bator
"MDL":3249,//Mandalay
"KUL":2979,//Kuala Lumpur
"CMB":4433,//Colombo
"MNL":3967,//Metro Manila
"ISB":3485,//Islamabad
"SEL":4381,//Seoul
"REP":943,//Siem Reap
"GOH":2215,//Nuuk
"MVD":6882,//Montevideo
"PSY":1823,//Stanley
"UIO":1673,//Quito
"LPB":646,//La Paz
"GEO":2282,//Georgetown
"CAY":2014,//Cayenne
"CCS":6917,//Caracas
"PBM":4469,//Paramaribo
"ASU":3745,//AsunciГіn
"LAS":10650,//Las Vegas
"SAN":11906,//San Diego
"SZG":551,//Salzburg
"MIL":2702,//Milan
"BSL":4524,//Basel
"ZRH":4534,//ZГјrich
"BRN":4523,//Bern
"NCE":1966,//Nice
"MRS":1964,//Marseille
"LYN":1959,//Lyon
"RGN":3251,//Yangon
"PNH":941,//Phnom Penh
"ADD":1786,//Addis Abbaba
"JKT":2531,//Jakarta
"NBO":2854,//Nairobi
"VTE":2879,//Vientiane
"MLA":3019,//Malta
"TGD":3182,//Podgorica
"CAS":3187,//Casablanca
"MPM":3222,//Maputo
"ABV":3386,//Abuja
"SEZ":4250,//Victorya/Seychelles
"IST":4658,//Istanbul
"VCE":2729,//Venice
"FLR":3117,//Florence
"AUH":4733,//Abu Dabi
"GOI":2396//Goa
};

var triptopCity = { 
"TLV":3100, // Tel Aviv		
"NYC":34668, // New York
"TOR":1672, // Toronto
"MEX":3495, // Mexico
"LIM":4307, // Lima
"BUA":591, // Buenos Aires
"MOW":4482, // Moscow 
"KIE":5077, // Kiev
"MIN":1093, // Minsk
"PEK":4157, // Beiging
"DEL":2825, // Deli
"TOK":3184, // Tokio
"SID":8166, // Sidney
"JOH":4718, // Johannesburg
"KAI":2140, // Cairo
"EIL":3089, // Eilat
"PTR":11595, // Petra
"ROM":3141, // Rome
"ATH":2610, // Athens
"ROD":2637, // Rhodes
"ANT":5003, // Antalya
"MAD":4783, // Madrid
"BRU":1096, // Brussels
"BER":2513, // Berlin
"PAR":2420, // Paris
"LON":2181, // London
"PRG":2018, // Prague
"BUC":4437, // Bucharest
"WAW":4402, // Warshaw
"VIE":1044, // Vienna
"TAL":2235, // Tallinn
"RIG":3301, // Riga
"VIL":3333, // Vilnius
"TBL":2510, // Tbilisi
"YER":670, // Yerevan
"ALM":3227, // Almaty
"BCN":4770, // Barcelona
"AMS":3712, // Amsterdam
"BUD":2767, // Budapest
"LCA":2010, // Larnaca
"WAS":35886,// Washington
"LAX":34172,// Los Angeles
"SFO":35312,// San Francisco
"MCO":34794,// Orlando
"YVR":1493, // Vancouver	
"YMQ":1709, // Montreal	
"BOM":2862, // Mumbai
"KTM":3695, // Kathmandu	
"BKK":4947, // Bang Kok
"HKG":2766, // Hong Kong	
"SIN":9807, // Singapore
"BOG":1852, // Bogota
"SCL":1823, // Santiago de Chile	
"GIG":1171, // Rio De Janeiro	
"GUA":2691, // Guatemala
"SJO":1955, // San Jose	
"HAV":2000, // Havana	
"SYD":679,  // Sydney	
"MEL":973,  // Melbourne	
"HBA":954,  // Hobart	
"WLG":3772, // Wellington
"EDI":4595, //Edinburgh
"DUB":3071,//Dublin
"MCM":3574,//Monte Carlo
"MUC":2562,//Munich
"FRA":2523,//Frankfurt
"SHA":4174,//Shanghai
"HRB":4218,//Harbin
"OSA":3183,//Осака
"HKT":4967,//Phuket
"SGN":7079,//Ho Chi Minh
"HAN":7078,//Hanoi
"RIO":1171,//Rio
"LPG":613,//La Plata
"MIA":34388,//Miami
"BOS":32446,//Boston
"CHI":32714,//Chicago
"SAO":1178,//San Paulo
"SSA":1216,//Salvador
"COR":629,//Córdoba
"PUQ":1822,//Punta Arenas
"PER":1032,//Perth
"AKL":3754,//Auckland
"CHC":3739,//Christchurch
"LED":4498,//Piter
"BAK":1045,//BAKU
"REK":2792,//REIKYAVIK
"TIA":525,//TIRANA
"ALV":561,//ANDORRA
"SOF":1382,//SOFIA
"SJJ":1148,//SARAEVO
"CPH":2088,//COPENHAGEN
"LUX":3334,//LUXEMBURG
"QVU":9833,//LICTENSHT.
"KIV":3573,//CHISINAU
"ZBK":3595,//MONTENEGRO
"OHD":3336,//OHRID
"OSL":3838,
"LJU":4656,//Ljubljana
"BTS":4649,//BRATISLAVA
"SAI":4432,//SAN MARINO
"BEG":4628,//BELGRAD
"LIS":4411,//LISBON
"HEL":2302,
"ZAG":1988,//ZAGREB
"STO":4885,//STOKHOLM
"GVA":4899,//GENEVA
"NAN":2292,//NADI
"POM":4130,//Port Moresby
"HIR":4673,//Honiara
"VLI":7015,//Port Vila
"TBU":4984,//Nukuʻalofa
"APW":4560,//Apia
"NOU":3732,//Nouméa
"NAS":1056,//Nassau
"SAL":2153,//San Salvador
"GCM":1765,//Georgetown
"AXA":587,//The Valley(Anguilla)
"BBQ":590,//Codrington
"AUA":671,//Oranjestad(Aruba)
"BZE":1105,//Belize
"BGI":1089,//Bridgetown
"BDA":1122,//Hamilton
"KIN":3158,//Kingston
"PTP":2683,//Pointe-À-Pitre
"GND":2681,//St. George'S
"DOM":2106,//Melville Hall
"PAP":2744,//Port-Au-Prince
"TGU":2763,//Tegucigalpa
"MNI":1372,//Gerald's
"FDF":3466,//Fort-de-France
"MGA":3781,//Managua
"SLU":4549,//Castries
"SJU":4429,//San Juan
"PTY":3924,//Panama City
"JQB":2115,//Santo Domingo
"POS":4985,//Port Of Spain
"SVD":4553,//Kingstown
"SKB":2283,//Basseterre
"PLS":5050,//Providenciales
"TAS":7010,//Tashkent
"DPS":2980,//Denpasar
"PBH":1123,//Paro
"MLE":3413,//Hulhulé(Maldives)
"TPE":4924,//Taipei
"DIL":2119,//Dili
"AMM":3224,//Amman
"LPQ":3295,//Luang Prabang
"ULN":3583,//Ulan Bator
"MDL":3658,//Mandalay
"KUL":3379,//Kuala Lumpur
"CMB":4812,//Colombo
"MNL":4329,//Metro Manila
"ISB":3888,//Islamabad
"SEL":4764,//Seoul
"REP":1422,//Siem Reap
"GOH":2667,//Nuuk
"MVD":7002,//Montevideo
"PSY":2277,//Stanley
"UIO":2129,//Quito
"LPB":1134,//La Paz
"GEO":2724,//Georgetown
"CAY":2460,//Cayenne
"CCS":7037,//Caracas
"PBM":4846,//Paramaribo
"ASU":4148,//Asunción
"LAS":34078,//Las Vegas
"SAN":35302,//San Diego
"SZG":1042,//Salzburg
"MIL":3129,//Milan
"BSL":4896,//Basel
"ZRH":4906,//Zürich
"BRN":4897,//Bern
"NCE":2416,//Nice
"MRS":2406,//Marseille
"LYN":2405,//Lyon
"RGN":3662,//Yangon
"PNH":1420,//Phnom Penh
"ADD":2239,//Addis Abbaba
"JKT":2941,//Jakarta
"NBO":3272,//Nairobi
"VTE":3298,//Vientiane
"MLA":3431,//Malta
"TGD":3593,//Podgorica
"CAS":3598,//Casablanca
"MPM":3630,//Maputo
"ABV":3793,//Abuja
"SEZ":4636,//Victorya/Seychelles
"IST":5019,//Istanbul
"VCE":3149,//Venice
"FLR":2690,//Florence
"GOA":2833//Goa
};