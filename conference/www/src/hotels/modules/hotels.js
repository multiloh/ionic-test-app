'use strict';

/**
 * @file Implementation for regular hotels service.
 * @author amglin@trip-top.com
 * @module triptop/hotels
 */

(function (triptop) {
    if (!triptop || !triptop.options || !triptop.widgets) {
        throw new Error('Application not configured');
    }

    // Application settings, main module and utility namespace
    var options = triptop.options;
    var widgets = triptop.widgets;
    var api = triptop.api;

    /**
     * Namespace for regular hotels service.
     * @namespace triptop.hotels
     */
    triptop.hotels = {
        scope: {}
    };

    /**
     * Tag directive <hotels> to implement container for regular hotels flow. Using template 'hotels/templates/content.html'.
     * @member {angular.Directive} hotels
     * @memberof triptop.widgets
     */
    widgets.directive('hotels', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div id="triptop-hotels" class="triptop-container"></div>',
            controller: function ($scope, $element) {
                triptop.hotels.scope = $scope;

                $scope.hotelOn = true;
                $scope.hotelSearchOn = true;
                $scope.hotelFlexibleOn = true;
                $scope.hotelFilterOn = true;

                $scope.hotelState = 'READY';

                $scope.imgDir = options.paths.basedir + 'hotels/img/';

                $scope.locationsCached = {};
                $scope.locationsCodes = {};

                $scope.resultSet = undefined;
                $scope.filterSet = undefined;
                $scope.querySet = undefined;

                $scope.loadLocations = function (query, callback) {
                    //var cached = $scope.locationsCached[query];
                    //if (cached) {
                    //    callback(cached);
                    //}
                    //else {
                        // ???
                        services.$locations(query, function (data) {
                            for (var i = data.length - 1; i >= 0; i--) {
                                var item = data[i];
                                //item.search = item.code + ' ' + item.location.name + ' ' + item.name;
                                item.search = item.name;
                                $scope.locationsCodes[item.id] = item;
                            }
                            $scope.locationsCached[query] = data;
                            callback(data);
                        });
                    //}
                };

                $scope.findLocations = function (value) {
                    return $scope.locationsCodes[value];
                };

                $scope.changeFlightState = function (state) {
                    if ($scope.hotelState === state) { return; }
                    $scope.hotelState = state;
                    widgets.safeApply($scope);
                };

                $scope.changeProcessState = function (state) {
                    if ($scope.processState === state) { return; }
                    $scope.processState = state;
                    widgets.safeApply($scope);
                };

                $scope.showRules = function ($event, product) {
                    var title = $event.target.innerText;
                    var rules = product.rules;
                    if(!rules) {
                        $scope.changeProcessState('WAITING');

                        services.$details(product, function (data) {
                            rules = data.fareRules;
                            rules = rules.replace(/\n/g, '<br>');
                            rules = rules.replace(/\t/g, '&nbsp;');
                            product.rules = rules;
                            $scope.changeProcessState('READY');
                            $scope.showRules($event, product);
                        }, function () {
                            $scope.changeProcessState('READY');
                        });
                    }
                    else {
                        api.message(title, rules);
                    }
                };

                //$scope.query = {
                //    location: '',
                //    hotelName: '',
                //    rank: 0,
                //    roomType: 0,
                //    available: 2,
                //    checkin: new Date(),
                //    checkout: new Date(),
                //    nights: 1
                //};
                $scope.query = {
                    segments: [{}],
                    paxes: {
                        adults: 1,
                        children: 0,
                        infants: 0
                    },
                    rank: 0,
                    roomType: 0,
                    flexible: false,
                    available: 2
                };

                if(options.hotels && options.hotels.start) {
                    var from = options.hotels.start.from;
                    //if(from) {
                    //    $scope.loadLocations(from, function(data) {
                    //        if(data && data.length > 0) {
                    //            $scope.query.segments[0].from = from;
                    //        }
                    //    });
                    //}

                    var to = options.hotels.start.to;
                    if(to) {
                        //$scope.loadLocations(to, function(data) {
                        //    if(data && data.length > 0) {
                        //        $scope.query.segments[0].to = to;
                        //    }
                        //});
                        $scope.query.segments[0].to = to;
                    }

                    var time = options.hotels.start.time;
                    if(time) {
                        $scope.query.segments[0].time = new Date(time);
                    }

                    var round = options.hotels.start.round;
                    if(round) {
                        $scope.query.segments[0].round = new Date(round);
                    }
                }
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'hotels', 'content', 'hotels.libs', function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);

                    if(options.hotels && options.hotels.ready) {
                        options.hotels.ready();
                    }
                });
            }
        };
    }]);

    // Creates a new dependency 'hotels.libs' for hotels flow initialization
    widgets.config(function (resourceLoaderProvider) {
        resourceLoaderProvider.addDependency('hotels.libs', function (loader, success) {
            loader.css('libs', 'bootstrap/css/less/bootstrap' + options.css_suffix);
            loader.css('libs', 'selectize/css/selectize.bootstrap3');
            loader.css('libs', 'datepicker/css/bootstrap-datepicker');
            loader.css('libs', 'jquery-datepicker/css/datepicker');
            loader.css('libs', 'bootstrap-slider/css/slider');
            loader.css('libs', 'elusive-iconfont/css/elusive-webfont');
            loader.css('hotels', 'hotels' + options.css_suffix);

            loader.get('templates', 'common', 'modal', null, function (template) {
                angular.element('body').append(template);
            });

            loader.js('libs', 'jquery/jquery.min', function () {
                var cnt = 5;
                var runner = function () {
                    cnt--;
                    if (cnt === 0 && success) { success(); }
                };
                loader.js('libs', 'bootstrap/js/bootstrap.min', runner);
                loader.js('libs', 'bootstrap-slider/js/bootstrap-slider', runner);
                loader.js('libs', 'datepicker/bootstrap-datepicker', runner);
                loader.js('libs', 'jquery-datepicker/datepicker', runner);
                loader.js('libs', 'selectize/js/standalone/selectize', function () {
                    loader.js('libs', 'selectize/js/plugins/continue_editing', runner);
                });
            });
        });
    });

    /**
     * Tag directive <hotelsearch> to implement search form for regular hotels. Using template 'hotels/templates/search/search.html'.
     * @member {angular.Directive} hotelsearch
     * @memberof triptop.widgets
     */
     widgets.directive('hotelsearch', ['$compile', '$timeout', 'resourceLoader', function ($compile, $timeout, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                $scope.onChangeTime = function () {
                    var segment = $scope.query.segments[0];

                    var timeInput = $element.find('input[name=time]');
                    var roundInput = $element.find('input[name=round]');

                    if (!segment.round || segment.time.getTime() > segment.round.getTime()) {
                        segment.round = new Date(segment.time.getFullYear(), segment.time.getMonth(), segment.time.getDate() + 1);
                    }

                    var date = new Date();
                    var dd = segment.round.getDate();
                    var mm = segment.round.getMonth() + 1; //January is 0!
                    var yyyy = segment.round.getFullYear();

                    if (dd < 10) {
                        dd = '0' + dd;
                    }

                    if (mm < 10) {
                        mm = '0' + mm;
                    }
                    date = dd + '.' + mm + '.' + yyyy;

                    roundInput.val(date);
                    roundInput.DatePickerShow();

                    console.log(segment);
                };

                $scope.configLocations = function () {
                    return {
                        valueField: 'id',
                        searchField: 'search',
                        searchConjunction: 'or',
                        options: [],
                        create: false,
                        render: {
                            item: function(item, escape) {
                                //return '<div class="tt-item">' +
                                //        '<span class="tt-name">' + escape(item.location.name) + '</span>' +
                                //        '<span class="tt-code">' + escape(item.code) + '</span>' +
                                //    '</div>';
                                return '<div class="tt-item">' +
                                    '<span class="tt-name">' + escape(item.name) + '</span>' +
                                    '</div>';
                            },
                            option: function(item, escape) {
                                //return '<div>' +
                                //        '<div class="tt-item">' +
                                //            '<span class="tt-name">' + escape(item.location.name) + '</span>' +
                                //            '<span class="tt-code">' + escape(item.code) + '</span>' +
                                //        '</div>' +
                                //        '<div class="tt-description">' + escape(item.name) + '</div>' +
                                //    '</div>';
                                return '<div>' +
                                    '<div class="tt-item">' +
                                    '<span class="tt-name">' + escape(item.name) + '</span>' +
                                    '</div>' +
                                    '</div>';
                            }
                        },
                        load: function (query, callback) {
                            if (query.length <= 2) {
                                return callback();
                            }
                            $scope.loadLocations(query, callback);
                        }
                    };
                };

                $scope.addSegment = function () {
                    var segments = $scope.query.segments, length = segments.length;
                    if(length === 9) {
                        return;
                    }
                    var last = $scope.query.segments[length - 1];
                    $scope.query.segments.push({from: last.to, time: last.time});
                };

                $scope.removeSegment = function (segment) {
                    var segments = $scope.query.segments;
                    for(var i = segments.length; i--;) {
                        if(segments[i] === segment) {
                            segments.splice(i, 1);
                        }
                    }
                };

                $scope.runSearch = function () {
                    var params = {
                        action: 'start'
                    };

                    var query = $scope.query, segment;
                    //if(query.tripType < 3) {
                        segment = query.segments[0];

                    if(!segment.to || !segment.time) {
                        return;
                    }

                        //params.fromLocation = segment.from;
                    params.cityId = segment.to;
                    params.checkIn = api.toISOString(segment.time, false, true);
                    params.checkOut = api.toISOString(segment.round, false, true);

                        //if(query.tripType === 2) {
                        //    if(!segment.round) {
                        //        return;
                        //    }
                        //    params.returnDate = api.toISOString(segment.round, false, true);
                        //}
                    //}
                    //else {
                    //    var segments = [];
                    //    for (var i = 0, length = query.segments.length; i < length; i++) {
                    //        segment = query.segments[i];
                    //        segments.push({
                    //            fromLocation: segment.from,
                    //            toLocation: segment.to,
                    //            departureDate: api.toISOString(segment.time, false, true)
                    //        });
                    //    }
                    //    params.segments = api.toJson({segments: segments});
                    //}

                    //params.adults = query.paxes.adults;
                    //params.children = query.paxes.children;
                    //params.infants = query.paxes.infants;
                    params.paxNumber = 1;
                    params.roomNumber = 1;
                    //params.flexDays = query.flexible ? 1 : 0;
                    //params.maxStops = query.stops > 1 ? 10 : query.stops;
                    params.hotelClass = ['', 'Economy', 'Premium', 'Standard', 'First', 'Business'][query.hotelClass];
                    params.displayCurrency = 'USD';

                    var scope = triptop.hotels.scope;

                    scope.resultSet = undefined;
                    scope.filterSet = undefined;

                    if(scope.querySet && scope.querySet.promise) {
                        $timeout.cancel(scope.querySet.promise);
                    }
                    scope.querySet = angular.copy(query);

                    services.$search(params, function (data) {
                        scope.filterSet = data.digest;

                        if (!data.visibleProducts || data.visibleProducts.length === 0) {
                            api.alert($element.find('.tt-notfound').text());
                            scope.changeFlightState('READY');
                        }
                        else {
                            data.pagination = {
                                amount: 1,
                                current: 0,
                                limit: data.total
                            };
                            scope.resultSet = data;
                            scope.changeFlightState('RESULTS');

                            triptop.hotels.scope.querySet.promise = $timeout(function () {
                                triptop.hotels.scope.querySet.corrupt = function () {
                                    triptop.hotels.scope.corruptSearch();
                                };
                            }, 15*60*1000);
                        }
                    }, function () {
                        api.alert($element.find('.tt-notfound').text());
                        scope.changeFlightState('READY');
                    });

                    scope.changeFlightState('SEARCHING');
                };

                triptop.hotels.scope.corruptSearch = function () {
                    api.alert($element.find('.tt-corrupt').text(), function () {
                        $scope.runSearch();
                        widgets.safeApply($scope);
                    });
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'hotels', 'search/search', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    /**
     * Filter 'hotelsQuerySegments' to create segments array.
     * @function hotelsQuerySegments
     * @memberof triptop.widgets
     * @param {object} query Search query.
     * @return {angular.Filter} Returns filter 'hotelsQuerySegments'.
     */
    widgets.filter('hotelsQuerySegments', function() {
        return function(query) {
            if(query.tripType === 3 || query.tripType === '3') {
                return query.segments;
            }
            return [query.segments[0]];
        };
    });

    /**
     * Tag directive <hotelsearchwait> to implement waiting search results form for regular hotels. Using template 'hotels/templates/search/wait.html'.
     * @member {angular.Directive} hotelsearchwait
     * @memberof triptop.widgets
     */
    widgets.directive('hotelsearchwait', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                var segment = $scope.query.segments[0];

                $scope.digest = {
                    //from: $scope.findLocations(segment.from),
                    //to: $scope.findLocations(segment.to),
                    to: $scope.locationsCodes[parseInt(segment.to, 10)],
                    time: segment.time,
                    round: segment.round,
                    tripType: $scope.query.tripType
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'hotels', 'search/wait', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    /**
     * Tag directive <hotelresult> to implement search results container for regular hotels. Using template 'hotels/templates/results/result.html'.
     * @member {angular.Directive} hotelresult
     * @memberof triptop.widgets
     */
    widgets.directive('hotelresult', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                $scope.displayedSimilarProduct = undefined;

                $scope.showDetails = function (product) {
                    product.showDetails = true;
                };

                $scope.hideDetails = function (product) {
                    product.showDetails = false;
                };

                $scope.loadPage = function (index) {
                    var pagination = $scope.resultSet.pagination;
                    if(!pagination) {
                        return;
                    }

                    if(index === undefined) {
                        index = pagination.amount - 1;
                    }

                    if(pagination.current === index) {
                        return;
                    }

                    $scope.changeProcessState('WAITING');

                    services.$page($scope.resultSet, index, function (data) {
                        pagination = $scope.resultSet.pagination;
                        data.pagination = {
                            amount: pagination.amount,
                            current: index,
                            limit: pagination.limit
                        };
                        triptop.hotels.scope.resultSet = data;
                        $scope.changeProcessState('READY');
                    }, function () {
                        $scope.changeProcessState('READY');
                    });
                };

                $scope.showSimilars = function (product) {
                    $scope.changeProcessState('WAITING');

                    if(product.similars) {
                        $scope.displayedSimilarProduct = product;
                        $scope.changeProcessState('READY');
                    }
                    else {
                        services.$similar($scope.resultSet, product, function (data) {
                            product.similars = data.visibleProducts;
                            $scope.displayedSimilarProduct = product;
                            $scope.changeProcessState('READY');
                        }, function () {
                            $scope.changeProcessState('READY');
                        });
                    }
                };

                $scope.hideSimilars = function (product) {
                    $scope.displayedSimilarProduct = undefined;
                };

                $scope.runBooking = function (product) {
                    var querySet = $scope.querySet;
                    if(querySet && querySet.corrupt) {
                        querySet.corrupt();
                    }
                    else {
                        triptop.hotels.scope.bookingProduct = product;
                        $scope.changeProcessState('WAITING');
                        $scope.changeFlightState('BOOKING');
                    }
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'hotels', 'results/result', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    /**
     * Filter 'hotelsVisibleProducts' to create array of visible products.
     * @function hotelsVisibleProducts
     * @memberof triptop.widgets
     * @param {object} row Regular hotel object.
     * @return {angular.Filter} Returns filter 'hotelsVisibleProducts'.
     */
    widgets.filter('hotelsVisibleProducts', function() {
        return function (product, displayedSimilarProduct) {
            if(displayedSimilarProduct && displayedSimilarProduct.supplier.code === product.supplier.code) {
                product.displayedSimilarProduct = true;
                return product.similars;
            }
            else {
                product.displayedSimilarProduct = false;
                return [product];
            }
        };
    });

    /**
     * Tag directive <hotelresultrow> to implement search results item for regular hotels. Using template 'hotels/templates/results/row.html'.
     * @member {angular.Directive} hotelresultrow
     * @memberof triptop.widgets
     */
    widgets.directive('hotelresultrow', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                $scope.stars = 3;
                $scope.getNumber = function(num) {
                    return new Array(num);
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'hotels', 'results/row', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    /**
     * Filter 'hotelsRow' to create array of any segments.
     * @function hotelsRow
     * @memberof triptop.widgets
     * @param {object} row Regular hotel object.
     * @return {angular.Filter} Returns filter 'hotelsRow'.
     */
    widgets.filter('hotelsRow', function() {
        return function(row) {
            return row.in && row.in.length > 0 ? [row.out, row.in] : [row.out];
        };
    });

    /**
     * Filter 'hotelsSeatsAmount' to calculate seats amount.
     * @function hotelsSeatsAmount
     * @memberof triptop.widgets
     * @param {object} row Regular hotel object.
     * @return {angular.Filter} Returns filter 'hotelsSeatsAmount'.
     */
    widgets.filter('hotelsSeatsAmount', function() {
        return function(row) {
            var i, seats, amount = 0;
            for (i = 0; i < row.out.length; i++) {
                seats = row.out[i].seats;
                if(amount === 0 || amount > seats) {
                    amount = seats;
                }
            }
            if(row.in) {
                for (i = 0; i < row.in.length; i++) {
                    seats = row.in[i].seats;
                    if(amount === 0 || amount > seats) {
                        amount = seats;
                    }
                }
            }
            return amount;
        };
    });

    /**
     * Filter 'hotelsChangesDetail' to display of transit destination.
     * @function hotelsChangesDetail
     * @memberof triptop.widgets
     * @param {object} segments Flight segment object.
     * @return {angular.Filter} Returns filter 'hotelsChangesDetail'.
     */
    widgets.filter('hotelsChangesDetail', function() {
        return function(segments) {
            var value = '';
            if(segments.length < 2) {
                return value;
            }
            for (var i = 1, length = segments.length; i < length; i++) {
                value += ', ' + segments[i].departure.location.location.name;
            }
            return value.substr(2);
        };
    });

    /**
     * Filter 'hotelsDuration' to parse duration.
     * @function hotelsDuration
     * @memberof triptop.widgets
     * @param {string} value Duration in ISO value.
     * @return {angular.Filter} Returns filter 'hotelsDuration'.
     */
    widgets.filter('hotelsDuration', function() {
        return function(value) {
            var str = '';
            if(!value) {
                return str;
            }

            var match = /PT((\d*)H)?((\d*)M)?/.exec(value);
            if (match[2]) {
                value = parseInt(match[2], 10);
                str = (value < 10 ? '0' : '') + value;
            }
            else {
                str = '00';
            }
            
            if (match[4]) {
                value = parseInt(match[4], 10);
                str += (':' + (value < 10 ? '0' : '') + value);
            }
            else {
                str += ':00';
            }
            return str;
        };
    });

    /**
     * Tag directive <hotelresultrowdetails> to implement item details for regular hotels. Using template 'hotels/templates/results/details.html'.
     * @member {angular.Directive} hotelresultrowdetails
     * @memberof triptop.widgets
     */
    widgets.directive('hotelresultrowdetails', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'hotels', 'results/details', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    /**
     * Tag directive <hotelflextable> to implement flexible table for regular hotels. Using template 'hotels/templates/results/flextable.html'.
     * @member {angular.Directive} hotelflextable
     * @memberof triptop.widgets
     */
    widgets.directive('hotelflextable', ['$compile', '$timeout', 'resourceLoader', function ($compile, $timeout, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                $scope.selectedDate = undefined;

                $scope.selectFlexDate = function (index) {
                    if($scope.selectedDate === index) {
                        return;
                    }

                    $scope.selectedDate = index;
                    $scope.changeProcessState('WAITING');

                    var date = $scope.resultSet.digest.flexDatePrices[index];
                    services.$supplement($scope.resultSet, date, function (data) {
                        if (!data.visibleProducts || data.visibleProducts.length === 0) {
                            api.alert($element.find('.tt-notfound').text());
                            $scope.changeProcessState('READY');
                            return;
                        }

                        data.pagination = {
                            amount: 1,
                            current: 0,
                            limit: data.total
                        };
                        triptop.hotels.scope.resultSet = data;

                        data.digest.flexDate = date;
                        triptop.hotels.scope.filterSet = data.digest;

                        $scope.changeFlightState('RESULTS');
                        $scope.changeProcessState('READY');

                        var querySet = triptop.hotels.scope.querySet;
                        if(querySet && querySet.promise) {
                            $timeout.cancel(querySet.promise);
                        }

                        triptop.hotels.scope.querySet.promise = $timeout(function () {
                            triptop.hotels.scope.querySet.corrupt = function () {
                                triptop.hotels.scope.corruptSearch();
                            };
                        }, 15*60*1000);
                    }, function () {
                        api.alert($element.find('.tt-notfound').text());
                        $scope.changeProcessState('READY');
                    });
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'hotels', 'results/flextable', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    // Parse hotels time from minutes
    var timeFormater = function (value) {
        var v, str = '';

        v = Math.floor(value / 60);
        str = v < 10 ? ('0' + v) : v;

        v = Math.round(value % 60);
        str += ':' + (v < 10 ? ('0' + v) : v);

        return str;
    };

    // Parse hotels duration from minutes
    var durationFormater = function (value) {
        var v, str = 'PT';

        v = Math.floor(value / 60);
        str += v < 10 ? ('0' + v) : v;

        v = Math.round(value % 60);
        str += 'H' + (v < 10 ? ('0' + v) : v) + 'M';

        return str;
    };

    /**
     * Tag directive <hotelfilter> to implement filter panel for regular hotels. Using template 'hotels/templates/results/filter.html'.
     * @member {angular.Directive} hotelfilter
     * @memberof triptop.widgets
     */
    widgets.directive('hotelfilter', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                var filterApply = function () {
                    $scope.changeProcessState('WAITING');
                    services.$page($scope.resultSet, 0, function (data) {
                        data.pagination = {
                            amount: 1,
                            current: 0,
                            limit: data.total
                        };
                        triptop.hotels.scope.resultSet = data;

                        if($scope.displayedSimilarProduct) {
                            var products = data.visibleProducts, product;
                            for (var i = 0, s = products.length; i < s; i++) {
                                var p = products[i];
                                if(p.supplier.code === $scope.displayedSimilarProduct.supplier.code) {
                                    product = p;
                                    break;
                                }
                            }

                            if(product) {
                                $scope.displayedSimilarProduct = product;

                                services.$similar($scope.resultSet, product, function (data) {
                                    product.similars = data.visibleProducts;
                                    $scope.changeProcessState('READY');
                                }, function () {
                                    $scope.changeProcessState('READY');
                                });
                            }
                            else {
                                $scope.changeProcessState('READY');
                            }
                        }
                        else {
                            $scope.changeProcessState('READY');
                        }
                    },
                    function () {
                        $scope.changeProcessState('READY');
                    });
                };

                //$scope._setSelectedAll = function () {
                //    var filterSet = $scope.filterSet;
                //    for (var i = 0; i < filterSet.airlines.length; i++) {
                //        filterSet.airlines[i].selected = filterSet.airlines.selected;
                //    }
                //};
                //
                //$scope.setSelectedAll = function () {
                //    $scope._setSelectedAll();
                //    if($scope.filterSet.airlines.selected) {
                //        filterApply();
                //    }
                //};
                //
                //$scope.setSelected = function (airline) {
                //    var airlines = $scope.filterSet.airlines;
                //    if(airline.selected) {
                //        var i, size = airlines.length;
                //        for (i = 0; i < size; i++) {
                //            if(!airlines[i].selected) {
                //                break;
                //            }
                //        }
                //        airlines.selected = i === size;
                //    }
                //    else {
                //        airlines.selected = false;
                //    }
                //    filterApply();
                //};

                $scope.setStops = function (value) {
                    filterApply();
                };

                $scope.setSort = function (value) {
                    filterApply();
                };

                $scope.configPrice = function () {
                    var config = {
                        min: Math.floor($scope.filterSet.minPrice),
                        max: Math.ceil($scope.filterSet.maxPrice)
                    };
                    config.value = [config.min, config.max];
                    $scope.filterSet.price = config.value;
                    return config;
                };

                $scope.changePrice = function (event, type) {
                    if(type == 'stop') {
                        filterApply();
                    }
                    else {
                        $scope.filterSet.price = event.value;
                        widgets.safeApply($scope);
                    }
                };                

                var timeConfig = {
                    min: 0,
                    max: 24 * 60 - 1,
                    formater: timeFormater
                };
                timeConfig.value = [timeConfig.min, timeConfig.max];

                //$scope.configDeparture = function () {
                //    var config = angular.copy(timeConfig);
                //    $scope.filterSet.departure = config.value;
                //    return config;
                //};
                //
                //$scope.changeDeparture = function (event, type) {
                //    if(type == 'stop') {
                //        filterApply();
                //    }
                //    else {
                //        $scope.filterSet.departure = event.value;
                //        widgets.safeApply($scope);
                //    }
                //};
                //
                //$scope.configArrival = function () {
                //    var config = angular.copy(timeConfig);
                //    $scope.filterSet.arrival = config.value;
                //    return config;
                //};
                //
                //$scope.changeArrival = function (event, type) {
                //    if(type == 'stop') {
                //        filterApply();
                //    }
                //    else {
                //        $scope.filterSet.arrival = event.value;
                //        widgets.safeApply($scope);
                //    }
                //};
                //
                //$scope.configDuration = function () {
                //    var config = angular.copy(timeConfig);
                //    $scope.filterSet.duration = config.value;
                //    return config;
                //};
                //
                //$scope.changeDuration = function (event, type) {
                //    if(type == 'stop') {
                //        filterApply();
                //    }
                //    else {
                //        $scope.filterSet.duration = event.value;
                //        widgets.safeApply($scope);
                //    }
                //};
            },
            link: function ($scope, $element, $attrs) {
                $scope.$watch('filterSet', function (filter) {
                    if(!filter.sort) {
                        filter.sort = 'price';
                    }

                    if(!filter.boarding) {
                        filter.boarding = 'BedAndBreakfast';
                    }

                    if(!filter.rating) {
                        filter.rating = 4;
                    }

                    resourceLoader.get('templates', 'hotels', 'results/filter', null, function (template) {
                        $element.html(template);
                        $compile($element.contents())($scope);
                    });
                });

                var top = $element.offset().top, fixedOn;
                var handler = function () {
                    if(window.scrollY > top) {
                        if(!fixedOn) {
                            $element.addClass('tt-fixed-on').parent().addClass('tt-fixed-filter');
                            fixedOn = true;
                        }
                    }
                    else if(fixedOn) {
                       $element.removeClass('tt-fixed-on').parent().removeClass('tt-fixed-filter');
                       fixedOn = false;
                    }
                };
                angular.element(window).bind('scroll', handler);

                $scope.$on('$destroy', function () {
                    angular.element(window).unbind('scroll', handler);
                });
            }
        };
    }]);

    /**
     * Filter 'hotelsTimeFormater' to parse hotels time from minutes.
     * @function hotelsTimeFormater
     * @memberof triptop.widgets
     * @param {string} value Duration in ISO value.
     * @return {angular.Filter} Returns filter 'hotelsTimeFormater'.
     */
    widgets.filter('hotelsTimeFormater', function() {
        return timeFormater;
    });

    /**
     * Service to communicate with the server side.
     * @memder services
     * @memberof triptop.hotels
     * @property {function} $locations Getting list of locations.
     * @property {function} $search Running the search query.
     * @property {function} $supplement Running query by date.
     * @property {function} $similar Running the search query similar data.
     * @property {function} $page Running query to retrieve data for specific page.
     * @property {function} $details Running query for hotel details.
     */
    var services = {
        options: {
            //path:  'http://repo.trip-top.com:8888/triptop-fo-api-war/api'
            path: '//82.80.216.47:8080/triptop-fo-api-war/api'
            //path: '//testsrv.trip-top.com:8888/triptop-fo-api-war/api'
        },
        params: {
            locale: 'EN' // options.locale ? options.locale.substr(0, 2).toUpperCase() : 'EN'
        },
        $locations: function (query, success) {
            api._Request('/hotels/dictionaries/cities', angular.extend({
                query: encodeURIComponent(query)
            }, this.params), this.options.path).send(success);
        },
        $search: function (params, success, error) {
            var _this = this;
            api._Request('/hotels/searches', angular.extend(params, this.params), this.options.path).send(function (data) {
                var id = data.id;

                api._Request('/hotels/searches/' + id + '/raw', angular.extend({
                    offset: 0,
                    limit: 10
                }, _this.params), _this.options.path).polling(function (data) {
                    return data.complete;
                },
                function (data) {
                    api._Request('/hotels/searches/' + id + '/processed', angular.extend({
                        //fold: 'airline',
                        offset: 0,
                        'ratings[]': 4,
                        'boardings[]': 'BedAndBreakfast',
                        sort: 'price:asc',
                        limit: data.total
                    }, _this.params), _this.options.path).send(success, error);
                },
                error);
            });
        },
        $supplement: function (resultSet, date, success, error) {
            var _this = this;
            api._Request('/hotels/searches/' + resultSet.id, angular.extend({
                action: 'supplement',
                departureDate: date.departure,
                returnDate: date.arrival
            }, _this.params), _this.options.path).polling(function (data) {
                return data.complete;
            },
            function (data) {
                api._Request('/hotels/searches/' + resultSet.id + '/processed', angular.extend({
                    fold: 'airline',
                    departureDate: date.departure,
                    returnDate: date.arrival,
                    offset: 0,
                    limit: data.total
                }, _this.params), _this.options.path).send(success, error);
            },
            error);
        },
        $similar: function (resultSet, product, success, error) {
            var filterSet = triptop.hotels.scope.filterSet;
            var filter = {
                offset: 0,
                limit: product.similarProducts,
                minPrice: filterSet.price[0],
                maxPrice: filterSet.price[1],
                'ratings[]': filterSet.rating,
                'boardings[]':  filterSet.boarding,
                //minDeparture: timeFormater(filterSet.departure[0]),
                //maxDeparture: timeFormater(filterSet.departure[1]),
                //minArrival: timeFormater(filterSet.arrival[0]),
                //maxArrival: timeFormater(filterSet.arrival[1]),
                //maxOutDuration: durationFormater(filterSet.duration[1]),
                //maxInDuration: durationFormater(filterSet.duration[1]),
                //minStops: filterSet.stops === '2' ? 0 : filterSet.stops,
                //maxStops: filterSet.stops,
                sort: filterSet.sort + ':asc'
            };

            //var date = filterSet.flexDate;
            //if(date) {
            //    filter.departureDate = date.departure;
            //    filter.returnDate = date.arrival;
            //}
            //
            //filter['airlines[]'] = product.supplierAirline.code;

            api._Request('/hotels/searches/' + resultSet.id + '/processed', angular.extend(filter, this.params), this.options.path).send(success);
        },
        $page: function (resultSet, index, success) {
            var filterSet = triptop.hotels.scope.filterSet;
            var filter = {
                fold: 'airline',
                offset: index * resultSet.pagination.limit,
                limit: 200, //resultSet.pagination.limit,
                minPrice: filterSet.price[0],
                maxPrice: filterSet.price[1],
                'ratings[]': filterSet.rating,
                'boardings[]':  filterSet.boarding,
                //minDeparture: timeFormater(filterSet.departure[0]),
                //maxDeparture: timeFormater(filterSet.departure[1]),
                //minArrival: timeFormater(filterSet.arrival[0]),
                //maxArrival: timeFormater(filterSet.arrival[1]),
                //maxOutDuration: durationFormater(filterSet.duration[1]),
                //maxInDuration: durationFormater(filterSet.duration[1]),
                //minStops: filterSet.stops === '2' ? 0 : filterSet.stops,
                //maxStops: filterSet.stops,
                sort: filterSet.sort + ':asc'
            };

            //var date = filterSet.flexDate;
            //if(date) {
            //    filter.departureDate = date.departure;
            //    filter.returnDate = date.arrival;
            //}
            //
            //var airlines = filterSet.airlines;
            //if(!airlines.selected) {
            //    var str = '';
            //    for (var i = 0; i < airlines.length; i++) {
            //        var item = airlines[i];
            //        if(item.selected) {
            //            str += ',' + item.airline.code;
            //        }
            //    }
            //    filter['airlines[]'] = str.substr(1);
            //}

            api._Request('/hotels/searches/' + resultSet.id + '/processed', angular.extend(filter, this.params), this.options.path).send(success);
        },
        $details: function (product, success, error) {
            api._Request('/hotels/products/' + product.id, this.params, this.options.path).polling(function (data) {
                return data.complete === undefined || data.complete === true;
            }, success, error);
        }
    };
}(window.triptop));