'use strict';

(function (triptop) {
    if (!triptop || !triptop.options || !triptop.widgets || !triptop.api) {
        throw new Error('Application not configured');
    }

    var options = triptop.options;
    var widgets = triptop.widgets;
    var api = triptop.api;

    widgets.directive('flightbooking', ['$compile', '$timeout', 'resourceLoader', function ($compile, $timeout, resourceLoader) {
        return {
            restrict: 'AE',
            scope: true,
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                var product = triptop.flights.scope.bookingProduct;
                product.agree = false;
                product.cardOwner = '0';
                product.docRequired = true; // TODO
                product.bookingType = 'CARD';

                $scope.product = product;
                $scope.bookingState = 'PAXES';

                $scope.siteType = 'TERMINAL'; // TODO

                $scope.changeBookingState = function (state) {
                    if ($scope.bookingState === state) { return; }
                    $scope.bookingState = state;
                    widgets.safeApply($scope);
                };

                $scope.cancelBooking = function () {
                    triptop.flights.scope.bookingProduct = $scope.product = null;
                    $scope.changeBookingState(null);
                    $scope.changeFlightState('RESULTS');
                };

                $scope.unavailableBooking = function () {
                    $scope.changeProcessState('READY');
                    api.alert($element.find('.tt-unavailable').text());
                };

                $scope.removeEnter = function ($event) {
                    $timeout(function () {
                        var target = angular.element($event.target);
                        if (target) {
                            var val = target.val(), length = val.length;
                            if (length > 0) {
                                target.val(val.substring(0, length - 1));
                            }
                        }
                    }, 100);
                };

                $scope.latinNameEnter = function ($event) {
                    var ch = $event.charCode;
                    if (ch !== 0 && ch !== 8 && ch !== 32 && (ch < 65 || ch > 122)) {
                        $scope.removeEnter($event);
                    }
                };

                $scope.dateEnter = function ($event) {
                    var ch = $event.charCode;
                    if (ch !== 0 && ch !== 8 && (ch < 46 || ch > 57)) {
                        $scope.removeEnter($event);
                    }
                };

                $scope.numberEnter = function ($event) {
                    var ch = $event.charCode;
                    if (ch !== 0 && ch !== 8 && (ch < 48 || ch > 57)) {
                        $timeout(function () {
                            var target = angular.element($event.target);
                            if (target) {
                                var val = target.val();
                                var upd = '';
                                for (var i = 0; i < val.length; i++) {
                                    var c = val.charCodeAt(i);
                                    if (c >= 48 && c <= 57) {
                                        upd += val.charAt(i);
                                    }
                                }
                                target.val(upd);
                            }
                        }, 100);
                    }
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'flights', 'booking/booking', 'flights.booking.data', function (template) {
                    try {
                        $scope.ipCountry = angular.element.trim(triptop.dictionaries.ips.airports[0].location.parent.code);
                    }
                    catch(e) {}

                    $element.html(template);
                    $compile($element.contents())($scope);
                    $scope.changeProcessState('READY');
                });
            }
        };
    }]);

    widgets.config(function (resourceLoaderProvider) {
        resourceLoaderProvider.addDependency('flights.booking.data', function (loader, success) {
            var counter = 4, runner = function () {
                counter--;
                if (counter === 0 && success) { success(); }
            };

            if(!triptop.dictionaries) { triptop.dictionaries = {}; }
            if(!triptop.dictionaries.flights) { triptop.dictionaries.flights = {}; }

            if(!triptop.dictionaries.flights.meals) {
                services.$meals(function (data) {
                    triptop.dictionaries.flights.meals = data;
                    runner();
                });
            }
            else {
                runner();
            }

            if(!triptop.dictionaries.flights.assistance) {
                services.$assistance(function (data) {
                    triptop.dictionaries.flights.assistance = data;
                    runner();
                });
            }
            else {
                runner();
            }

            if(!triptop.dictionaries.ips) { triptop.dictionaries.ips = {}; }
            if(!triptop.dictionaries.ips.airports) {
                services.$ips(function (data) {
                    triptop.dictionaries.ips.airports = data;
                    runner();
                });
            }
            else {
                runner();
            }

            if(!triptop.dictionaries.countries) {
                services.$countries(function (data) {
                    triptop.dictionaries.countries = data;
                    runner();
                });
            }
            else {
                runner();
            }
        });
    });

    widgets.directive('flightbookingdigest', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'flights', 'booking/digest', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('flightbookingpaxes', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                if(!$scope.product.paxes) {
                    var types = ['adults', 'children', 'infants'];
                    var paxes = triptop.flights.scope.querySet.paxes;
                    $scope.product.paxes = [];
                    for (var key in types) {
                        var count = paxes[types[key]] ? paxes[types[key]] : 0;
                        for (var j = 0; j < count; j++) {
                            $scope.product.paxes.push({sex: 'male', country: $scope.ipCountry});
                        }
                    }
                }

                $scope.getCountries = function () {
                    var options = [], countries = triptop.dictionaries.countries;
                    for (var i = 0, s = countries.length; i < s; i++) {
                        var opt = countries[i];
                        options.push({ value: opt.code.substr(0, 2), text: opt.name });
                    }
                    return options;
                };

                $scope.checkPaxType = function (pax) {
                    if (!pax.birthday || isNaN(pax.birthday.getYear())) {
                        pax.type = null;
                        return;
                    }

                    var diff = new Date(new Date().getTime() - pax.birthday.getTime());
                    diff = Math.abs(diff.getUTCFullYear() - 1970);
                    if (diff < 2) {
                        pax.type = 'INFANTS';
                    }
                    else if (diff < 12) {
                        pax.type = 'CHILDREN';
                    }
                    else {
                        pax.type = 'ADULTS';
                    }
                };

                $scope.gotoForm = function () {
                    $scope.paxesList.submited = true;

                    if ($scope.paxesList.$valid !== true) {
                        $element.find('[ng-form] .ng-pristine.ng-invalid').removeClass('ng-pristine').addClass('ng-dirty');
                        return;
                    }

                    var types = 'ADULTS_ONLY';
                    var paxes = $scope.product.paxes, tmp = angular.copy(triptop.flights.scope.querySet.paxes);
                    if(tmp.adults === 0) {
                        types = 'CHILD_ONLY';
                    }
                    else if(tmp.infants > 0 || tmp.children > 0) {
                        types = 'ADULTS_WITH_CHILD';
                    }
                    else {
                        types = 'MIXED';
                    }

                    var checked, counter;
                    for (var i = 0; i < paxes.length; i++) {
                        var pax = paxes[i];
                        $scope.checkPaxType(pax);
                        var type = pax.type;

                        checked = false;
                        if(types === 'MIXED') {
                            checked = true;
                        }
                        else if(types === 'ADULTS_WITH_CHILD') {
                            if(type === 'INFANTS' || type === 'CHILDREN') {
                                checked = true;
                            }
                            else {
                                pax.type = 'ADULTS';
                            }
                        }
                        else if(types === 'ADULTS_ONLY') {
                            pax.type = 'ADULTS';
                        }

                        type = type.toLowerCase();
                        if(checked && tmp[type] > 0) {
                            tmp[type] = tmp[type] - 1;
                        }
                    }

                    checked = false;
                    if(types === 'MIXED') {
                        counter = tmp.adults > 0 ? tmp.adults : 0 +
                            tmp.children > 0 ? tmp.children : 0 +
                            tmp.infants > 0 ? tmp.infants : 0;

                        checked = counter === 0;
                    }
                    else if(types === 'ADULTS_WITH_CHILD') {
                        counter = tmp.children > 0 ? tmp.children : 0 +
                            tmp.infants > 0 ? tmp.infants : 0;

                        checked = counter === 0;
                    }
                    else if(types === 'ADULTS_ONLY') {
                        checked = true;
                    }

                    if (!checked) {
                        $element.find('[ng-form] .tt-paxes-type').addClass('tt-dirty');
                        return;
                    }
                    else {
                        $element.find('[ng-form] .tt-paxes-type').removeClass('tt-dirty');
                    }

                    $scope.changeBookingState('FORM');
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'flights', 'booking/paxes', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.controller('PaxRowController', ['$scope', function ($scope) {
        $scope.isMouseEnter = false;

        $scope.changePaxSex = function (pax, value) {
            pax.sex = value;
        };                

        $scope.changeBirthDay = function (pax) {
            $scope.checkPaxType(pax);
        };

        $scope.contentMouseEnter = function (value) {
            $scope.isMouseEnter = value;
        };
    }]);

    widgets.directive('flightbookingoptions', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                $scope.showOptions = false;

                var meals = $scope.meals = triptop.dictionaries.flights.meals;
                var assistance = $scope.assistance = triptop.dictionaries.flights.assistance;

                $scope.getMeals = function () {
                    var options = [];
                    for (var i = 0, s = meals.length; i < s; i++) {
                        var opt = meals[i];
                        options.push({ value: opt, text: opt });
                    }
                    return options;
                };

                $scope.setMeals = function (data) {
                    $scope.pax.meals.name = data;
                };

                $scope.getAssistance = function () {
                    var options = [];
                    for (var i = 0, s = assistance.length; i < s; i++) {
                        var opt = assistance[i];
                        options.push({ value: opt, text: opt });
                    }
                    return options;
                };

                $scope.setAssistance = function (data) {
                    $scope.pax.assistance.name = data;
                };

                $scope.openOptions = function (pax) {
                    $scope.showOptions = true;
                };

                $scope.hideOptions = function (pax) {
                    $scope.showOptions = false;
                };

                $scope.closeOptions = function (pax) {
                    $scope.showOptions = false;
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'flights', 'booking/options/options', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('flightbookingbonus', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                $scope.showBonus = false;

                var airlines = $scope.airlines = {};

                var i, airline, product = $scope.product;
                for (i = 0; i < product.out.length; i++) {
                    airline = product.out[i].airline;
                    if(!airlines[airline.code]) {
                        airlines[airline.code] = airline;
                    }
                }
                if(product.in) {
                    for (i = 0; i < product.in.length; i++) {
                        airline = product.in[i].airline;
                        if(!airlines[airline.code]) {
                            airlines[airline.code] = airline;
                        }
                    }
                }

                var supplier = airlines[product.supplierAirline.code];
                if(supplier) {
                    $scope.pax.airline = angular.copy(supplier);
                }

                $scope.getAirlines = function () {
                    var options = [];
                    angular.forEach(airlines, function(airline, code) {
                        options.push({ value: code, text: airline.name });
                    });
                    return options;
                };

                $scope.setAirlines = function (data) {
                    $scope.pax.airline.name = airlines[data].name;
                };

                $scope.openBonus = function () {
                    $scope.showBonus = true;
                };

                $scope.hideBonus = function () {
                    $scope.showBonus = false;
                };

                $scope.closeBonus = function () {
                    $scope.showBonus = false;
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'flights', 'booking/options/bonus', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('flightbookingform', ['$compile', '$timeout', 'resourceLoader', function ($compile, $timeout, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                if(!$scope.product.person) {
                    $scope.product.person = angular.copy($scope.product.paxes[0]);
                }

                $scope.changeBookingType = function (type, cardOwner) {
                    $scope.product.bookingType = type;
                    $scope.product.cardOwner = cardOwner ? cardOwner : '0';
                };

                $scope.getPaxes = function () {
                    var options = [];
                    angular.forEach($scope.product.paxes, function(pax, index) {
                        options.push({ value: pax, text: pax.surname + ' ' + pax.name });
                    });
                    return options;
                };

                $scope.toggleAgreement = function () {
                    $scope.product.agree = !$scope.product.agree;
                };

                $scope.showTerms = function ($event) {
                    var content = angular.element('#tt-booking-rules');
                    if(content.length === 0) {
                        $scope.changeProcessState('WAITING');

                        api.loader.get('terms', {}, function() {
                            content = angular.element('<div style="display: none;"><termsrules></termsrules></div>');
                            angular.element('body').append(content);
                            $compile(content.contents()[0])($scope);
                            $timeout(function () {
                                $scope.changeProcessState('READY');
                                $scope.showTerms($event);
                            }, 3000);
                        });
                    }
                    else {
                        api.message($event.target.innerText, content.html());
                    }
                };

                $scope.showConvention = function ($event) {
                    var content = angular.element('#tt-booking-convention');
                    if(content.length === 0) {
                        $scope.changeProcessState('WAITING');

                        api.loader.get('terms', {}, function() {
                            content = angular.element('<div style="display: none;"><termsconvention></termsconvention></div>');
                            angular.element('body').append(content);
                            $compile(content.contents()[0])($scope);
                            $timeout(function () {
                                $scope.changeProcessState('READY');
                                $scope.showConvention($event);
                            }, 3000);
                        });
                    }
                    else {
                        api.message($event.target.innerText, content.html());
                    }
                };

                $scope.gotoBook = function () {
                    var product = $scope.product;
                    if (!product.agree) {
                        return;
                    }

                    $scope.bookingForm.submited = true;

                    if ($scope.bookingForm.$valid !== true) {
                        $element.find('[ng-form] .ng-pristine.ng-invalid').removeClass('ng-pristine').addClass('ng-dirty');
                        return;
                    }

                    $scope.changeProcessState('WAITING');

                    var gotoPayment  = function () {
                        services.$paymentPrices(product, function (data) {
                            product.paymentPrices = data.total;
                            product.paymentPrices.currencyCode = product.onlinePrice.currencyCode;
                            product.frameUrl = services.$frameUrl(product);
                            $scope.changeBookingState('PAYMENT');
                            $scope.changeProcessState('READY');
                        });
                    };

                    var gotoConfirm  = function () {
                        services.$confirmation(product, function (data) {
                            product.confirm = data;
                            $scope.changeBookingState('CONFIRM');
                            $scope.changeProcessState('READY');
                        });
                    };

                    services.$newCart(function (data) {
                        product.cartId = data.id ? data.id : data;

                        services.$addProduct(product, function () {
                            var paxes = product.paxes, size = paxes.length, counter = size + 1;

                            var reservation = function () {
                                counter--;
                                if(counter === 0) {
                                    services.$reservation(product, function (data) {
                                        product.docketId = data.id ? data.id : data;

                                        services.$checkReservation(product, function () {
                                            if (product.bookingType === 'CARD') {
                                                gotoPayment();
                                            }
                                            else {
                                                gotoConfirm();
                                            }
                                        }, $scope.unavailableBooking);
                                    });
                                }
                            };

                            for (var i = 0; i < size; i++) {
                                services.$addPax(product, paxes[i], reservation);
                            }

                            services.$addPerson(product, reservation);
                        }, $scope.unavailableBooking);
                    });
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'flights', 'booking/formation', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.directive('flightbookingpersons', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div class="tt-persons"></div>',
            controller: function ($scope, $element) {
                $scope.bookingPersons = angular.copy($scope.product.paxes);
                
                $scope.selectPerson = function (person) {
                    $scope.product.person = angular.copy(person);
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'flights', 'booking/paxes/persons', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    widgets.filter('totalPrices', function() {
        return function(product, currency) {
            if(!currency) {
                currency = product.onlinePrice.currencyCode;
            }

            var price = product.bookingType === 'CARD' ? product.onlinePrice.totalAmount : product.onlinePrice.baseAmount;
            if(typeof price === 'string' && price.length > 0) {
                price = parseFloat(price, 10);
            }

            var markup = product.onlinePrice.manualMarkup;
            if(markup) {
                if(typeof markup === 'string' && markup.length > 0) {
                    markup = parseInt(markup, 10);
                }
            }
            else {
                markup = 0;
            }
            product.onlinePrice.manualMarkup = markup;

            var value = price + markup;
            value = value.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');

            var currencies = triptop.dictionaries.currencies;
            if(currencies[currency].right) {
                return value + ' ' + currencies[currency].right;
            }
            if(currencies[currency].left) {
                return currencies[currency].left + ' ' + value;
            }
            return value;
        };
    });

    widgets.directive('flightbookingpayment', ['$compile', '$timeout', 'resourceLoader', function ($compile, $timeout, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                $scope.paymentState = 'WAITING';
                $scope.paymentCurrency = $scope.product.onlinePrice.currencyCode;
                $scope.hash = window.location.hash;

                var promise, polling = function () {
                    var hash = window.location.hash;
                    if($scope.hash != hash) {
                        if (api.endsWith(hash, 'success')) {
                            $scope.paymentState = 'COMPLETE';
                        }
                        else if (api.endsWith(hash, 'close')) {
                            $scope.closePayment();
                        }
                        window.history.back();
                    }
                    promise = $timeout(polling, 1000);
                };

                $scope.closePayment = function () {
                    if(promise) {
                        $timeout.cancel(promise);
                    }

                    if($scope.paymentState === 'COMPLETE') {
                        $scope.changeProcessState('WAITING');

                        var product = $scope.product;
                        services.$checkConfirmation(product, function() {
                            services.$confirmation(product, function (data) {
                                product.confirm = data;
                                $scope.changeBookingState('CONFIRM');
                                $scope.changeProcessState('READY');
                            });
                        }, $scope.unavailableBooking);
                    }
                    else {
                        $scope.changeBookingState('FORM');
                    }
                };

                $scope.iframeLoadedCallBack = function () {
                    $scope.paymentState = 'READY';
                    widgets.safeApply($scope, polling);
                };

                $scope.setPaymentCurrency = function (value) {
                    var product = $scope.product;
                    if(product.paymentPrices) {
                        var prices = product.paymentPrices[value];
                        if(prices) {
                            if(promise) {
                                $timeout.cancel(promise);
                            }

                            product.onlinePrice = {
                                fare: ((prices.fare + prices.agent) / 100).toFixed(2),
                                tax: (prices.tax / 100).toFixed(2),
                                baseAmount: ((prices.fare + prices.agent + prices.tax) / 100).toFixed(2),
                                onlineMarkup: (prices.creditcard / 100).toFixed(2),
                                totalAmount: (prices.total / 100).toFixed(2),
                                affiliateProfit: (prices.agent / 100).toFixed(2),
                                manualMarkup: (product.onlinePrice.manualMarkup * prices.total / product.onlinePrice.totalAmount).toFixed(2),
                                currencyCode: value
                            };
                            product.frameUrl = services.$frameUrl(product);

                            $scope.paymentState = 'WAITING';
                            $scope.reloadPayment();
                        }
                    }
                };

                $scope.reloadPayment = function () {
                    resourceLoader.get('templates', 'flights', 'booking/payment', null, function (template) {
                        $element.html(template);
                        $compile($element.contents())($scope);
                    });
                };
            },
            link: function ($scope, $element, $attrs) {
                $scope.reloadPayment();
            }
        };
    }]);

    widgets.directive('flightbookingconfirm', ['$compile', 'resourceLoader', function ($compile, resourceLoader) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div></div>',
            controller: function ($scope, $element) {
                $scope.openDocument = function (id) {
                    window.open(options.paths.services.main + '/documents/' + id, '_blank').focus();
                };
            },
            link: function ($scope, $element, $attrs) {
                resourceLoader.get('templates', 'flights', 'booking/confirm', null, function (template) {
                    $element.html(template);
                    $compile($element.contents())($scope);
                });
            }
        };
    }]);

    var services = {
        options: {
            path: '//repo.trip-top.com:8888/triptop-fo-api-war/api'
        },
        params: {
            locale: 'EN' // options.locale ? options.locale.substr(0, 2).toUpperCase() : 'EN'
        },
        $meals: function (success) {
            api._Request('/flights/dictionaries/meals', this.params, this.options.path).send(success);
        },
        $assistance: function (success) {
            api._Request('/flights/dictionaries/assistance', this.params, this.options.path).send(success);
        },
        $ips: function (success) {
            api._Request('/ips/airport', this.params, this.options.path).send(success);
        },
        $countries: function (success) {
            api._Request('/flights/dictionaries/countries', this.params, this.options.path).send(success);
        },
        $newCart: function (success) {
            api._Request('/carts', angular.extend({
                action: 'new'
            }, this.params), this.options.path).send(success);
        },
        $addProduct: function (product, success, error) {
            var _this = this;
            api._Request('/carts/' + product.cartId + '/RegularFlight/' + product.id, angular.extend({
                action: 'add',
                productAmount: 1,
                manualMarkupAmount: product.onlinePrice.manualMarkup
            }, this.params), this.options.path).send(function () {
                api._Request('/carts/' + product.cartId + '/availability', angular.extend({
                    action: 'check'
                }, _this.params), _this.options.path).send(function () {
                    api._Request('/carts/' + product.cartId + '/availability', {}, _this.options.path).polling(function (data) {
                        return data.status === 'SUCCESS';
                    }, success, error);
                }, error);
            }, error);
        },
        $addPax: function (product, pax, success) {
            var details;
            if(pax.meals || pax.assistance || (pax.airline && pax.airline.card)) {
                details = {};

                if(pax.meals) {
                    details.meal = pax.meals.code;
                }
                if(pax.assistance) {
                    details.specialAssistance = pax.assistance.code;
                }
                if(pax.airline && pax.airline.card) {
                    details.frequentFlyerCards = [{ card: {airline: pax.airline.code, cardNumber: pax.airline.card }}];
                }
            }

            api._Request('/carts/' + product.cartId + '/order/RegularFlight/' + product.id + '/paxes', angular.extend({
                action: 'add',
                pax: api.toJson({
                    type: 'Adult',
                    title: pax.sex == 'male' ? 'Mister' : 'Miss',
                    firstName: pax.name,
                    lastName: pax.surname,
                    middleName: '',
                    gender: pax.sex == 'male' ? 'Male' : 'Female',
                    country: pax.country,
                    birthday: api.toISOString(pax.birthday, false, true),
                    // email: pax.email,
                    // phone: pax.phone,
                    passportNumber: product.docRequired ? pax.docnum : undefined,
                    passportExpirationDate: product.docRequired ? api.toISOString(pax.docdate, false, true) : undefined,
                    flightDetails: details
                })
            }, this.params), this.options.path).send(function (data) {
                pax.id = data.id ? data.id : data;
                success();
            });
        },
        $addPerson: function (product, success) {
            var person = product.person;
            api._Request('/carts/' + product.cartId + '/order/details', angular.extend({
                action: 'edit',
                details: api.toJson({
                    payer: {
                        type: 'Adult',
                        title: person.sex == 'male' ? 'Mister' : 'Miss',
                        firstName: person.name,
                        lastName: person.surname,
                        middleName: '',
                        gender: person.sex == 'male' ? 'Male' : 'Female',
                        country: person.country,
                        email: person.email,
                        phone: person.phone
                    }
                })
            }, this.params), this.options.path).send(success);
        },
        $reservation: function (product, success) {
            api._Request('/carts/' + product.cartId + '/order/reservation', angular.extend({
                action: "make",
                willPayNow: product.bookingType === 'CARD'
            }, this.params), this.options.path).send(success);
        },
        $checkReservation: function (product, success, error) {
            api._Request('/carts/' + product.cartId + '/order/reservation', {}, this.options.path).polling(function (data) {
                return data.status === 'SUCCESS';
            }, success, error);
        },
        $frameUrl: function (product) {
            var loc = window.location.toString();
            var pos = loc.indexOf('#');
            loc = pos != -1 ? loc.substr(0, pos) : loc;

            return api._Request('/pay/portalform', {
                docketId: product.docketId,
                bookingCurrencyCode: product.paymentPrices.currencyCode,
                originalUrl: encodeURIComponent(loc),
                cardOwnerId: product.cardOwner,
                locale: 'en'
            }, this.options.path).url();
        },
        $paymentPrices: function (product, success) {
            api._Request('/pay/prices', {
                docketId: product.docketId,
                locale: 'en'
            }, this.options.path).send(success);
        },
        $checkConfirmation: function (product, success, error) {
            api._Request('/carts/' + product.cartId + '/order/confirmation', {}, this.options.path).polling(function (data) {
                return data.status === 'SUCCESS';
            }, success, error);
        },
        $confirmation: function (product, success) {
            api._Request('/carts/' + product.cartId + '/order/confirmation/body', this.params, this.options.path).send(success);
        }
    };
}(window.triptop));