/**
 *
 * Date picker
 * Author:Stefan Petre www.eyecon.ro
 * 
 * Dual licensed under the MIT and GPL licenses
 * 
 */
(function ($) {
	var DatePicker = function () {
		var textPosition = jQuery.browser.msie ? "static" : "relative";
		var	ids = {},
			views = {
				years: 'datepickerViewYears',
				moths: 'datepickerViewMonths',
				days: 'datepickerViewDays'
			},
			tpl = {
				wrapper: '<div class="datepicker"><div class="datepickerContainer"><table cellspacing="0" cellpadding="0"><tbody><tr></tr></tbody></table></div></div>',
				head: [
					'<td>',
					'<table cellspacing="0" cellpadding="0">',
						'<thead>',
							'<tr>',
								'<th class="datepickerGoPrev"><a href="#"><span><%=prev%></span></a></th>',
								'<th colspan="5" class="datepickerMonth" style="position:relative;"><a href="#"></a><div class="datepickerMonthList"></div></th>',
								'<th class="datepickerGoNext"><a href="#"><span><%=next%></span></a></th>',
							'</tr>',
							'<tr class="datepickerDoW">',
//								'<th><span><%=week%></span></th>',
								'<th><span><%=day1%></span></th>',
								'<th><span><%=day2%></span></th>',
								'<th><span><%=day3%></span></th>',
								'<th><span><%=day4%></span></th>',
								'<th><span><%=day5%></span></th>',
								'<th><span><%=day6%></span></th>',
								'<th><span><%=day7%></span></th>',
							'</tr>',
						'</thead>',
					'</table></td>'
				],
				space : '<td class="datepickerSpace"><div></div></td>',
				days: [
					'<tbody class="datepickerDays">',
						'<tr>',
//							'<th class="datepickerWeek"><a href="#"><span><%=weeks[0].week%></span></a></th>',
							'<td class="<%=weeks[0].days[0].classname%>"><a href="#"><span><%=weeks[0].days[0].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[0].days[0].text2%></div></a></td>',
							'<td class="<%=weeks[0].days[1].classname%>"><a href="#"><span><%=weeks[0].days[1].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[0].days[1].text2%></div></a></td>',
							'<td class="<%=weeks[0].days[2].classname%>"><a href="#"><span><%=weeks[0].days[2].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[0].days[2].text2%></div></a></td>',
							'<td class="<%=weeks[0].days[3].classname%>"><a href="#"><span><%=weeks[0].days[3].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[0].days[3].text2%></div></a></td>',
							'<td class="<%=weeks[0].days[4].classname%>"><a href="#"><span><%=weeks[0].days[4].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[0].days[4].text2%></div></a></td>',
							'<td class="<%=weeks[0].days[5].classname%>"><a href="#"><span><%=weeks[0].days[5].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[0].days[5].text2%></div></a></td>',
							'<td class="<%=weeks[0].days[6].classname%>"><a href="#"><span><%=weeks[0].days[6].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[0].days[6].text2%></div></a></td>',
						'</tr>',
						'<tr>',
//							'<th class="datepickerWeek"><a href="#"><span><%=weeks[1].week%></span></a></th>',
							'<td class="<%=weeks[1].days[0].classname%>"><a href="#"><span><%=weeks[1].days[0].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[1].days[0].text2%></div></a></td>',
							'<td class="<%=weeks[1].days[1].classname%>"><a href="#"><span><%=weeks[1].days[1].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[1].days[1].text2%></div></a></td>',
							'<td class="<%=weeks[1].days[2].classname%>"><a href="#"><span><%=weeks[1].days[2].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[1].days[2].text2%></div></a></td>',
							'<td class="<%=weeks[1].days[3].classname%>"><a href="#"><span><%=weeks[1].days[3].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[1].days[3].text2%></div></a></td>',
							'<td class="<%=weeks[1].days[4].classname%>"><a href="#"><span><%=weeks[1].days[4].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[1].days[4].text2%></div></a></td>',
							'<td class="<%=weeks[1].days[5].classname%>"><a href="#"><span><%=weeks[1].days[5].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[1].days[5].text2%></div></a></td>',
							'<td class="<%=weeks[1].days[6].classname%>"><a href="#"><span><%=weeks[1].days[6].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[1].days[6].text2%></div></a></td>',
						'</tr>',
						'<tr>',
//							'<th class="datepickerWeek"><a href="#"><span><%=weeks[2].week%></span></a></th>',
							'<td class="<%=weeks[2].days[0].classname%>"><a href="#"><span><%=weeks[2].days[0].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[2].days[0].text2%></div></a></td>',
							'<td class="<%=weeks[2].days[1].classname%>"><a href="#"><span><%=weeks[2].days[1].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[2].days[1].text2%></div></a></td>',
							'<td class="<%=weeks[2].days[2].classname%>"><a href="#"><span><%=weeks[2].days[2].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[2].days[2].text2%></div></a></td>',
							'<td class="<%=weeks[2].days[3].classname%>"><a href="#"><span><%=weeks[2].days[3].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[2].days[3].text2%></div></a></td>',
							'<td class="<%=weeks[2].days[4].classname%>"><a href="#"><span><%=weeks[2].days[4].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[2].days[4].text2%></div></a></td>',
							'<td class="<%=weeks[2].days[5].classname%>"><a href="#"><span><%=weeks[2].days[5].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[2].days[5].text2%></div></a></td>',
							'<td class="<%=weeks[2].days[6].classname%>"><a href="#"><span><%=weeks[2].days[6].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[2].days[6].text2%></div></a></td>',
						'</tr>',
						'<tr>',
//							'<th class="datepickerWeek"><a href="#"><span><%=weeks[3].week%></span></a></th>',
							'<td class="<%=weeks[3].days[0].classname%>"><a href="#"><span><%=weeks[3].days[0].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[3].days[0].text2%></div></a></td>',
							'<td class="<%=weeks[3].days[1].classname%>"><a href="#"><span><%=weeks[3].days[1].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[3].days[1].text2%></div></a></td>',
							'<td class="<%=weeks[3].days[2].classname%>"><a href="#"><span><%=weeks[3].days[2].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[3].days[2].text2%></div></a></td>',
							'<td class="<%=weeks[3].days[3].classname%>"><a href="#"><span><%=weeks[3].days[3].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[3].days[3].text2%></div></a></td>',
							'<td class="<%=weeks[3].days[4].classname%>"><a href="#"><span><%=weeks[3].days[4].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[3].days[4].text2%></div></a></td>',
							'<td class="<%=weeks[3].days[5].classname%>"><a href="#"><span><%=weeks[3].days[5].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[3].days[5].text2%></div></a></td>',
							'<td class="<%=weeks[3].days[6].classname%>"><a href="#"><span><%=weeks[3].days[6].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[3].days[6].text2%></div></a></td>',
						'</tr>',
						'<tr>',
//							'<th class="datepickerWeek"><a href="#"><span><%=weeks[4].week%></span></a></th>',
							'<td class="<%=weeks[4].days[0].classname%>"><a href="#"><span><%=weeks[4].days[0].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[4].days[0].text2%></div></a></td>',
							'<td class="<%=weeks[4].days[1].classname%>"><a href="#"><span><%=weeks[4].days[1].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[4].days[1].text2%></div></a></td>',
							'<td class="<%=weeks[4].days[2].classname%>"><a href="#"><span><%=weeks[4].days[2].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[4].days[2].text2%></div></a></td>',
							'<td class="<%=weeks[4].days[3].classname%>"><a href="#"><span><%=weeks[4].days[3].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[4].days[3].text2%></div></a></td>',
							'<td class="<%=weeks[4].days[4].classname%>"><a href="#"><span><%=weeks[4].days[4].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[4].days[4].text2%></div></a></td>',
							'<td class="<%=weeks[4].days[5].classname%>"><a href="#"><span><%=weeks[4].days[5].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[4].days[5].text2%></div></a></td>',
							'<td class="<%=weeks[4].days[6].classname%>"><a href="#"><span><%=weeks[4].days[6].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[4].days[6].text2%></div></a></td>',
						'</tr>',
						'<tr>',
//							'<th class="datepickerWeek"><a href="#"><span><%=weeks[5].week%></span></a></th>',
							'<td class="<%=weeks[5].days[0].classname%>"><a href="#"><span><%=weeks[5].days[0].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[5].days[0].text2%></div></a></td>',
							'<td class="<%=weeks[5].days[1].classname%>"><a href="#"><span><%=weeks[5].days[1].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[5].days[1].text2%></div></a></td>',
							'<td class="<%=weeks[5].days[2].classname%>"><a href="#"><span><%=weeks[5].days[2].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[5].days[2].text2%></div></a></td>',
							'<td class="<%=weeks[5].days[3].classname%>"><a href="#"><span><%=weeks[5].days[3].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[5].days[3].text2%></div></a></td>',
							'<td class="<%=weeks[5].days[4].classname%>"><a href="#"><span><%=weeks[5].days[4].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[5].days[4].text2%></div></a></td>',
							'<td class="<%=weeks[5].days[5].classname%>"><a href="#"><span><%=weeks[5].days[5].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[5].days[5].text2%></div></a></td>',
							'<td class="<%=weeks[5].days[6].classname%>"><a href="#"><span><%=weeks[5].days[6].text%></span><div style="position:' + textPosition + ';bottom:0px;font-size:10px;height:20px;overflow:hidden;white-space:nowrap;"><%=weeks[5].days[6].text2%></div></a></td>',
						'</tr>',
					'</tbody>'
				]
			},
			defaults = {
				flat: false,
				starts: 1,
				prev: '&#9664;',
				next: '&#9654;',
				lastSel: false,
				mode: 'single',
				view: 'days',
				calendars: 1,
				format: 'Y-m-d',
				position: 'bottom',
				eventName: 'click',
				onRender: function(){return {};},
				onChange: function(){return true;},
				onChangeMonth: function(){return true;},
				onShow: function(){return true;},
				onBeforeShow: function(){return true;},
				onAfterFill: function(){return true;},
				onHide: function(){return true;},
				availableMonth: function(){return null;},
				availableYear:function() {var min = new Date().getFullYear(), max = min + 11, data = new Array(); for (var i = min; i <= max; i++) {data.push(i);} return data;},
				locale: {
					days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
					daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
					daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"],
					months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
					monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
					weekMin: 'wk'
				}
			},
			fill = function(el) {
				var options = $(el).data('datepicker');
				var cal = $(el);
				var currentCal = Math.floor(options.calendars/2), date, data, dow, month, cnt = 0, week, days, indic, indic2, html, tblCal;
				cal.find('td>table tbody').remove();
				for (var i = 0; i < options.calendars; i++) {
					date = new Date(options.current);
					date.addMonths(-currentCal + i);
					tblCal = cal.find('table').eq(i+1);
					switch (tblCal[0].className) {
						case 'datepickerViewDays':
							dow = '<span class="datepickerCurrentMonth">' + formatDate(date, 'B') + '</span><span class="datepickerCurrentYear">' + formatDate(date, 'Y') + '</span>';
							break;
					}
					tblCal.find('thead tr:first th:eq(1) a').html(dow);

					date.setDate(1);
					data = {weeks:[], test: 10};
					month = date.getMonth();
					var dow = (date.getDay() - options.starts) % 7;
					date._addDays(-(dow + (dow < 0 ? 7 : 0)));
					week = -1;
					cnt = 0;
					while (cnt < 42) {
						var fromUser = options.onRender(date);
						indic = parseInt(cnt/7,10);
						indic2 = cnt%7;
						if (!data.weeks[indic]) {
							week = date.getWeekNumber();
							data.weeks[indic] = {
								week: week,
								days: []
							};
						}
						if(!fromUser.text) fromUser.text = "";
						data.weeks[indic].days[indic2] = {
							text: date.getDate(),
							text2: fromUser.text,
							classname: []
						};
						if (month != date.getMonth()) {
							data.weeks[indic].days[indic2].classname.push('datepickerNotInMonth');
						}
						if (date.getDay() == 0) {
							data.weeks[indic].days[indic2].classname.push('datepickerSunday');
						}
						if (date.getDay() == 6) {
							data.weeks[indic].days[indic2].classname.push('datepickerSaturday');
						}
						var val = date.valueOf();
						var index = $.inArray(val, options.date);
						if (fromUser.selected || options.date == val || index > -1 || (options.mode == 'range' && val >= options.date[0] && val <= options.date[1])) {
							data.weeks[indic].days[indic2].classname.push('datepickerSelected');
						}
						if(index > -1) {
							data.weeks[indic].days[indic2].classname.push('datepickerSelected_' + index);
						}
						if (fromUser.disabled) {
							data.weeks[indic].days[indic2].classname.push('datepickerDisabled');
						}
						if (fromUser.className) {
							data.weeks[indic].days[indic2].classname.push(fromUser.className);
						}
						data.weeks[indic].days[indic2].classname = data.weeks[indic].days[indic2].classname.join(' ');
						cnt++;
						date._addDays(1);
					}
					html = tmpl(tpl.days.join(''), data); // + html;
					tblCal.append(html);
					
					if(options.bindHover) 
						tblCal.find('td.datepickerEnable').bind('mouseenter', mouseenter);
				}
				options.onAfterFill.apply(this, [cal.get(0)]);
			},
			parseDate = function (date, format) {
/*
				if (date.constructor == Date) {
					return new Date(date);
				}
*/
				var parts = date != null ? date.split(/\s*[ ]|[\.]|[\-]|[\/]\s*/) : []; // split(/\W+/);
				var against = format.split(/\W+/), d, m, y, h, min, now = new Date();
				for (var i = 0; i < parts.length; i++) {
					switch (against[i]) {
						case 'd':
						case 'e':
							d = parseInt(parts[i], 10);
							break;
						case 'm':
							m = parseInt(parts[i], 10)-1;
							break;
						case 'Y':
						case 'y':
							y = parseInt(parts[i], 10);
							y += y > 100 ? 0 : (y < 29 ? 2000 : 1900);
							break;
						case 'H':
						case 'I':
						case 'k':
						case 'l':
							h = parseInt(parts[i], 10);
							break;
						case 'P':
						case 'p':
							if (/pm/i.test(parts[i]) && h < 12) {
								h += 12;
							} else if (/am/i.test(parts[i]) && h >= 12) {
								h -= 12;
							}
							break;
						case 'M':
							min = parseInt(parts[i], 10);
							break;
						case 'B':
							var index = -1;
							if(!Array.prototype.indexOf) {
							    for(var j = 0, l = now.months.length; j < l; j++)
			    	               if(now.months[j] === parts[i]) {
			    	            	   index = j; break;
			    	               }
   							}
							else {
								index = now.months.indexOf(parts[i]);
							}
							if(index >= 0) {
								m = index;
								if(m < now.getMonth()) y = now.getFullYear() + 1;
							}
							break;
						case 'b':
							var index = -1;
							if(!Array.prototype.indexOf) {
							    for(var j = 0, l = now.monthsShort.length; j < l; j++)
			    	               if(now.monthsShort[j] === parts[i]) {
			    	            	   index = j; break;
			    	               }
   							}
							else {
								index = now.monthsShort.indexOf(parts[i]);
							}
							if(index >= 0) {
								m = index;
								if(m < now.getMonth()) y = now.getFullYear() + 1;
							}
							break;
					}
				}
				return new Date(
					y === undefined ? now.getFullYear() : y,
					m === undefined ? now.getMonth() : m,
					d === undefined ? now.getDate() : d,
					h === undefined ? now.getHours() : h,
					min === undefined ? now.getMinutes() : min,
					0
				);
			},
			formatDate = function(date, format) {
				var m = date.getMonth();
				var d = date.getDate();
				var y = date.getFullYear();
				var wn = date.getWeekNumber();
				var w = date.getDay();
				var s = {};
				var hr = date.getHours();
				var pm = (hr >= 12);
				var ir = (pm) ? (hr - 12) : hr;
				var dy = date.getDayOfYear();
				if (ir == 0) {
					ir = 12;
				}
				var min = date.getMinutes();
				var sec = date.getSeconds();
				var parts = format.split(''), part;
				for ( var i = 0; i < parts.length; i++ ) {
					part = parts[i];
					switch (parts[i]) {
						case 'a':
							part = date.getDayName();
							break;
						case 'A':
							part = date.getDayName(true);
							break;
						case 'b':
							part = date.getMonthName();
							break;
						case 'B':
							part = date.getMonthName(true);
							break;
						case 'C':
							part = 1 + Math.floor(y / 100);
							break;
						case 'd':
							part = (d < 10) ? ("0" + d) : d;
							break;
						case 'e':
							part = d;
							break;
						case 'H':
							part = (hr < 10) ? ("0" + hr) : hr;
							break;
						case 'I':
							part = (ir < 10) ? ("0" + ir) : ir;
							break;
						case 'j':
							part = (dy < 100) ? ((dy < 10) ? ("00" + dy) : ("0" + dy)) : dy;
							break;
						case 'k':
							part = hr;
							break;
						case 'l':
							part = ir;
							break;
						case 'm':
							part = (m < 9) ? ("0" + (1+m)) : (1+m);
							break;
						case 'M':
							part = (min < 10) ? ("0" + min) : min;
							break;
						case 'p':
						case 'P':
							part = pm ? "PM" : "AM";
							break;
						case 's':
							part = Math.floor(date.getTime() / 1000);
							break;
						case 'S':
							part = (sec < 10) ? ("0" + sec) : sec;
							break;
						case 'u':
							part = w + 1;
							break;
						case 'w':
							part = w;
							break;
						case 'y':
							part = ('' + y).substr(2, 2);
							break;
						case 'Y':
							part = y;
							break;
					}
					parts[i] = part;
				}
				return parts.join('');
			},
			extendDate = function(options) {
				if (Date.prototype.tempDate) {
					return;
				}
				Date.prototype.tempDate = null;
				Date.prototype.months = options.months;
				Date.prototype.monthsShort = options.monthsShort;
				Date.prototype.days = options.days;
				Date.prototype.daysShort = options.daysShort;
				Date.prototype.getMonthName = function(fullName) {
					return this[fullName ? 'months' : 'monthsShort'][this.getMonth()];
				};
				Date.prototype.getDayName = function(fullName) {
					return this[fullName ? 'days' : 'daysShort'][this.getDay()];
				};
				Date.prototype._addDays = function (n) {
					this.setDate(this.getDate() + n);
					this.tempDate = this.getDate();
				};
				Date.prototype.addMonths = function (n) {
					if (this.tempDate == null) {
						this.tempDate = this.getDate();
					}
					this.setDate(1);
					this.setMonth(this.getMonth() + n);
					this.setDate(Math.min(this.tempDate, this.getMaxDays()));
				};
				Date.prototype.addYears = function (n) {
					if (this.tempDate == null) {
						this.tempDate = this.getDate();
					}
					this.setDate(1);
					this.setFullYear(this.getFullYear() + n);
					this.setDate(Math.min(this.tempDate, this.getMaxDays()));
				};
				Date.prototype.getMaxDays = function() {
					var tmpDate = new Date(Date.parse(this)),
						d = 28, m;
					m = tmpDate.getMonth();
					d = 28;
					while (tmpDate.getMonth() == m) {
						d ++;
						tmpDate.setDate(d);
					}
					return d - 1;
				};
				Date.prototype.getFirstDay = function() {
					var tmpDate = new Date(Date.parse(this));
					tmpDate.setDate(1);
					return tmpDate.getDay();
				};
				Date.prototype.getWeekNumber = function() {
					var tempDate = new Date(this);
					tempDate.setDate(tempDate.getDate() - (tempDate.getDay() + 6) % 7 + 3);
					var dms = tempDate.valueOf();
					tempDate.setMonth(0);
					tempDate.setDate(4);
					return Math.round((dms - tempDate.valueOf()) / (604800000)) + 1;
				};
				Date.prototype.getDayOfYear = function() {
					var now = new Date(this.getFullYear(), this.getMonth(), this.getDate(), 0, 0, 0);
					var then = new Date(this.getFullYear(), 0, 0, 0, 0, 0);
					var time = now - then;
					return Math.floor(time / 24*60*60*1000);
				};
			},
			layout = function (el) {
				var options = $(el).data('datepicker');
				var cal = $('#' + options.id);
/*				if (!options.extraHeight) {
					var divs = $(el).find('div');
					if(divs.length > 3) {
						options.extraHeight = divs.get(0).offsetHeight + divs.get(1).offsetHeight;
						options.extraWidth = divs.get(2).offsetWidth + divs.get(3).offsetWidth;
					}
					else {
*/						
				if(!options.extraHeight)
					options.extraHeight = 0;
				if(!options.extraWidth)
					options.extraWidth = 0;
/*					}
				}
*/				var tbl = cal.find('table:first').get(0);
				var width = (tbl) ? tbl.offsetWidth : 100;
				var height = (tbl) ? tbl.offsetHeight : 100;
				cal.css({
					width: width + options.extraWidth + 'px',
					height: height + options.extraHeight + 'px'
				}).find('div.datepickerContainer').css({
					width: width + 'px',
					height: height + 'px'
				});
			},
			mouseenter = function(ev) {
				if ($(ev.target).is('div') || $(ev.target).is('span')) {
					ev.target = ev.target.parentNode;
				}
				var el = $(ev.target);
				if (el.is('a')) {
					var parentEl = el.parent();
					var tblEl = parentEl;
					while (!tblEl.hasClass('datepickerContainer')) {
						tblEl = tblEl.parent();
					}
					var flag = true; var last = null;
					tblEl.find('td.datepickerEnable').each(function(index) {
						var item = $(this);
						item.removeClass('datepickerHover_1');
						if(flag) {
							item.addClass('datepickerHover');
							if(last == null) {
								item.addClass('datepickerHover_0');
							}
							else {
								item.removeClass('datepickerHover_0');
							}
							last = item;
						}
						else {
							item.removeClass('datepickerHover');
						}
						if(item.is(parentEl)) {
							flag = false;
						}
					});
					if(last != null) {
						last.addClass('datepickerHover_1');
					}
				}
			},
			click = function(ev) {
				var targetObj = ev.target;
				if ($(ev.target).is('div') || $(ev.target).is('span')) {
					ev.target = ev.target.parentNode;
				}
				var el = $(ev.target);
				if (el.is('li:not(.datepickerMonthListDisabled)')) {
					var options = $(this).data('datepicker');
					var parentEl = el.parent();
					var tblEl = parentEl.parent().parent().parent().parent().parent();
					var tblIndex = $('table', this).index(tblEl.get(0)) - 1;
					var monthOffset = Math.floor(options.calendars/2) - tblIndex;
					switch (parentEl.get(0).className) {
						case 'datepickerMonthSelector':
							options.current.setMonth(parseInt(el.attr('data-index'), 10) + monthOffset);
							break;
						case 'datepickerYearSelector':
							var curMonth = options.current.getMonth();
							if(monthOffset > 0) {
								monthOffset = curMonth - monthOffset < 0 ? 1 : 0;
							}
							else if(monthOffset < 0) {
								monthOffset = curMonth - monthOffset > 11 ? -1 : 0;
							}
							options.current.setFullYear(parseInt(el.attr('data-index'), 10) + monthOffset);
							break;
					}
					parentEl.find('.datepickerMonthListSelected').removeClass('datepickerMonthListSelected');
					el.addClass('datepickerMonthListSelected');
					parentEl.parent().hide();
					options.onChangeMonth.apply(this, [options.current]);
					fill(this);
				}
				else if (el.is('a')) {
					var text = el.find('span').text();
					ev.target.blur();
					if (el.hasClass('datepickerDisabled')) {
						return false;
					}
					var options = $(this).data('datepicker');
					var parentEl = el.parent();
					var tblEl = parentEl.parent().parent().parent();
					var tblIndex = $('table', this).index(tblEl.get(0)) - 1;
					var tmp = new Date(options.current);
					var changed = false;
					var changedMonth = false;
					var fillIt = false;
					if (parentEl.is('th')) {
						if (parentEl.hasClass('datepickerWeek') && options.mode == 'range' && !parentEl.next().hasClass('datepickerDisabled')) {
							var val = parseInt(parentEl.next().text(), 10);
							tmp.addMonths(tblIndex - Math.floor(options.calendars/2));
							if (parentEl.next().hasClass('datepickerNotInMonth')) {
								tmp.addMonths(val > 15 ? -1 : 1);
							}
							tmp.setDate(val);
							options.date[0] = (tmp.setHours(0,0,0,0)).valueOf();
							tmp.setHours(23,59,59,0);
							tmp._addDays(6);
							options.date[1] = tmp.valueOf();
							fillIt = true;
							changed = true;
							options.lastSel = false;
						} else if (parentEl.hasClass('datepickerMonth')) {
							tmp.addMonths(tblIndex - Math.floor(options.calendars/2));
							switch (tblEl.get(0).className) {
								case 'datepickerViewDays':
									var targetClass = targetObj.className;
									var list = parentEl.find('.datepickerMonthList');
									if(!list.is(':visible') && targetClass != null && targetClass.length > 0) {
										var text = $(targetObj).text();
										if(targetClass == 'datepickerCurrentMonth') {
											if(list.find('.datepickerMonthSelector').length == 0) {
												list.empty();
												var ul = $('<ul class="datepickerMonthSelector"/>').appendTo(list);
												var availableMonth = options.availableMonth.apply(this);
												for (var i = 0, length = options.locale.months.length; i < length; i++) {
													var itemClass = (availableMonth != null && availableMonth[i] !== i) ? 'datepickerMonthListDisabled' : '';
													var val = options.locale.months[i];
													if(text == val) {
														itemClass += ' datepickerMonthListSelected';
													}
													ul.append('<li class="' + itemClass + '" data-index="' + i + '">' + val + '</li>');
												}
											}
											list.show();
										}
										else if(targetClass == 'datepickerCurrentYear') {
											if(list.find('.datepickerYearSelector').length == 0) {
												list.empty();
												var ul = $('<ul class="datepickerYearSelector"/>').appendTo(list);
												var availableYear = options.availableYear.apply(this);
												$.each(availableYear, function(index, value) {
													var val = '' + value;
													if(text == val) {
														ul.append('<li class="datepickerMonthListSelected" data-index="' + val + '">' + val + '</li>');
													}
													else {
														ul.append('<li data-index="' + val + '">' + val + '</li>');
													}
												});
												list.show().scrollTop(ul.find('.datepickerMonthListSelected').position().top - 125);
											}
											else {
												list.show();	
											}
										}
										
										var hideList = function (ev) {
											if (ev.target != ev.data.trigger && !isChildOf(ev.data.list.get(0), ev.target, ev.data.list.get(0))) {
												ev.data.list.hide();
												$(document).unbind('mousedown', hideList);
											}
										};
										$(document).bind('mousedown', {list: list, trigger: this}, hideList);
									}
									break;
							}
						} else if (parentEl.parent().parent().is('thead') && !parentEl.hasClass('datepickerGoDisabled')) {
							switch (tblEl.get(0).className) {
								case 'datepickerViewDays':
									options.current.addMonths(parentEl.hasClass('datepickerGoPrev') ? -1 : 1);
									break;
							}
							fillIt = true;
							changedMonth = true;
						}
					} else if (parentEl.is('td') && !parentEl.hasClass('datepickerDisabled')) {
						switch (tblEl.get(0).className) {
							default:
								var val = parseInt(text, 10);
								tmp.addMonths(tblIndex - Math.floor(options.calendars/2));
								if (parentEl.hasClass('datepickerNotInMonth')) {
									tmp.addMonths(val > 15 ? -1 : 1);
								}
								tmp.setDate(val);
								switch (options.mode) {
									case 'multiple':
										val = (tmp.setHours(0,0,0,0)).valueOf();
										if ($.inArray(val, options.date) > -1) {
											$.each(options.date, function(nr, dat){
												if (dat == val) {
													options.date.splice(nr,1);
													return false;
												}
											});
										} else {
											options.date.push(val);
										}
										break;
									case 'range':
										if (!options.lastSel) {
											options.date[0] = (tmp.setHours(0,0,0,0)).valueOf();
										}
										val = (tmp.setHours(23,59,59,0)).valueOf();
										if (val < options.date[0]) {
											options.date[1] = options.date[0] + 86399000;
											options.date[0] = val - 86399000;
										} else {
											options.date[1] = val;
										}
										options.lastSel = !options.lastSel;
										break;
									default:
										options.date = tmp.valueOf();
										break;
								}
								changed = true;
								break;
						}
						fillIt = true;
					}
					if (changed) {
						options.onChange.apply(this, prepareDate(options));
					}
					if (changedMonth) {
						options.onChangeMonth.apply(this, [options.current]);
					}
					if (fillIt) {
						fill(this);
					}
				}
				return false;
			},
			prepareDate = function (options) {
				var tmp;
				if (options.mode == 'single') {
					tmp = new Date(options.date);
					return [formatDate(tmp, options.format), tmp, options.el];
				} else {
					tmp = [[],[], options.el];
					$.each(options.date, function(nr, val){
						var date = new Date(val);
						tmp[0].push(formatDate(date, options.format));
						tmp[1].push(date);
					});
					return tmp;
				}
			},
			getViewport = function () {
				var m = document.compatMode == 'CSS1Compat';
				return {
					l : window.pageXOffset || (m ? document.documentElement.scrollLeft : document.body.scrollLeft),
					t : window.pageYOffset || (m ? document.documentElement.scrollTop : document.body.scrollTop),
					w : window.innerWidth || (m ? document.documentElement.clientWidth : document.body.clientWidth),
					h : window.innerHeight || (m ? document.documentElement.clientHeight : document.body.clientHeight)
				};
			},
			isChildOf = function(parentEl, el, container) {
				if (parentEl == el) {
					return true;
				}
				if (parentEl.contains) {
					return parentEl.contains(el);
				}
				if ( parentEl.compareDocumentPosition ) {
					return !!(parentEl.compareDocumentPosition(el) & 16);
				}
				var prEl = el.parentNode;
				while(prEl && prEl != container) {
					if (prEl == parentEl)
						return true;
					prEl = prEl.parentNode;
				}
				return false;
			},
			show = function (ev) {
				var cal = $('#' + $(this).data('datepickerId'));
				if (!cal.is(':visible')) {
					var calEl = cal.get(0);
					fill(calEl);
					var options = cal.data('datepicker');
					options.onBeforeShow.apply(this, [cal.get(0)]);
					var pos = $(this).offset();
					var viewPort = getViewport();
					var top = pos.top;
					var left = pos.left;
					var oldDisplay = $.curCSS(calEl, 'display');
					cal.css({
						visibility: 'hidden',
						display: 'block'
					});
					layout(calEl);
					switch (options.position){
						case 'top':
							top -= calEl.offsetHeight;
							break;
						case 'left':
							left -= calEl.offsetWidth;
							break;
						case 'right':
							left += this.offsetWidth;
							break;
						case 'bottom':
							top += this.offsetHeight;
							break;
					}
					if (top + calEl.offsetHeight > viewPort.t + viewPort.h) {
						top = pos.top  - calEl.offsetHeight;
					}
					if (top < viewPort.t) {
						top = pos.top + this.offsetHeight + calEl.offsetHeight;
					}
					if (left + calEl.offsetWidth > viewPort.l + viewPort.w) {
						left = pos.left - calEl.offsetWidth;
					}
					if (left < viewPort.l) {
						left = pos.left + this.offsetWidth;
					}
					cal.css({
						visibility: 'visible',
						display: 'block',
						top: top + 'px',
						left: left + 'px'
					});
					if (options.onShow.apply(this, [cal.get(0)]) != false) {
						cal.show();
					}
					$(document).bind('mousedown', {cal: cal, trigger: this}, hide);
				}
				return false;
			},
			hide = function (ev) {
				if (ev.target != ev.data.trigger && !isChildOf(ev.data.cal.get(0), ev.target, ev.data.cal.get(0))) {
					if (ev.data.cal.data('datepicker').onHide.apply(this, [ev.data.cal.get(0)]) != false) {
						ev.data.cal.hide();
					}
					$(document).unbind('mousedown', hide);
				}
			};
		return {
			init: function(options){
				options = $.extend({}, defaults, options||{});
				extendDate(options.locale);
				options.calendars = Math.max(1, parseInt(options.calendars,10)||1);
				options.mode = /single|multiple|range/.test(options.mode) ? options.mode : 'single';
				return this.each(function(){
					if (!$(this).data('datepicker')) {
						options.el = this;
						options.date = new Date();
/*
						if (options.date.constructor == String) {
							options.date = parseDate(options.date, options.format);
							options.date.setHours(0,0,0,0);
						}
*/
						if (options.mode != 'single') {
							options.date = [options.date.valueOf()];
/*
							if (options.date.constructor != Array) {
								options.date = [options.date.valueOf()];
								if (options.mode == 'range') {
									options.date.push(((new Date(options.date[0])).setHours(23,59,59,0)).valueOf());
								}
							} else {
								for (var i = 0; i < options.date.length; i++) {
									options.date[i] = (parseDate(options.date[i], options.format).setHours(0,0,0,0)).valueOf();
								}
								if (options.mode == 'range') {
									options.date[1] = ((new Date(options.date[1])).setHours(23,59,59,0)).valueOf();
								}
							}
*/
						} else {
							options.date = options.date.valueOf();
						}
						if (!options.current) {
							options.current = new Date();
						} else {
							options.current = parseDate(options.current, options.format);
						} 
						options.current.setDate(1);
						options.current.setHours(0,0,0,0);

						var last_datepicker_id = window.last_datepicker_id ? (window.last_datepicker_id + 1) : 1;
						window.last_datepicker_id = last_datepicker_id;
						
						var id = 'datepicker_' + last_datepicker_id + '_' + parseInt(Math.random() * 1000), cnt;
						options.id = id;
						$(this).data('datepickerId', options.id);
						var cal = $(tpl.wrapper).attr('id', id).bind('click', click).data('datepicker', options);
						if (options.className) {
							cal.addClass(options.className);
						}
						if (options.renderWrapper) {
							options.renderWrapper(cal, options);
						}
						var html = '';
						for (var i = 0; i < options.calendars; i++) {
							cnt = options.starts;
							if (i > 0) {
								html += tpl.space;
							}
							html += tmpl(tpl.head.join(''), {
//									week: options.locale.weekMin,
									prev: options.prev,
									next: options.next,
									day1: options.locale.daysMin[(cnt++)%7],
									day2: options.locale.daysMin[(cnt++)%7],
									day3: options.locale.daysMin[(cnt++)%7],
									day4: options.locale.daysMin[(cnt++)%7],
									day5: options.locale.daysMin[(cnt++)%7],
									day6: options.locale.daysMin[(cnt++)%7],
									day7: options.locale.daysMin[(cnt++)%7]
								});
						}
						cal
							.find('tr:first').append(html)
								.find('table').addClass(views[options.view]);
						fill(cal.get(0));
						
						if (options.flat) {
							cal.appendTo(this).show().css('position', 'relative');
							layout(cal.get(0));
						} else {
							cal.appendTo(document.body);
							$(this).bind(options.eventName, show);
						}
					}
				});
			},
			showPicker: function() {
				return this.each( function () {
					if ($(this).data('datepickerId')) {
						show.apply(this);
					}
				});
			},
			hidePicker: function() {
				return this.each( function () {
					if ($(this).data('datepickerId')) {
						$('#' + $(this).data('datepickerId')).hide();
					}
				});
			},
			setDate: function(date, shiftTo){
				return this.each(function(){
					if ($(this).data('datepickerId')) {
						var cal = $('#' + $(this).data('datepickerId'));
						var options = cal.data('datepicker');
						options.date = date;
/*
						if (options.date.constructor == String) {
							options.date = parseDate(options.date, options.format);
							options.date.setHours(0,0,0,0);
						}
*/
						if (options.mode != 'single') {
/*
							if (options.date.constructor != Array) {
								options.date = [options.date.valueOf()];
								if (options.mode == 'range') {
									options.date.push(((new Date(options.date[0])).setHours(23,59,59,0)).valueOf());
								}
							} else {
*/
								for (var i = 0; i < options.date.length; i++) {
									var d = options.date[i];
									if (typeof d == 'string') {
										options.date[i] = (parseDate(d, options.format).setHours(0,0,0,0)).valueOf();
									}
									else if (typeof d == 'number') {
										options.date[i] = ((new Date(d)).setHours(23,59,59,0)).valueOf();
									}
								}
								if (options.mode == 'range') {
									options.date[1] = ((new Date(options.date[1])).setHours(23,59,59,0)).valueOf();
								}
//							}
						} else {
							options.date = options.date.valueOf();
						}
						if (shiftTo) {
							options.current = new Date(options.mode != 'single' ? options.date[0] : options.date);
							options.current.setHours(0,0,0,0);
						}
						fill(cal.get(0));
					}
				});
			},
			getCurrentMonth: function(){
				if (this.size() > 0) {
					return $('#' + $(this).data('datepickerId')).data('datepicker').current;
				}
				return null;
			},
			setCurrentMonth: function(date, month){
				return this.each(function(){
					if ($(this).data('datepickerId')) {
						var cal = $('#' + $(this).data('datepickerId'));
						var options = cal.data('datepicker');
						try {
/*							
							var date = new Date();
							var m = typeof(month) == "number" ? month : parseInt(month);
							if(m > 0) { date.setMonth(0, 1); date.addMonths(m); }
*/
							options.current = new Date(date.getTime());
							options.current.setHours(0,0,0,0);
							if(month) { options.current.addMonths(month); }
						}
						catch(e) {}
					}
				});
			},
			_setCurrentMonth: function(month){
				return this.each(function(){
					if ($(this).data('datepickerId')) {
						var cal = $('#' + $(this).data('datepickerId'));
						var options = cal.data('datepicker');
						try {
							var date = new Date();
							var m = typeof(month) == "number" ? month : parseInt(month);
							if(m > 0) { date.setMonth(0, 1); date.addMonths(m); }
							options.current = date;
							options.current.setHours(0,0,0,0);
						}
						catch(e) {}
					}
				});
			},
			setDate2: function(year, month, day, shiftTo){
				return this.each(function(){
					if ($(this).data('datepickerId')) {
						var cal = $('#' + $(this).data('datepickerId'));
						var options = cal.data('datepicker');
						var date = (new Date(year, month, day)).valueOf();
						options.date = [date, date];

						if (shiftTo) {
							options.current = new Date(options.mode != 'single' ? options.date[0] : options.date);
							options.current.setHours(0,0,0,0);
						}
						fill(cal.get(0));
					}
				});
			},
			setFirst: function(date1, date2, shiftTo){
				return this.each(function(){
					if ($(this).data('datepickerId')) {
						var cal = $('#' + $(this).data('datepickerId'));
						var options = cal.data('datepicker');
						
						if(date2 == null || date2 < date1) date2 = date1;
						date1.setHours(0,0,0,0);
						date2.setHours(0,0,0,0);
						options.date = [date1.valueOf(), date2.valueOf()];
						options.lastSel = true;
						
						if (shiftTo) {
							options.current = new Date(options.mode != 'single' ? options.date[0] : options.date);
							options.current.setHours(0,0,0,0);
						}
						fill(cal.get(0));
					}
				});
			},
			getDate: function(formated) {
				if (this.size() > 0) {
					return prepareDate($('#' + $(this).data('datepickerId')).data('datepicker'))[formated ? 0 : 1];
				}
			},
			dateParse: function(date) {
				if (this.size() > 0) {
					var options = $('#' + $(this).data('datepickerId')).data('datepicker');
					return parseDate(date, options.format);
				}
				return null;
			},
			dateFormat: function(date) {
				if (this.size() > 0) {
					var options = $('#' + $(this).data('datepickerId')).data('datepicker');
					return formatDate(date, options.format);
				}
				return null;
			},
			clear: function(){
				return this.each(function(){
					if ($(this).data('datepickerId')) {
						var cal = $('#' + $(this).data('datepickerId'));
						var options = cal.data('datepicker');
						if (options.mode != 'single') {
							options.date = [];
							fill(cal.get(0));
						}
					}
				});
			},
			fixLayout: function(){
				return this.each(function(){
					if ($(this).data('datepickerId')) {
						var cal = $('#' + $(this).data('datepickerId'));
						var options = cal.data('datepicker');
						if (options.flat) {
							layout(cal.get(0));
						}
					}
				});
			}
		};
	}();
	$.fn.extend({
		DatePicker: DatePicker.init,
		DatePickerHide: DatePicker.hidePicker,
		DatePickerShow: DatePicker.showPicker,
		DatePickerSetDate: DatePicker.setDate,
		DatePickerSetDate2: DatePicker.setDate2,
		DatePickerSetFirst: DatePicker.setFirst,
		DatePickerSetCurrentMonth: DatePicker.setCurrentMonth,
		DatePickerGetCurrentMonth: DatePicker.getCurrentMonth,
		DatePickerGetDate: DatePicker.getDate,
		DatePickerClear: DatePicker.clear,
		DatePickerLayout: DatePicker.fixLayout,
		DatePickerParse: DatePicker.dateParse,
		DatePickerFormat: DatePicker.dateFormat
	});
})(jQuery);

(function(){
  var cache = {};
  this.tmpl = function tmpl(str, data){
    var fn = !/\W/.test(str) ?
      cache[str] = cache[str] ||
        tmpl(document.getElementById(str).innerHTML) :
     
      new Function("obj",
        "var p=[],print=function(){p.push.apply(p,arguments);};" +
        "with(obj){p.push('" +
        str
          .replace(/[\r\t\n]/g, " ")
          .split("<%").join("\t")
          .replace(/((^|%>)[^\t]*)'/g, "$1\r")
          .replace(/\t=(.*?)%>/g, "',$1,'")
          .split("\t").join("');")
          .split("%>").join("p.push('")
          .split("\r").join("\\'")
      + "');}return p.join('');");
    return data ? fn( data ) : fn;
  };
})();