angular.module('starter.services', ['ngResource'])

.factory('Session', function ($resource) {
    return $resource('http://10.0.0.2:5000/sessions/:sessionId');
});